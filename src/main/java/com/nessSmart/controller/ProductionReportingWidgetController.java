package com.nessSmart.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.nessSmart.Constants.Constants;
import com.nessSmart.Constants.URLMappingConstants;
import com.nessSmart.mapping.database.model.ProductionLogsAnalysisReport;
import com.nessSmart.mapping.response.model.ProductionReportingResponse;
import com.nessSmart.mapping.response.model.WidgetDataAndDrillDown;
import com.nessSmart.service.fetchdata.JiraFetchDataService;
import com.nessSmart.service.fetchdata.ProductionLogsFetchDataService;

@RestController
@RequestMapping(URLMappingConstants.PRODUCTION_REPORTING_WIDGET_MAPPING)
@CrossOrigin
public class ProductionReportingWidgetController {
	
	
	@Autowired
	JiraFetchDataService jiraService;
	
	@Autowired
	ProductionLogsFetchDataService productionLogsFetchService;

	@RequestMapping(value = URLMappingConstants.GET_JIRA_ISSUES, method = RequestMethod.GET, produces = Constants.PRODUCES_VALUE)
	public WidgetDataAndDrillDown getJiraVersionDetails(
			@RequestParam(value = Constants.RESOURCENAME, required = true) String resourceName) {
		return jiraService.getP1Issues() ;
	}
	
	@RequestMapping(value = URLMappingConstants.GET_PRODUCTION_LOGS_ANALYSIS, method = RequestMethod.GET, produces = Constants.PRODUCES_VALUE)
	public ProductionLogsAnalysisReport getProductionLogsAnalysisReport() {
		return productionLogsFetchService.getProductionLogsAnalysisReport();
	}
}
