package com.nessSmart.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.nessSmart.Constants.Constants;
import com.nessSmart.Constants.URLMappingConstants;
import com.nessSmart.mapping.common.model.Dashboard;
import com.nessSmart.mapping.common.model.DashboardData;
import com.nessSmart.mapping.common.model.User;
import com.nessSmart.mapping.common.model.Widget;
import com.nessSmart.service.fetchdata.DashboardFetchDataServices;
import com.nessSmart.service.savedata.DashboardSaveDataServices;
import com.nessSmart.service.savedata.UserSaveDataServices;

@RestController
@RequestMapping(URLMappingConstants.DASHBOARD_MAPPING)
@CrossOrigin
public class DashboardController {
	@Autowired
	DashboardFetchDataServices dashboardFetchDataServices;

	@Autowired
	DashboardSaveDataServices dashboardSaveDataServices;

	@Autowired
	UserSaveDataServices userSaveDataService;

	@RequestMapping(value = URLMappingConstants.SAVE_DASHBOARD_APIS, method = RequestMethod.POST, consumes = Constants.CONSUMES_VALUE)
	public String saveDashboard(@RequestBody Dashboard dashboard) {
		return dashboardSaveDataServices.saveAnyObject(dashboard);
	}

	@RequestMapping(value = URLMappingConstants.DELETE_DASHBOARD_APIS, method = RequestMethod.DELETE)
	public boolean deleteDashboard(@RequestParam(value = Constants.DASHBOARD_ID, required = true) String dashboardId) {
		return dashboardSaveDataServices.deleteDashboard(dashboardId);
	}

	@RequestMapping(value = URLMappingConstants.GET_DASHBOARD_APIS, method = RequestMethod.GET)
	public DashboardData getDashboardsForUser(@RequestParam(value = Constants.USER_ID, required = true) String userId) {
		return dashboardFetchDataServices.getDashboardsForUser(userId);
	}

	@RequestMapping(value = URLMappingConstants.SAVE_USER_DETAILS, method = RequestMethod.POST, consumes = Constants.CONSUMES_VALUE)
	public String saveUserDetails(@RequestBody User user) {
		return userSaveDataService.createUserObject(user);
	}

	@RequestMapping(value = URLMappingConstants.UPDATE_USER_DETAILS, method = RequestMethod.POST, consumes = Constants.CONSUMES_VALUE)
	public String updateUserDetails(@RequestBody User user) {
		return userSaveDataService.updateUserObject(user);
	}

	@RequestMapping(value = URLMappingConstants.SAVE_WIDGETS_MASTER_DETAILS, method = RequestMethod.POST, consumes = Constants.CONSUMES_VALUE)
	public String saveWidgetsMasterDetails(@RequestBody Widget widget) {
		return dashboardSaveDataServices.saveWidgetsMasterDetails(widget);
	}

	@RequestMapping(value = URLMappingConstants.GET_DASHBOARD_WIDGETS__DETAILS, method = RequestMethod.GET)
	public List<Widget> getAllWidgets() {
		return dashboardFetchDataServices.getAllWidgets();
	}

	@RequestMapping(value = URLMappingConstants.GET_ALL_USER__DETAILS, method = RequestMethod.GET)
	public List<User> getAllUsers() {
		return dashboardFetchDataServices.getAllUserDetails();
	}
}
