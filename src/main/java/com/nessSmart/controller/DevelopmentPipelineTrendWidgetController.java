package com.nessSmart.controller;

import java.io.IOException;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.nessSmart.Constants.Constants;
import com.nessSmart.Constants.URLMappingConstants;
import com.nessSmart.mapping.response.model.DevelopmentPipelineTrendWidget;
import com.nessSmart.mapping.response.model.DrillDown;
import com.nessSmart.mapping.response.model.WidgetResponse;
import com.nessSmart.service.fetchdata.BuildFailureRateProdService;
import com.nessSmart.service.fetchdata.DevPipelineTrendWidgetJenkinsService;
import com.nessSmart.service.fetchdata.JiraFetchDataService;
import com.nessSmart.service.fetchdata.TestLinkFetchDataService;

@RestController
@RequestMapping(URLMappingConstants.DEVELOPMENT_WIDGET_MAPPING)
@CrossOrigin
public class DevelopmentPipelineTrendWidgetController {
	@Autowired
	TestLinkFetchDataService testLinkService;

	@Autowired
	DevPipelineTrendWidgetJenkinsService jenkinsService;

	@Autowired
	JiraFetchDataService jiraService;
	
	@Autowired
	BuildFailureRateProdService buildFailureRateProdService;

	
	@RequestMapping(value = URLMappingConstants.GET_JENKINS_AVERAGE_LEAD_TIME_FOR_PRODUCTION, method = RequestMethod.GET, produces = Constants.PRODUCES_VALUE)
	public DevelopmentPipelineTrendWidget getAverageLeadTimeforProductionChanges
			(@RequestParam(value = Constants.PROD_PROJECTNAME, required = true) String projectName,
			//TODO: Need move these env in property file  
		   @RequestParam(value = Constants.DEV_ENV, required = true) String devEnv, 
			@RequestParam(value =Constants.PROD_ENV, required = true) String prodEnv) {
		 return jenkinsService.getAverageLeadTimeforProductionChanges();
	}
	
	
	@RequestMapping(value = URLMappingConstants.GET_JIRA_PRODUCTION_RECOVERY_TIME, method = RequestMethod.GET)
	public WidgetResponse getJiraRecoveryTimeSpent(@RequestParam(value = Constants.RESOURCENAME, required = true) String resourceName) {
		return jiraService.getJiraRecoveryTimeSpent();
	}
	
	@RequestMapping(value = URLMappingConstants.GET_PRODUCTION_FAILURE_RATE, method = RequestMethod.GET)
	public WidgetResponse getProductionFailureRate() {
		return buildFailureRateProdService.getFailureRate();

	}
	
	@RequestMapping(value = URLMappingConstants.GET_JENKINS_PRODUCTION_DEPLOYMENT_FREQUENCY, method = RequestMethod.GET, produces = Constants.PRODUCES_VALUE)
	public DevelopmentPipelineTrendWidget getProductionDeploymentFrequency(
			@RequestParam(value = Constants.PROJECTNAME, required = true) String projectName,
			@RequestParam(value = Constants.DATE_FORMATE, required = true) Date buildDeployeDate,
			@RequestParam(value = Constants.ENV, required = true) String env) throws IOException, ParseException {
		return jenkinsService.getProductionDeploymentFrequency();
	}

}
