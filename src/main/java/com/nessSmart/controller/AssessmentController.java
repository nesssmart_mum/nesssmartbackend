package com.nessSmart.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.nessSmart.Constants.Constants;
import com.nessSmart.Constants.URLMappingConstants;
import com.nessSmart.mapping.database.model.Assessment;
import com.nessSmart.mapping.database.model.AssessmentResponse;
import com.nessSmart.mapping.database.model.AssessmentTitles;
import com.nessSmart.mapping.database.model.Question;
import com.nessSmart.service.fetchdata.AssessmentFetchDataService;
import com.nessSmart.service.savedata.AssessmentSaveOrDeleteDataService;

@RestController
@RequestMapping(URLMappingConstants.ASSESSMENT_MAPPING)
@CrossOrigin
public class AssessmentController {

	@Autowired
	AssessmentFetchDataService fetchDataService;

	@Autowired
	AssessmentSaveOrDeleteDataService saveDataService;

	@RequestMapping(value = URLMappingConstants.SAVE_QUESTIONBANK_ENDURL, method = RequestMethod.POST, consumes = Constants.CONSUMES_VALUE, produces = Constants.PRODUCES_VALUE)
	public boolean save(@RequestBody List<Question> questionList) {
		saveDataService.saveQuestionBank(questionList);
		return true;
	}

	@RequestMapping(value = URLMappingConstants.SAVE_ASSESSMENT_ENDURL, method = RequestMethod.POST, consumes = Constants.CONSUMES_VALUE)
	public boolean saveAssessment(@RequestBody Assessment assessment) {
		return saveDataService.saveAnyObject(assessment);
	}

	@RequestMapping(value = URLMappingConstants.SAVE_ASSESSMENT_RESPONSE_ENDURL, method = RequestMethod.POST, consumes = Constants.CONSUMES_VALUE)
	public String saveAssessmentResponse(@RequestBody AssessmentResponse response) {
		return saveDataService.saveAssessmentResponse(response);
	}

	@RequestMapping(value = URLMappingConstants.GET_QUESTIONBANK_ENDURL, method = RequestMethod.GET, produces = Constants.PRODUCES_VALUE)
	public List<Question> getQuestions(
			@RequestParam(value = Constants.PROJECTTYPE, required = false) String projectType) {
		return fetchDataService.getQuestionBank(projectType);
	}

	@RequestMapping(value = URLMappingConstants.GET_ASSESSMENT_TITLES, method = RequestMethod.GET, produces = Constants.PRODUCES_VALUE)
	public List<AssessmentTitles> getAssessmentTitles() {
		return fetchDataService.getAssessmentTitles();
	}

	@RequestMapping(value = URLMappingConstants.GET_ASSESSMENT_BY_ID_ENDURL, method = RequestMethod.GET, produces = Constants.PRODUCES_VALUE)
	public Assessment getAssessmentById(
			@RequestParam(value = Constants.ASSESSMENT_ID, required = true) String assessmentId) {
		return fetchDataService.getAssessmentById(assessmentId);
	}

	@RequestMapping(value = URLMappingConstants.GET_ASSESSMENT_RESPONSE_REPORT_BY_ID_ENDURL, method = RequestMethod.GET, produces = Constants.PRODUCES_VALUE)
	public AssessmentResponse getAssessmentResponseByUserId(
			@RequestParam(value = Constants.ASSESSMENT_RESPONSE_ID, required = true) String assessmentResponseId) {
		return fetchDataService.getAssessmentReportById(assessmentResponseId);
	}

	@RequestMapping(value = URLMappingConstants.GET_ASSESSMENT_RESPONSE_BY_ID_ENDURL, method = RequestMethod.GET, produces = Constants.PRODUCES_VALUE)
	public AssessmentResponse getAssessmentResponseById(
			@RequestParam(value = Constants.ASSESSMENT_RESPONSE_ID, required = true) String assessmentResponseId) {
		return fetchDataService.getAssessmentResponseById(assessmentResponseId);
	}

	@RequestMapping(value = URLMappingConstants.GET_ASSESSMENT_RESPONSE_MAPPING_ENDURL, method = RequestMethod.GET)
	public String getAssessmentResponseMapping() {
		return fetchDataService.getAssessmentResponseMapping().toString();
	}

	@RequestMapping(value = URLMappingConstants.DELETE_ASSESSMENT_ENDURL, method = RequestMethod.DELETE)
	public boolean deleteAssessment(
			@RequestParam(value = Constants.ASSESSMENT_ID, required = true) String assessmentId) {
		return saveDataService.deleteAssessment(assessmentId);
	}

	@RequestMapping(value = URLMappingConstants.DELETE_ASSESSMENT_RESPONSE_ENDURL, method = RequestMethod.DELETE)
	public boolean deleteAssessmentResponse(
			@RequestParam(value = Constants.ASSESSMENT_RESPONSE_ID, required = true) String assessmentResponseId) {
		return saveDataService.deleteAssessmentResponse(assessmentResponseId);
	}

}
