package com.nessSmart.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.nessSmart.Constants.Constants;
import com.nessSmart.Constants.URLMappingConstants;
import com.nessSmart.mapping.database.model.FunctionalTestStatusReport;
import com.nessSmart.mapping.response.model.WidgetResponse;
import com.nessSmart.service.fetchdata.DevPipelineTrendWidgetJenkinsService;
import com.nessSmart.service.fetchdata.JiraFetchDataService;
import com.nessSmart.service.fetchdata.ReleaseStatusWidgetService;

@RestController
@RequestMapping(URLMappingConstants.RELEASE_STATUS_WIDGET_MAPPING)
@CrossOrigin
public class ReleaseStatusWidgetController {

	@Autowired
	JiraFetchDataService jiraService;

	@Autowired
	DevPipelineTrendWidgetJenkinsService devPipelineTrendWidgetJenkinsService;

	@Autowired
	ReleaseStatusWidgetService releaseStatusWidgetService;

	@RequestMapping(value = URLMappingConstants.GET_JIRA_RELEASE_STATUS, method = RequestMethod.GET, produces = Constants.PRODUCES_VALUE)
	public List<WidgetResponse> getJiraVersionDetails(
			@RequestParam(value = Constants.RESOURCENAME, required = true) String resourceName) {

		return jiraService.getVersionStatus();
	}

	@RequestMapping(value = URLMappingConstants.GET_FUNTIONAL_TEST_STATUS, method = RequestMethod.GET, produces = Constants.PRODUCES_VALUE)
	public List<FunctionalTestStatusReport> getFunctionalTestStatusDetail(
			@RequestParam(value = Constants.PROJECTNAME, required = true) String projectName,
			@RequestParam(value = Constants.DEV_PROJECTNAME, required = true) String devEnv,
			@RequestParam(value = Constants.INT_PROJECTNAME, required = true) String intEnv,
			@RequestParam(value = Constants.TEST_PROJECTNAME, required = true) String testEnv,
			@RequestParam(value = Constants.PROD_PROJECTNAME, required = true) String prodEnv) {

		return devPipelineTrendWidgetJenkinsService.getFunctinalTestStatusDetail();
	}

	@RequestMapping(value = URLMappingConstants.GET_RELEASE_MANIFEST_MAPPING, method = RequestMethod.GET, produces = Constants.PRODUCES_VALUE)
	public Map<String, List<WidgetResponse>> getReleaseManifestMapping(
			@RequestParam(value = Constants.RESOURCENAME, required = true) String resourceName) {
		return releaseStatusWidgetService.getManifestdData();
	}

}
