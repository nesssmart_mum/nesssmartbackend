package com.nessSmart.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AboutController {

	final String APPLICATION_START_MESSAGE = "NessSmart Application Started !";
	final String FRONT_SLASH = "/";

	@RequestMapping(FRONT_SLASH)
	public String home() {
		return APPLICATION_START_MESSAGE;
	}

}
