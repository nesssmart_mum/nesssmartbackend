package com.nessSmart.controller;

import java.sql.SQLException;
import java.util.List;

import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.nessSmart.Constants.Constants;
import com.nessSmart.Constants.URLMappingConstants;
import com.nessSmart.mapping.common.model.JenkinsPipelineDetails;
import com.nessSmart.mapping.database.model.TestLink;
import com.nessSmart.mapping.database.model.TestLinkData;
import com.nessSmart.service.fetchdata.DevPipelineTrendWidgetJenkinsService;
import com.nessSmart.service.savedata.ProductionLogsSaveDataService;
import com.nessSmart.service.savedata.TestLinkSaveDataService;
import com.nessSmart.service.savedata.TriggerToolsDataService;

@RestController
@RequestMapping("/nessSmart/trigerSaveData")
@CrossOrigin
public class TriggerToolsDataController {

	@Autowired
	TestLinkSaveDataService testLinkService;

	@Autowired
	TriggerToolsDataService triggerService;
	
	@Autowired
	DevPipelineTrendWidgetJenkinsService jenkinsService;
	
	@Autowired
	ProductionLogsSaveDataService productionLogsSaveDataService;

	@RequestMapping(value = "/setTestlink", method = RequestMethod.GET, produces = Constants.PRODUCES_VALUE)
	public boolean insertTestLinkSuccessRatio(
			@RequestParam(value = Constants.PROJECTID, required = true) String projectId) {

		return testLinkService.saveTestLinkDataToDb(projectId);

	}

	@RequestMapping(value = "/jenkins", method = RequestMethod.GET, produces = Constants.PRODUCES_VALUE)
	public boolean getJenkinsBuildData() {
		triggerService.fetchDataFromJenkins();
		return true;
	}

	@RequestMapping(value = "/saveJiraData", method = RequestMethod.GET, produces = Constants.PRODUCES_VALUE)
	public boolean getDataFromJira() {
		triggerService.fetchJiraDetails();
		return true;
	}

	@RequestMapping(value = "/failureRateInProduction", method = RequestMethod.GET, produces = Constants.PRODUCES_VALUE)
	public boolean failureRateInProduction() {

		return true;
	}

	@RequestMapping(value = "/saveFunctionalTestingStatus ", method = RequestMethod.GET, produces = Constants.PRODUCES_VALUE)
	public boolean saveFunctionalTestingStatus(
			@RequestParam(value = Constants.PROJECTNAME, required = true) String projectName,
			@RequestParam(value = Constants.DEV_PROJECTNAME, required = true) String devProjectName,
			@RequestParam(value = Constants.INT_PROJECTNAME, required = true) String intProjectName,
			@RequestParam(value = Constants.TEST_PROJECTNAME, required = true) String sitProjectName,
			@RequestParam(value = Constants.PROD_PROJECTNAME, required = true) String prodProjectName)
			throws JSONException {

		triggerService.saveFunctionalTestingStatus();
		return true;
	}

	@RequestMapping(value = "/saveJenkinsPipelineDetails", method = RequestMethod.GET, produces = Constants.PRODUCES_VALUE)
	public JenkinsPipelineDetails saveJenkinsPipelineDetails(
			@RequestParam(value = Constants.PROJECTNAME, required = true) String projectName,
			@RequestParam(value = Constants.PIPLELINE_NAME, required = true) String pipelineName) {
		return jenkinsService.saveJenkinsPipelineDetails();
	}
	
	
	@RequestMapping(value = URLMappingConstants.READ_PRODUCTION_LOGS_ANALYSIS, method = RequestMethod.GET, produces = Constants.PRODUCES_VALUE)
	public boolean readProductionLogsAndSaveInDb() {
		return productionLogsSaveDataService.readProductionLogsAndSaveInDb();

	}
	
	
	@RequestMapping(value = URLMappingConstants.SAVE_TESTLINK_YEAR_DATA, method = RequestMethod.GET, produces = Constants.PRODUCES_VALUE)
	public TestLink getYearDataFromTestLinkData() throws SQLException {
		return testLinkService.getYearDataFromTestLinkData();
	}
	
	
	
}
