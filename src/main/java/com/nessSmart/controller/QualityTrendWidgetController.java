package com.nessSmart.controller;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.nessSmart.Constants.Constants;
import com.nessSmart.Constants.URLMappingConstants;
import com.nessSmart.mapping.response.model.WidgetDataAndDrillDown;
import com.nessSmart.service.fetchdata.JenkinsFetchDataService;
import com.nessSmart.service.fetchdata.JiraFetchDataService;
import com.nessSmart.service.fetchdata.TestLinkFetchDataService;

@RestController
@RequestMapping(URLMappingConstants.QUALITY_WIDGET_MAPPING)
@CrossOrigin
public class QualityTrendWidgetController {

	@Autowired
	TestLinkFetchDataService testLinkService;

	@Autowired
	JenkinsFetchDataService jenkinsService;

	@Autowired
	JiraFetchDataService jiraService;

	@RequestMapping(value = URLMappingConstants.GET_TESTLINK_DATA_ENDURL, method = RequestMethod.GET, produces = Constants.PRODUCES_VALUE)
	public WidgetDataAndDrillDown getTestLinkSuccessRatio() {
		return testLinkService.fetchTestLinkData();
	}

	@RequestMapping(value = URLMappingConstants.JENKINS_SUCCESS_RATIO, method = RequestMethod.GET, produces = Constants.PRODUCES_VALUE)
	public WidgetDataAndDrillDown getJenkinsSuccessRatio(
			@RequestParam(value = Constants.RESOURCENAME, required = true) String resourceName) throws IOException {
		return jenkinsService.getJenkinsBuildTestDetailsForQualityWidget();
	}

	@RequestMapping(value = URLMappingConstants.JENKINS_SONAR, method = RequestMethod.GET, produces = Constants.PRODUCES_VALUE)
	public WidgetDataAndDrillDown getJenkinsSonarData(
			@RequestParam(value = Constants.RESOURCENAME, required = true) String resourceName) throws IOException {
		return jenkinsService.getJenkinsSonarDetailsForQualityWidget();
	}

}
