package com.nessSmart.utils;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.mysql.jdbc.StringUtils;
import com.nessSmart.Constants.Constants;
import com.nessSmart.Constants.Months;
import com.nessSmart.mapping.common.model.Issues;
import com.nessSmart.mapping.common.model.Measures;
import com.nessSmart.mapping.database.model.JiraIssueDetails;
import com.nessSmart.mapping.json.model.JiraDetails;
import com.nessSmart.mapping.json.model.JiraIssues;

public class CommonUtility {

	public static String getHours(String seconds) {
		float timeInHours = 0;
		if (!StringUtils.isNullOrEmpty(seconds)) {
			float timeInSeconds = Float.parseFloat(seconds);
			int hourInSeconds = 3600;
			timeInHours = timeInSeconds / hourInSeconds;
		}
		return Float.toString(timeInHours);

	}

	public static String getFormattedDateString(String since) {
		String formattedDate = null;
		DateFormat dateFormat = new SimpleDateFormat(Constants.DATE_FORMAT_JIRA);
		try {
			Date date = dateFormat.parse(since);
			formattedDate = dateFormat.format(date);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return formattedDate;
	}

	public static String getFormattedDate(String since) {
		String formattedDate = null;
		DateFormat dateFormat = new SimpleDateFormat(Constants.DATE_FORMAT_NO_TIME);
		try {
			Date date = dateFormat.parse(since);
			formattedDate = dateFormat.format(date);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return formattedDate;
	}
	
	public static String getConvertedDate(long timestamp) {
		Date date = new Date(timestamp);
		String s = date.toString();
		List<String> dateArray = new ArrayList<String>(Arrays.asList(s.split(" ")));
		Months month = Months.valueOf(dateArray.get(dateArray.size() - 5));
		String dateString = dateArray.get(dateArray.size() - 1) + Constants.DASH + month.getMonthInNumber()
				+ Constants.DASH + dateArray.get(dateArray.size() - 4) + Constants.T
				+ dateArray.get(dateArray.size() - 3) + Constants.TIME_ZONE;
		String finalDate = getFormattedDateString(dateString);
		return finalDate;
	}

	public static Date getDate(String dateFormat, String dateString) {
		Date date = null;
		try {
			DateFormat dateFormatter = new SimpleDateFormat(dateFormat);
		
			date = dateFormatter.parse(dateString.trim());
		} catch (ParseException e) {			

			e.printStackTrace();
		}
		return date;
	}

	public static boolean isDateBetween(Date date, Date startDate, Date endDate) {
		if (date.compareTo(startDate) >= 0 && date.compareTo(endDate) <= 0) {
			return true;
		}
		return false;
	}

	public static List<Issues> populateSonarIssues(String jsonResponse) {
		List<Issues> listdata = new ArrayList<Issues>();
		try {
			JSONObject jsonObj = new JSONObject(jsonResponse);
			ObjectMapper mapper = new ObjectMapper();
			JSONArray jArray = (JSONArray) jsonObj.get(Constants.ISSUES);
			if (jArray != null) {
				for (int i = 0; i < jArray.length(); i++) {
					JSONObject issues = (JSONObject) jArray.get(i);
					listdata.add(mapper.readValue(issues.toString(), Issues.class));
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		} catch (JsonParseException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return listdata;

	}

	public static List<Measures> populateSonarqubeCoveragehistory(String jsonResponse) {
		List<Measures> listdata = new ArrayList<Measures>();
		try {
			JSONObject jsonObj = new JSONObject(jsonResponse);
			ObjectMapper mapper = new ObjectMapper();
			JSONArray jArray = (JSONArray) jsonObj.get(Constants.MEASURES);
			if (jArray != null) {
				for (int i = 0; i < jArray.length(); i++) {
					JSONObject issues = (JSONObject) jArray.get(i);
					listdata.add(mapper.readValue(issues.toString(), Measures.class));
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		} catch (JsonParseException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return listdata;
	}

	public static List<JiraIssueDetails> populateJiraIssueDetails(JiraDetails details, String resourceName) {
		List<JiraIssueDetails> jiraIssueDetailsList = new ArrayList<JiraIssueDetails>();
		for (JiraIssues issue : details.getIssues()) {
			String resolution = (issue.getFields().getResolution() != null) ? Constants.RESOLVED : Constants.UNRESOLVED;
			String createdDate = CommonUtility.getFormattedDateString(issue.getFields().getCreated());
			String jiraIssueCreateDate = CommonUtility.getFormattedDate(issue.getFields().getCreated());
			
			JiraIssueDetails jiraIssueDetails = new JiraIssueDetails();
			jiraIssueDetails.setIssueCreateDate(jiraIssueCreateDate);
			jiraIssueDetails.setId(resourceName + issue.getId());
			jiraIssueDetails.setResourceName(resourceName);
			jiraIssueDetails.setIssueId(issue.getId());
			jiraIssueDetails.setIssueKey(issue.getKey());
			jiraIssueDetails.setIssueTypeId(issue.getFields().getIssuetype().getId());
			jiraIssueDetails.setIssueTypeName(issue.getFields().getIssuetype().getName());
			jiraIssueDetails.setIsSubTask(issue.getFields().getIssuetype().getSubtask());
			jiraIssueDetails.setTimeestimate(issue.getFields().getTimeoriginalestimate());
			jiraIssueDetails.setTimespent(issue.getFields().getTimespent());
			jiraIssueDetails.setCreateDate(createdDate);
			jiraIssueDetails.setCreatedDate(createdDate);
			jiraIssueDetails.setResolution(resolution);
			jiraIssueDetails.setLabels(issue.getFields().getLabels());
			if (issue.getFields().getPriority() != null) {
				jiraIssueDetails.setPriority(issue.getFields().getPriority().getName());
				jiraIssueDetails.setUpdatedDate(issue.getFields().getUpdated());
				jiraIssueDetailsList.add(jiraIssueDetails);
			}
		}
		return jiraIssueDetailsList;
	}

	public static JiraDetails processJiraData(String jsonObj) {
		ObjectMapper mapper = new ObjectMapper();
		JiraDetails jiraIssueDetails = null;
		try {
			jiraIssueDetails = mapper.readValue(jsonObj, JiraDetails.class);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return jiraIssueDetails;
	}

	public static <T> List<T> convertJsonToList(String jsonObj, Class<T> className)
			throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		return mapper.readValue(jsonObj, TypeFactory.defaultInstance().constructCollectionType(List.class, className));
	}

	public static Map<String, String> getPropertiesMap(String endPointUrl, String endUrl, String userName,
			String password, String resourceName) throws IOException {
		Map<String, String> jenkinsPropertiesMap = new HashMap<String, String>();
		jenkinsPropertiesMap.put(Constants.ENDPOINT_URL, endPointUrl);
		jenkinsPropertiesMap.put(Constants.ENDURL, endUrl);
		jenkinsPropertiesMap.put(Constants.USERNAME, userName);
		jenkinsPropertiesMap.put(Constants.PASSWORD, password);
		jenkinsPropertiesMap.put(Constants.RESOURCENAME, resourceName);
		return jenkinsPropertiesMap;
	}
	
	public static String getdDateFormatString(String since) {
		String formattedDate = null;
		DateFormat dateFormat = new SimpleDateFormat(Constants.DATE_FORMAT_NO_TIME);
		try {
			Date date = dateFormat.parse(since);
			formattedDate = dateFormat.format(date);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return formattedDate;
	}

}
