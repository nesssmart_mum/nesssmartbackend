package com.nessSmart.utils;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.springframework.stereotype.Component;

@Component
public class LoadProperties {

	public Properties loadPropertiesFile(String propFileName) throws IOException {
		InputStream inputStream = null;
		Properties prop = new Properties();
		try {

			inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);
			if (inputStream != null) {
				prop.load(inputStream);
			} else {
				throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
			}

		} catch (Exception e) {
			System.out.println("Failed to loadPropertiesFile in LoadProperties : " + e);
		} finally {
			inputStream.close();
		}
		return prop;
	}

}
