package com.nessSmart.utils;

import java.util.Comparator;
import java.util.Date;

import com.nessSmart.Constants.Constants;
import com.nessSmart.mapping.common.model.History;
import com.nessSmart.mapping.common.model.JiraProjectVersionDetails;
import com.nessSmart.mapping.response.model.WidgetResponse;

public class ComparatorUtils {

	public static Comparator<JiraProjectVersionDetails> getDateComaparatorForJiraProjectVersionDetails() {
		return new Comparator<JiraProjectVersionDetails>() {
			@Override
			public int compare(JiraProjectVersionDetails o1, JiraProjectVersionDetails o2) {
				Date date1 = CommonUtility.getDate(Constants.DATE_FORMAT_NO_TIME, o1.getCreatedDate());
				Date date2 = CommonUtility.getDate(Constants.DATE_FORMAT_NO_TIME, o2.getCreatedDate());
				return date1.compareTo(date2);
			}
		};
	}

	public static Comparator<WidgetResponse> getComaparatorForManifestEnv() {
		return new Comparator<WidgetResponse>() {
			@Override
			public int compare(WidgetResponse o1, WidgetResponse o2) {
				Integer value1 = Integer.parseInt(o1.getArrowValue());
				Integer value2 = Integer.parseInt(o2.getArrowValue());
				if (value1.compareTo(value2) > 0)
					return -1;
				else if (value1.compareTo(value2) < 0)
					return 1;
				else
					return 0;
			}
		};
	}
	
	public static Comparator<History> getDateComaparatorForSonarHistory() {
		return new Comparator<History>() {
			@Override
			public int compare(History h1, History h2) {
				Date date1 = CommonUtility.getDate(Constants.DATE_FORMAT_NO_TIME, h1.getDate());
				Date date2 = CommonUtility.getDate(Constants.DATE_FORMAT_NO_TIME, h2.getDate());
				return date1.compareTo(date2);
			}
		};
	}

}
