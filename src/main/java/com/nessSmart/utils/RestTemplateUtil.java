package com.nessSmart.utils;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.nessSmart.Constants.Constants;

@SuppressWarnings("rawtypes")
@Service
public class RestTemplateUtil {

	public String getRestResponseWithoutCredentials(Map<String, String> parametersMap,
			Map<String, String> propertiesMap, HttpMethod method) throws IOException {
		RestTemplate restTemplate = new RestTemplate();
		HttpEntity<String> request = new HttpEntity<String>("");
		String finalUrl = getEndPointURL(parametersMap, propertiesMap);
		ResponseEntity<String> response = restTemplate.exchange(finalUrl, method, request, String.class);
		return response.getBody();
	}

	public String getRestResponseWithCredentials(Map<String, String> parametersMap, Map<String, String> propertiesMap,
			HttpMethod method) throws IOException {
		RestTemplate restTemplate = new RestTemplate();
		Map<String, String> headers = new HashMap<String, String>();
		headers.put(Constants.USERNAME, propertiesMap.get(Constants.USERNAME));
		headers.put(Constants.PASSWORD, propertiesMap.get(Constants.PASSWORD));
		HttpEntity<String> request = new HttpEntity<String>(getRestTemplateHeaders(headers));
		String finalUrl = getEndPointURL(parametersMap, propertiesMap);
		ResponseEntity<String> response = restTemplate.exchange(finalUrl, method, request, String.class);
		return response.getBody();
	}

	private String getEndPointURL(Map<String, String> parametersMap, Map<String, String> propertiesMap) {
		StringBuffer endUrl = new StringBuffer();
		endUrl.append(propertiesMap.get(Constants.ENDURL));
		endUrl = updateEndUrlWithParameters(endUrl, parametersMap);
		String endPointUrl = propertiesMap.get(Constants.ENDPOINT_URL) + endUrl;
		return endPointUrl;
	}

	private StringBuffer updateEndUrlWithParameters(StringBuffer endUrl, Map<String, String> parametersMap) {

		if (parametersMap.size() == 0)
			return endUrl;

		endUrl.append(Constants.QUESTION_MARK);
		Iterator it = parametersMap.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry map = (Map.Entry) it.next();
			endUrl.append((String) map.getKey()).append(Constants.EQUALS).append((String) map.getValue());
			if (it.hasNext())
				endUrl.append(Constants.AND);
		}
		return endUrl;
	}

	private HttpHeaders getRestTemplateHeaders(Map<String, String> headers) {

		String plainCreds = headers.get(Constants.USERNAME) + Constants.COLON + headers.get(Constants.PASSWORD);
		byte[] plainCredsBytes = plainCreds.getBytes();
		byte[] base64CredsBytes = Base64.encodeBase64(plainCredsBytes);
		String base64Creds = new String(base64CredsBytes);

		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.add(Constants.AUTHORIZATION, "Basic " + base64Creds);
		return httpHeaders;
	}

}
