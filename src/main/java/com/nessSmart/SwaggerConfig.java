package com.nessSmart;

import static springfox.documentation.builders.PathSelectors.regex;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
@ComponentScan(basePackages = "com.nessSmart")
public class SwaggerConfig {

	
	 @Bean
	    public Docket newsApi() {
			return new Docket(DocumentationType.SWAGGER_2).groupName("NessSmart").apiInfo(apiInfo()).select().
	        		paths(regex("/nessSmart.*")).build();
	    }

	    private ApiInfo apiInfo() {
	        return new ApiInfoBuilder().title("Swagger For NessSmart").description("APIs available in NessSmart").build();
	    }
	    
	    //http://localhost:8080/swagger-ui.html
}
