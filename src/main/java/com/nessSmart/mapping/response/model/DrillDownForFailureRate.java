/**
 * 
 */
package com.nessSmart.mapping.response.model;

import java.util.List;

/**
 * @author P7110974
 *
 */
public class DrillDownForFailureRate {
	private String month;
	private List<String> buildVersion;
	
	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}
	public List<String> getBuildVersion() {
		return buildVersion;
	}

	public void setBuildVersion(List<String> buildVersion) {
		this.buildVersion = buildVersion;
	}

	

}
