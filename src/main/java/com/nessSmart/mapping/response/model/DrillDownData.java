package com.nessSmart.mapping.response.model;

import java.util.List;

public class DrillDownData {
	private String name;
	
	private List<Sections> sections;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<Sections> getSections() {
		return sections;
	}
	public void setSections(List<Sections> sections) {
		this.sections = sections;
	}



	
	
}
