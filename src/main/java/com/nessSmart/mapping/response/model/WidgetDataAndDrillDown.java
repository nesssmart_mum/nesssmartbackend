package com.nessSmart.mapping.response.model;

import java.util.List;

public class WidgetDataAndDrillDown {

	private List widgetData;
	private DrillDown drillDown;

	public DrillDown getDrillDown() {
		return drillDown;
	}

	public void setDrillDown(DrillDown drillDown) {
		this.drillDown = drillDown;
	}

	public List getWidgetData() {
		return widgetData;
	}

	public void setWidgetData(List widgetData) {
		this.widgetData = widgetData;
	}

}
