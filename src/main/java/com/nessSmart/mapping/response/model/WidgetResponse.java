package com.nessSmart.mapping.response.model;

public class WidgetResponse {

	private String name;
	private String value;
	private String hoverValue;
	private String unitValue;
	private String arrowValue;
	private DrillDown drillDown;
	private WidgetDataAndDrillDown widgetDataAndDrillDown;

	public WidgetDataAndDrillDown getWidgetDataAndDrillDown() {
		return widgetDataAndDrillDown;
	}

	public void setWidgetDataAndDrillDown(WidgetDataAndDrillDown widgetDataAndDrillDown) {
		this.widgetDataAndDrillDown = widgetDataAndDrillDown;
	}

	public DrillDown getDrillDown() {
		return drillDown;
	}

	public void setDrillDown(DrillDown drillDown) {
		this.drillDown = drillDown;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getArrowValue() {
		return arrowValue;
	}

	public void setArrowValue(String arrowValue) {
		this.arrowValue = arrowValue;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getHoverValue() {
		return hoverValue;
	}

	public void setHoverValue(String hoverValue) {
		this.hoverValue = hoverValue;
	}

	public String getUnitValue() {
		return unitValue;
	}

	public void setUnitValue(String unitValue) {
		this.unitValue = unitValue;
	}

}
