/**
 * 
 */
package com.nessSmart.mapping.response.model;

/**
 * @author P7110974
 *
 */
public class Sections {
	private String title;
	private Double value = 0D;

	
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	public Double getValue() {
		return value;
	}

	public void setValue(Double value) {
		this.value = value;
	}

}
