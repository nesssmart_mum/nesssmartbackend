package com.nessSmart.mapping.response.model;

public class DevelopmentPipelineTrendWidget {
	private String name;
	private int value;
	private String arrowValue;
	private int greaterThenCurrentDate;
	private int lessThenCurrentDate;
	private String unitValue;
	private DrillDown drillDown;

	public String getUnitValue() {
		return unitValue;
	}

	public void setUnitValue(String unitValue) {
		this.unitValue = unitValue;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public String getArrowValue() {
		return arrowValue;
	}

	public void setArrowValue(String arrowValue) {
		this.arrowValue = arrowValue;
	}

	public int getGreaterThenCurrentDate() {
		return greaterThenCurrentDate;
	}

	public void setGreaterThenCurrentDate(int greaterThenCurrentDate) {
		this.greaterThenCurrentDate = greaterThenCurrentDate;
	}

	public int getLessThenCurrentDate() {
		return lessThenCurrentDate;
	}

	public void setLessThenCurrentDate(int lessThenCurrentDate) {
		this.lessThenCurrentDate = lessThenCurrentDate;
	}

	public DrillDown getDrillDown() {
		return drillDown;
	}

	public void setDrillDown(DrillDown drillDown) {
		this.drillDown = drillDown;
	}
}
