/**
 * 
 */
package com.nessSmart.mapping.response.model;

import java.util.List;

/**
 * @author P7110974
 *
 */
public class DrillDownLeadTimeForChanges {
	String manifestVersion;
	List<VersionData> versionData;

	public List<VersionData> getVersionData() {
		return versionData;
	}

	public void setVersionData(List<VersionData> versionData) {
		this.versionData = versionData;
	}

	public String getManifestVersion() {
		return manifestVersion;
	}

	public void setManifestVersion(String manifestVersion) {
		this.manifestVersion = manifestVersion;
	}

}
