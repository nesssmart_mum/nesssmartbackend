package com.nessSmart.mapping.response.model;

import java.util.List;

public class DrillDown {
	private String xLabel;
	private String yLabel;
	private List drillDownData;
	
	public String getxLabel() {
		return xLabel;
	}

	public void setxLabel(String xLabel) {
		this.xLabel = xLabel;
	}

	public String getyLabel() {
		return yLabel;
	}

	public void setyLabel(String yLabel) {
		this.yLabel = yLabel;
	}
	public List getDrillDownData() {
		return drillDownData;
	}

	public void setDrillDownData(List drillDownData) {
		this.drillDownData = drillDownData;
	}

}
