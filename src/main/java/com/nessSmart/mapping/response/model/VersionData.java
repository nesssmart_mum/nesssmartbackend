/**
 * 
 */
package com.nessSmart.mapping.response.model;

/**
 * @author P7110974
 *
 */
public class VersionData {
	String environment;
	int week;

	public String getEnvironment() {
		return environment;
	}

	public void setEnvironment(String environment) {
		this.environment = environment;
	}

	public int getWeek() {
		return week;
	}

	public void setWeek(int week) {
		this.week = week;
	}

}
