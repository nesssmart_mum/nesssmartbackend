package com.nessSmart.mapping.database.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Level {

	@JsonProperty
	private String title;

	@JsonProperty
	private List<Heading> headingList;

	@JsonProperty
	private String completion;

	@JsonProperty
	private String rating;

	public String getCompletion() {
		return completion;
	}

	public void setCompletion(String completion) {
		this.completion = completion;
	}

	public String getRating() {
		return rating;
	}

	public void setRating(String rating) {
		this.rating = rating;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public List<Heading> getHeadingList() {
		return headingList;
	}

	public void setHeadingList(List<Heading> headingList) {
		this.headingList = headingList;
	}

}
