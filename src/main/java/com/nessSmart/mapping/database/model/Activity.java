package com.nessSmart.mapping.database.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Activity {

	@JsonProperty
	private String activityId;

	@JsonProperty
	private String activityDescription;

	@JsonProperty
	private String keyFindings;

	public String getKeyFindings() {
		return keyFindings;
	}

	public void setKeyFindings(String keyFindings) {
		this.keyFindings = keyFindings;
	}

	public String getActivityId() {
		return activityId;
	}

	public void setActivityId(String activityId) {
		this.activityId = activityId;
	}

	public String getActivityDescription() {
		return activityDescription;
	}

	public void setActivityDescription(String activityDescription) {
		this.activityDescription = activityDescription;
	}

}
