package com.nessSmart.mapping.database.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Section {

	@JsonProperty
	private String title;

	@JsonProperty
	private String projectType;

	@JsonProperty
	private List<Activity> activityList;

	@JsonProperty
	private List<Level> levelList;

	@JsonProperty
	private String stakeholderInterviews;
	
	@JsonProperty
	private String keyFindings;

	public String getKeyFindings() {
		return keyFindings;
	}

	public void setKeyFindings(String keyFindings) {
		this.keyFindings = keyFindings;
	}

	public String getStakeholderInterviews() {
		return stakeholderInterviews;
	}

	public void setStakeholderInterviews(String stakeholderInterviews) {
		this.stakeholderInterviews = stakeholderInterviews;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getProjectType() {
		return projectType;
	}

	public void setProjectType(String projectType) {
		this.projectType = projectType;
	}

	public List<Activity> getActivityList() {
		return activityList;
	}

	public void setActivityList(List<Activity> activityList) {
		this.activityList = activityList;
	}

	public List<Level> getLevelList() {
		return levelList;
	}

	public void setLevelList(List<Level> levelList) {
		this.levelList = levelList;
	}

}
