package com.nessSmart.mapping.database.model;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.nessSmart.mapping.json.model.ProductionLogsAnalysis;

@Document
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductionLogsAnalysisReport {
	@Id
	private String projectName;
	private List<ProductionLogsAnalysis> productionLogsAnalysisList;

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public List<ProductionLogsAnalysis> getProductionLogsAnalysisList() {
		return productionLogsAnalysisList;
	}

	public void setProductionLogsAnalysisList(List<ProductionLogsAnalysis> productionLogsAnalysisList) {
		this.productionLogsAnalysisList = productionLogsAnalysisList;
	}
}
