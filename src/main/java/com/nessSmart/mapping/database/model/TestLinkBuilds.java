package com.nessSmart.mapping.database.model;

import org.springframework.data.annotation.Id;

public class TestLinkBuilds {

	private String buildId;
	private String buildCreateDate;
	private String testExecutionId;
	private String testExecutionType;
	private String testExecutionStatus;

	public String getTestExecutionId() {
		return testExecutionId;
	}

	public void setTestExecutionId(String testExecutionId) {
		this.testExecutionId = testExecutionId;
	}

	public String getTestExecutionType() {
		return testExecutionType;
	}

	public void setTestExecutionType(String testExecutionType) {
		this.testExecutionType = testExecutionType;
	}

	public String getTestExecutionStatus() {
		return testExecutionStatus;
	}

	public void setTestExecutionStatus(String testExecutionStatus) {
		this.testExecutionStatus = testExecutionStatus;
	}

	public String getBuildId() {
		return buildId;
	}

	public void setBuildId(String buildId) {
		this.buildId = buildId;
	}

	public String getBuildCreateDate() {
		return buildCreateDate;
	}

	public void setBuildCreateDate(String buildCreateDate) {
		this.buildCreateDate = buildCreateDate;
	}

}
