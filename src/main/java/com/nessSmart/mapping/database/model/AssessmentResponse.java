package com.nessSmart.mapping.database.model;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@Document
@JsonIgnoreProperties(ignoreUnknown = true)
public class AssessmentResponse {

	@Id
	@JsonProperty
	private String assessmentResponseId;

	@JsonProperty
	private String title;

	@JsonProperty
	private String userId;

	@JsonProperty
	private List<Section> sectionList;

	private String totalScore;

	public String getTotalScore() {
		return totalScore;
	}

	public void setTotalScore(String totalScore) {
		this.totalScore = totalScore;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public List<Section> getSectionList() {
		return sectionList;
	}

	public void setSectionList(List<Section> sectionList) {
		this.sectionList = sectionList;
	}

	public String getAssessmentResponseId() {
		return assessmentResponseId;
	}

	public void setAssessmentResponseId(String assessmentResponseId) {
		this.assessmentResponseId = assessmentResponseId;
	}

}
