package com.nessSmart.mapping.database.model;

import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class FunctionalTestStatusReport {
	private String environment;
	private String buildVersion;
	private int failCount;
	private int passCount;
	private String color;
	private long timestamp;
	private String upstreamBuildVersion;
	private String upstreamBuildNumber;
	private String upstreamProjectName;

	public String getEnvironment() {
		return environment;
	}

	public void setEnvironment(String environment) {
		this.environment = environment;
	}

	public String getBuildVersion() {
		return buildVersion;
	}

	public void setBuildVersion(String buildVersion) {
		this.buildVersion = buildVersion;
	}

	public int getFailCount() {
		return failCount;
	}

	public void setFailCount(int failCount) {
		this.failCount = failCount;
	}

	public int getPassCount() {
		return passCount;
	}

	public void setPassCount(int passCount) {
		this.passCount = passCount;
	}

	public long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getUpstreamBuildVersion() {
		return upstreamBuildVersion;
	}

	public void setUpstreamBuildVersion(String upstreamBuildVersion) {
		this.upstreamBuildVersion = upstreamBuildVersion;
	}

	public String getUpstreamBuildNumber() {
		return upstreamBuildNumber;
	}

	public void setUpstreamBuildNumber(String upstreamBuildNumber) {
		this.upstreamBuildNumber = upstreamBuildNumber;
	}

	public String getUpstreamProjectName() {
		return upstreamProjectName;
	}

	public void setUpstreamProjectName(String upstreamProjectName) {
		this.upstreamProjectName = upstreamProjectName;
	}

}
