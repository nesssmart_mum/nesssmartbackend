package com.nessSmart.mapping.database.model;

import org.springframework.data.annotation.Id;

public class TestLinkData {
	
	@Id
	private String id;
	
	private String buildId;
	private String passCountTC;
	private String failCountTC;
	
	private String totalCountAutomation;
	private String totalCountManual;
	
	private String totalCountTC;
	private String totalAutomationTest;
	private String totalManualTest;

	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getBuildId() {
		return buildId;
	}

	public void setBuildId(String buildId) {
		this.buildId = buildId;
	}

	public String getPassCountTC() {
		return passCountTC;
	}

	public void setPassCountTC(String passCountTC) {
		this.passCountTC = passCountTC;
	}

	public String getFailCountTC() {
		return failCountTC;
	}

	public void setFailCountTC(String failCountTC) {
		this.failCountTC = failCountTC;
	}

	public String getTotalCountTC() {
		return totalCountTC;
	}

	public void setTotalCountTC(String totalCountTC) {
		this.totalCountTC = totalCountTC;
	}

	public String getTotalCountAutomation() {
		return totalCountAutomation;
	}

	public void setTotalCountAutomation(String totalCountAutomation) {
		this.totalCountAutomation = totalCountAutomation;
	}

	public String getTotalCountManual() {
		return totalCountManual;
	}

	public void setTotalCountManual(String totalCountManual) {
		this.totalCountManual = totalCountManual;
	}

	public String getTotalAutomationTest() {
		return totalAutomationTest;
	}

	public void setTotalAutomationTest(String totalAutomationTest) {
		this.totalAutomationTest = totalAutomationTest;
	}

	public String getTotalManualTest() {
		return totalManualTest;
	}

	public void setTotalManualTest(String totalManualTest) {
		this.totalManualTest = totalManualTest;
	}

}
