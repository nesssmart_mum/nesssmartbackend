package com.nessSmart.mapping.database.model;

import java.util.List;

import org.springframework.data.annotation.Id;

public class FunctionalTestStatusResult {
	@Id
	private String projectName;
	List<FunctionalTestStatusReport> functionalTestStatusReports ;
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	public List<FunctionalTestStatusReport> getFunctionalTestStatusReports() {
		return functionalTestStatusReports;
	}
	public void setFunctionalTestStatusReports(List<FunctionalTestStatusReport> functionalTestStatusReports) {
		this.functionalTestStatusReports = functionalTestStatusReports;
	}
	
	

}
