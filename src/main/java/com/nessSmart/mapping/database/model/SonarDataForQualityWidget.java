package com.nessSmart.mapping.database.model;

import java.util.List;

import org.springframework.data.annotation.Id;

import com.nessSmart.mapping.common.model.Issues;
import com.nessSmart.mapping.common.model.Measures;

public class SonarDataForQualityWidget {

	@Id
	private String resourceName;

	private String env;

	private String projectName;

	private List<Issues> issueList;

	private List<Measures> measureList;

	public String getResourceName() {
		return resourceName;
	}

	public void setResourceName(String resourceName) {
		this.resourceName = resourceName;
	}

	public List<Issues> getIssueList() {
		return issueList;
	}

	public void setIssueList(List<Issues> issueList) {
		this.issueList = issueList;
	}

	public List<Measures> getMeasureList() {
		return measureList;
	}

	public void setMeasureList(List<Measures> measureList) {
		this.measureList = measureList;
	}

	public String getEnv() {
		return env;
	}

	public void setEnv(String env) {
		this.env = env;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
}
