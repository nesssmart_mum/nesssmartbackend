package com.nessSmart.mapping.database.model;

import java.util.List;

import org.springframework.data.annotation.Id;

public class TestLink {
	@Id
	private String projectName;
	private List<TestLinkBuilds> testLinkBuilds;

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public List<TestLinkBuilds> getTestLinkBuilds() {
		return testLinkBuilds;
	}

	public void setTestLinkBuilds(List<TestLinkBuilds> testLinkBuilds) {
		this.testLinkBuilds = testLinkBuilds;
	}

}
