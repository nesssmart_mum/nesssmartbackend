package com.nessSmart.mapping.database.model;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class JiraIssueDetails {

	@Id
	private String id;

	private String resourceName;
	private String issueId;
	private String issueKey;

	private String issueTypeId;
	private String issueTypeName;
	private String isSubTask;

	private String timespent;
	private String createdDate;
	private String createDate;
	private String updatedDate;

	private String timeestimate;
	
	private String resolution;
	private String priority;
	private List<String> labels;
	private String issueCreateDate;
	

	public String getResolution() {
		return resolution;
	}

	public void setResolution(String resolution) {
		this.resolution = resolution;
	}

	public String getPriority() {
		return priority;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}

	public List<String> getLabels() {
		return labels;
	}

	public void setLabels(List<String> labels) {
		this.labels = labels;
	}

	public String getResourceName() {
		return resourceName;
	}

	public void setResourceName(String resourceName) {
		this.resourceName = resourceName;
	}

	public String getIssueId() {
		return issueId;
	}

	public void setIssueId(String issueId) {
		this.issueId = issueId;
	}

	public String getIssueKey() {
		return issueKey;
	}

	public void setIssueKey(String issueKey) {
		this.issueKey = issueKey;
	}

	public String getIssueTypeId() {
		return issueTypeId;
	}

	public void setIssueTypeId(String issueTypeId) {
		this.issueTypeId = issueTypeId;
	}

	public String getIssueTypeName() {
		return issueTypeName;
	}

	public void setIssueTypeName(String issueTypeName) {
		this.issueTypeName = issueTypeName;
	}

	public String getIsSubTask() {
		return isSubTask;
	}

	public void setIsSubTask(String isSubTask) {
		this.isSubTask = isSubTask;
	}

	public String getTimespent() {
		return timespent;
	}

	public void setTimespent(String timespent) {
		if (timespent == null) {
			this.timespent = "0";
		} else {
			this.timespent = timespent;
		}

	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public String getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(String updatedDate) {
		this.updatedDate = updatedDate;
	}

	public String getTimeestimate() {
		return timeestimate;
	}

	public void setTimeestimate(String timeestimate) {
		this.timeestimate = timeestimate;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public String getIssueCreateDate() {
		return issueCreateDate;
	}

	public void setIssueCreateDate(String issueCreateDate) {
		this.issueCreateDate = issueCreateDate;
	}

}
