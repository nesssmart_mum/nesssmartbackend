package com.nessSmart.mapping.json.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Fields {

	@JsonProperty
	private IssueType issuetype;
	@JsonProperty
	private String timespent;

	@JsonProperty
	private String timeestimate;
	
	@JsonProperty
	private String timeoriginalestimate;
	
	@JsonProperty
	private String aggregatetimespent;
	
	@JsonProperty
	private String created;
	
	@JsonProperty
	private String updated;
	
	@JsonProperty
	private Resolution resolution;
	
	@JsonProperty
	private Priority priority;
	
	@JsonProperty
	private List<String> labels;


	public Resolution getResolution() {
		return resolution;
	}

	public void setResolution(Resolution resolution) {
		this.resolution = resolution;
	}

	public Priority getPriority() {
		return priority;
	}

	public void setPriority(Priority priority) {
		this.priority = priority;
	}

	public List<String> getLabels() {
		return labels;
	}

	public void setLabels(List<String> labels) {
		this.labels = labels;
	}

	public IssueType getIssuetype() {
		return issuetype;
	}

	public void setIssuetype(IssueType issuetype) {
		this.issuetype = issuetype;
	}

	public String getTimespent() {
		return timespent;
	}

	public void setTimespent(String timespent) {
		this.timespent = timespent;
	}

	public String getTimeestimate() {
		return timeestimate;
	}

	public void setTimeestimate(String timeestimate) {
		this.timeestimate = timeestimate;
	}

	public String getCreated() {
		return created;
	}

	public void setCreated(String created) {
		this.created = created;
	}

	public String getUpdated() {
		return updated;
	}

	public void setUpdated(String updated) {
		this.updated = updated;
	}

	public String getTimeoriginalestimate() {
		return timeoriginalestimate;
	}

	public void setTimeoriginalestimate(String timeoriginalestimate) {
		this.timeoriginalestimate = timeoriginalestimate;
	}

	public String getAggregatetimespent() {
		return aggregatetimespent;
	}

	public void setAggregatetimespent(String aggregatetimespent) {
		this.aggregatetimespent = aggregatetimespent;
	}
	
}
