package com.nessSmart.mapping.json.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class JiraDetails {

	private String projectDetails;

	@JsonProperty
	private int startAt;

	@JsonProperty
	private int maxResults;

	@JsonProperty
	private int total;

	@JsonProperty
	private List<JiraIssues> issues;

	public List<JiraIssues> getIssues() {
		return issues;
	}

	public void setIssues(List<JiraIssues> issues) {
		this.issues = issues;
	}

	public String getProjectDetails() {
		return projectDetails;
	}

	public void setProjectDetails(String projectDetails) {
		this.projectDetails = projectDetails;
	}

	public int getStartAt() {
		return startAt;
	}

	public void setStartAt(int startAt) {
		this.startAt = startAt;
	}

	public int getMaxResults() {
		return maxResults;
	}

	public void setMaxResults(int maxResults) {
		this.maxResults = maxResults;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}
}
