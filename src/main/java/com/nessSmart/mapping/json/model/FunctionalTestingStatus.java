package com.nessSmart.mapping.json.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
@JsonIgnoreProperties(ignoreUnknown = true)
public class FunctionalTestingStatus {
	@JsonProperty
	List<ChildReports> childReports;

	public List<ChildReports> getChildReports() {
		return childReports;
	}

	public void setChildReports(List<ChildReports> childReports) {
		this.childReports = childReports;
	}
	
}
