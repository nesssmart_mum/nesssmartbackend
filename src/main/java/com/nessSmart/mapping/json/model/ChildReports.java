package com.nessSmart.mapping.json.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ChildReports {
@JsonProperty
private Result result;

public Result getResult() {
	return result;
}

public void setResult(Result result) {
	this.result = result;
}


}
