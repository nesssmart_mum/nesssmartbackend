package com.nessSmart.mapping.common.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Actions {

	@JsonProperty
	List<Parameters> parameters;

	@JsonProperty
	List<Causes> causes;

	public List<Parameters> getParameters() {
		return parameters;
	}

	public void setParameters(List<Parameters> parameters) {
		this.parameters = parameters;
	}

	public List<Causes> getCauses() {
		return causes;
	}

	public void setCauses(List<Causes> causes) {
		this.causes = causes;
	}

}
