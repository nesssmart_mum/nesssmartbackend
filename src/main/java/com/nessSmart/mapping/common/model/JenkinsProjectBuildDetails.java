package com.nessSmart.mapping.common.model;

import java.util.List;

import org.springframework.data.annotation.Id;

public class JenkinsProjectBuildDetails {
	@Id
	private String projectName;
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	public List<Builds> getBuilds() {
		return builds;
	}
	public void setBuilds(List<Builds> builds) {
		this.builds = builds;
	}
	
	private List<Builds> builds;
	
	

}
