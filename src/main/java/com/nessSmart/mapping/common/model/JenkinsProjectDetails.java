package com.nessSmart.mapping.common.model;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonProperty;

@Document
public class JenkinsProjectDetails {

	@JsonProperty
	private String _class;
	@JsonProperty
	private List<Builds> builds;

	@Id
	private String projectName;

	private List<Issues> issueList;

	private List<Measures> measureList;
	
	private  String environment;
	
	public String getEnvironment() {
		return environment;
	}

	public void setEnvironment(String environment) {
		this.environment = environment;
	}

	public long getSystime() {
		return systime;
	}

	public void setSystime(long systime) {
		this.systime = systime;
	}

	private long systime;
	
	

	public List<Issues> getIssueList() {
		return issueList;
	}

	public void setIssueList(List<Issues> issueList) {
		this.issueList = issueList;
	}

	public List<Measures> getMeasureList() {
		return measureList;
	}

	public void setMeasureList(List<Measures> measureList) {
		this.measureList = measureList;
	}

	public String get_class() {
		return _class;
	}

	public void set_class(String _class) {
		this._class = _class;
	}

	public List<Builds> getBuilds() {
		return builds;
	}

	public void setBuilds(List<Builds> builds) {
		this.builds = builds;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	@JsonProperty
	private List<Jobs> jobs;

	public List<Jobs> getJobs() {
		return jobs;
	}

	public void setJobs(List<Jobs> jobs) {
		this.jobs = jobs;
	}
	
	private List<Modules> modules;
	public List<Modules> getModules() {
		return modules;
	}
	public void setModules(List<Modules> modules) {
		this.modules = modules;
	}

}
