package com.nessSmart.mapping.common.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Jobs {
	@JsonProperty
	private String _class;
	@JsonProperty
	private String name;
	@JsonProperty
	private String url;
	@JsonProperty
	private List<Builds> builds;
	
	
	public List<Builds> getBuilds() {
		return builds;
	}
	public void setBuilds(List<Builds> builds) {
		this.builds = builds;
	}
	public String get_class() {
		return _class;
	}
	public void set_class(String _class) {
		this._class = _class;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}

}
