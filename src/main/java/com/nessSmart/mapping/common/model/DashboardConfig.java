package com.nessSmart.mapping.common.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Document
@JsonIgnoreProperties(ignoreUnknown = true)
public class DashboardConfig {
	@Id
	private String dashboardConfigMappingId;
	private String userId;
	private String columnCount;
	private String theme;

	
	public String getDashboardConfigMappingId() {
		return dashboardConfigMappingId;
	}

	public void setDashboardConfigMappingId(String dashboardConfigMappingId) {
		this.dashboardConfigMappingId = dashboardConfigMappingId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getColumnCount() {
		return columnCount;
	}

	public void setColumnCount(String columnCount) {
		this.columnCount = columnCount;
	}

	public String getTheme() {
		return theme;
	}

	public void setTheme(String theme) {
		this.theme = theme;
	}

}
