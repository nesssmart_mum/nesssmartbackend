package com.nessSmart.mapping.common.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Causes {
	@JsonProperty
	String upstreamBuild;
	@JsonProperty
	String upstreamProject;
	@JsonProperty
	String upstreamUrl;
	public String getUpstreamBuild() {
		return upstreamBuild;
	}
	public void setUpstreamBuild(String upstreamBuild) {
		this.upstreamBuild = upstreamBuild;
	}
	public String getUpstreamProject() {
		return upstreamProject;
	}
	public void setUpstreamProject(String upstreamProject) {
		this.upstreamProject = upstreamProject;
	}
	public String getUpstreamUrl() {
		return upstreamUrl;
	}
	public void setUpstreamUrl(String upstreamUrl) {
		this.upstreamUrl = upstreamUrl;
	}
	
	
	

}
