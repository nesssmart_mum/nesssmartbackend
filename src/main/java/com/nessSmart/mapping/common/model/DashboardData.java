package com.nessSmart.mapping.common.model;

import java.util.List;

import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Document
@JsonIgnoreProperties(ignoreUnknown = true)
public class DashboardData {

	private String userID;
	private DashboardConfiguration dashboardConfiguration;
	private List<Dashboard> dashboardList;

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public DashboardConfiguration getDashboardConfiguration() {
		return dashboardConfiguration;
	}

	public void setDashboardConfiguration(DashboardConfiguration dashboardConfiguration) {
		this.dashboardConfiguration = dashboardConfiguration;
	}

	public List<Dashboard> getDashboardList() {
		return dashboardList;
	}

	public void setDashboardList(List<Dashboard> dashboardList) {
		this.dashboardList = dashboardList;
	}

}
