package com.nessSmart.Constants;

public interface Constants {

	String ASSESSMENT_ID = "assessmentId";
	String ID = "id";
	String ASSESSMENT_RESPONSE_ID = "assessmentResponseId";
	String PROJECTTYPE = "projectType";
	String PROJECTID = "projectId";
	String PROJECTNAME = "projectName";
	String RESOURCENAME = "resourceName";
	String RESOURCEID = "resourceId";
	String CREATED_DATE = "createdDate";
	String CREATE_DATE = "createDate";
	String UP = "up";
	String DOWN = "down";
	String TRUE = "true";
	String RECOVERY = "Recovery";
	String SUCCESS = "SUCCESS";
	String COVERAGE = "coverage";
	String BUILD_SUCCESS_RATIO = "Build Success Ratio";
	String UNIT_TEST_COVERAGE = "Unit Test Coverage";
	String DATE_FORMAT_JIRA = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";
	String DATE_FORMAT_NO_TIME = "yyyy-MM-dd";
	String RESOLUTION = "resolution";
	String UNRESOLVED = "unresolved";
	String RESOLVED = "resolved";
	String PRIORITY = "priority";
	String LABELS = "labels";
	String RED = "Red";
	String GREEN = "Green";
	String AMBER = "Amber";
	String DASHBOARD_ID = "dashboardId";
	String USER_ID = "userId";
	String NAME = "name";
	String PERCENTAGE = "Percentage";
	String HOURS = "Hours";
	
	// Rest Authorization Constants
	String AUTHORIZATION = "Authorization";
	String USERNAME = "username";
	String PASSWORD = "password";

	// Symbols Constants
	String COLON = ":";
	String EQUALS = "=";
	String QUESTION_MARK = "?";
	String AND = "&";
	String DASH = "-";
	String T = "T";

	// URL Constants
	String ENDURL = "endUrl";
	String ENDPOINT_URL = "endPointURL";

	// DevelopmentPipelineTrendWidget
	String DEV_ENV = "dev_env";
	String PROD_ENV = "prod_env";
	String PROD = "PROD";
	String ENV = "env";
	String URL = "url";

	String ENVIRONMENT = "env";
	String DATE = "DATE";
	String DATE_FORMATE = "buildDeployeDate";

	String PIPLELINE_NAME = "pipelineName";
	String PIPELINE = "pipeline";

	String START_TIME_APPEND = "T00:00:01.000+0530";
	String END_TIME_APPEND = "T23:59:59.000+0530";
	String TIME_ZONE = ".000+0530";

	String NCLOC = "ncloc";
	String NUMBER_OF_LINES_COVERED = "Number Of Lines Covered";
	String DUPLICATED_LINES = "Duplicated Lines";
	String DUPLICATED_LINES_DENSITY = "duplicated_lines_density";
	String CRITICAL_ISSUES = "Critical Issues";
	String BLOCKER_ISSUES = "Blocker Issues";
	String CRITICAL = "CRITICAL";
	String BLOCKER = "BLOCKER";

	String ISSUES = "issues";
	String MEASURES = "measures";
	String RECOVERY_TIMESPENT = "Average Recovery Time";
	String CHANGE_FAILURE_RATE_IN_PRODUCTION = "Change Failure Rate in Production";
	String ISSUE_TYPE_NAME = "issueTypeName";
	String SUBTASK = "isSubTask";
	String NO_CHANGES = "nochanges";

	String L1 = "L1";
	String L2 = "L2";
	String L3 = "L3";
	String HIGHEST = "Highest";
	String HIGH = "High";
	String MEDIUM = "Medium";
	String SEVERITY_1 = "Severity 1";
	String SEVERITY_2 = "Severity 2";
	String SEVERITY_3 = "Severity 3";
	String SEVERITY1_L1 = "Severity1_L1";
	String SEVERITY1_L2 = "Severity1_L2";
	String SEVERITY1_L3 = "Severity1_L3";
	String SEVERITY2_L1 = "Severity2_L1";
	String SEVERITY2_L2 = "Severity2_L2";
	String SEVERITY2_L3 = "Severity2_L3";
	String SEVERITY3_L1 = "Severity3_L1";
	String SEVERITY3_L2 = "Severity3_L2";
	String SEVERITY3_L3 = "Severity3_L3";

	String DEV_PROJECTNAME = "dev_projectName";
	String INT_PROJECTNAME = "int_projectName";
	String TEST_PROJECTNAME = "test_projectName";
	String PROD_PROJECTNAME = "prod_projectName";

	String RELEASE_PROD_ENVIRONMENT = "environment";
	String RELEASE_DEPLOYMENT_TYPE = "deploymentType";
	float HUNDRED = 100;

	String MAIN_ATTRIBUTES = "mainAttributes";
	String MANIFEST_VERSION = "Manifest-Version";

	String LEVEL_COMPLETION = "levelCompletion";
	String LEVEL_RATING = "levelRating";
	String LEVEL_MAXSCORE = "levelMaxScore";
	String LEVEL_SCORE = "levelScore";

	String QUESTION_TYPE_3 = "3";
	String QUESTION_TYPE_2 = "2";

	String TITLE = "title";
	String PRODUCES_VALUE = "application/json;charset=utf-8";
	String CONSUMES_VALUE = "application/json";
	String COMPLEXITY = "complexity";
	String CODE_COMPLEXITY = "Code Complexity";
	
	String DATE_FORMAT_JENKINS = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";
	
	String PASS_PERCENTAGE = "Pass Percentage";
	String AUTOMATION_PERCENTAGE = "Automation Percentage";
	String DAYS= "Days";
	String MINUTES= "Minutes";
	String YLABEL = "No. Of Deployments";
	String XLABEL= "xLabel";
	String 	DRILLDOWN ="DRILLDOWN";
	String MONTHLY= "Monthly";
	String DEPLOYMENTS= "Deployments";
	String BUG = "Bug";
	String FALSE = "false";
	String RECOVERY_TIME = "recoverytime";
	String AVERAGE_RECOVER_TIME = "Average Recover Time";
	String AVG_LEAD_TIME_FOR_PROD_CHANGES = "Average Lead Time for Production Changes";
	String[] MONTH_LIST = {"Jan","Feb","Mar","Apr","May","Jun","July","Aug","Sep","Oct","Nov","Dec"};
	String COMPLEXITY_ISSUES = "Code Complexity";
	String PASS_TEST_CASES ="Pass Percentage";
	String AUTOMATED_TEST_CASES ="Automation Percentage";
	String MANUAL_TEST_CASES ="Manual Test Cases";
	String TEST_EXECUTION_PASS_STATUS = "p";
	String TEST_EXECUTION_FAIL_STATUS = "f";
	String TEST_EXECUTION_TYPE_AUTOMATION ="2";
	String TEST_EXECUTION_TYPE_MANUAL ="1";
	String WEEKS= "WEEKS";

}
