package com.nessSmart.Constants;

public interface PropertiesConstants {

	// TestLink Properties Constants
	String TESTLINK_PROPERTIES_FILENAME = "testlink.properties";
	String TESTLINK_ENDPOINTURL = "rest.testlink.endpointurl";
	String TESTLINK_ENDURL = "rest.testlink.endurl";
	String REST_TESTLINK_RESOURCENAME = "rest.testlink.resourcename";

	String TESTLINK_STARTDATE_1 = "rest.testlink.start.date1.build";
	String TESTLINK_ENDDATE_1 = "rest.testlink.end.date1.build";
	String TESTLINK_STARTDATE_2 = "rest.testlink.start.date2.build";
	String TESTLINK_ENDDATE_2 = "rest.testlink.end.date2.build";

	// Jenkins Properties Constants
	String JENKINS_PROPERTIES_FILENAME = "jenkins.properties";
	String JENKINS_ENDPOINTURL = "rest.jenkins.job.endpointurl";
	String JENKINS_USERNAME = "rest.jenkins.job.username";
	String JENKINS_PASSWORD = "rest.jenkins.job.password";
	String JENKINS_JOB_ENDURL = "rest.jenkins.job.endurl";
	String JENKINS_RESOURCENAME = "rest.jenkins.job.resourcename";
	String JENKINS_BUILD_STARTDATE_1 = "start.date1.jenkins.build";
	String JENKINS_BUILD_ENDDATE_1 = "end.date1.jenkins.build";
	String JENKINS_BUILD_STARTDATE_2 = "start.date2.jenkins.build";
	String JENKINS_BUILD_ENDDATE_2 = "end.date2.jenkins.build";
	String JENKINS_JOB_DEPLOMENT_TYPE_ENDURL = "rest.jenkins.job.deploymenttype.endurl";
	String JENKINS_PIPELINE = "pipeline";
	String MIUNSMONTH = "rest.build.numberOfMonths.minus";
	String JENKINS_PROD_DEPLOYMENT_CURRENTMONTH = "rest.jenkins.prod.deployment.current.month";
	String JENKINS_PROD_DEPLOYMENT_LASTMONTH = "rest.jenkins.prod.deployment.last.month";
	// Git Properties Constants
	String GIT_PROPERTIES_FILENAME = "";
	String GIT_ENDPOINTURL = "";
	String GIT_ENDURL = "";

	// Jira Properties Constants
	String JIRA_PROPERTIES_FILENAME = "jira.properties";
	String JIRA_ENDPOINTURL = "rest.jira.endpointurl";
	String JIRA_USERNAME = "rest.jira.username";
	String JIRA_PASSWORD = "rest.jira.password";
	String JIRA_STARTDATE_1 = "start.date1.jira";
	String JIRA_ENDDATE_1 = "end.date1.jira";
	String JIRA_STARTDATE_2 = "start.date2.jira";
	String JIRA_ENDDATE_2 = "end.date2.jira";
	String JIRA_RELEASE_START_DATE = "start.date.release.jira";
	String JIRA_RELEASE_END_DATE = "end.date.release.jira";
	String JIRA_RESOURCENAME = "rest.jira.resourcename";
	String JIRA_ISSUES_ENDURL = "rest.jira.issues.endurl";
	String JIRA_VERSIONS_ENDURL = "rest.jira.versions.endurl";
	String JIRA_GET_PROJECT_DETAILS_ENDURL = "rest.jira.project.details.endurl";

	String JIRA_RECOVERY_STARTDATE_1 = "start.recovery.date1.jira";
	String JIRA_RECOVERY_ENDDATE_1 = "end.recovery.date1.jira";
	String JIRA_RECOVERY_STARTDATE_2 = "start.recovery.date2.jira";
	String JIRA_RECOVERY_ENDDATE_2 = "end.recovery.date2.jira";

	// Sonarqube Properties Constants
	String SONARQUBE_PROPERTIES_FILENAME = "jenkins.properties";
	String SONARQUBE_ENDPOINTURL = "rest.jenkins.sonarqube.endpointurl";
	String SONARQUBE_USERNAME = "rest.sonarqube.username";
	String SONARQUBE_PASSWORD = "rest.sonarqube.password";
	String SONARQUBE_UNITTEST_COVERAGE_ENDURL = "rest.sonarqube.unitTestCoverage.endurl";
	String SONARQUBE_ISSUES_ENDURL = "rest.sonarqube.issue.endurl";
	String SONARQUBE_RESOURCENAME = "rest.sonarqube.resourcename";

	String JENKINS_JOB_SUCCESSFUL_BUILD_ENDURL = "rest.jenkins.job.lastSuccessfulBuild.endurl";

	String JENKINS_JOB_FUNCTIONAL_TEST_PROJECTNAME_ENDUR = "rest.jenkins.job.fuctional.test.endurl";

	String JENKINS_JOB_LAST_STABLE__BUIDS_DISPLAY_NAME_ENDUR = "rest.jenkins.job.lastStableBuildDisplayname.endurl";
	String JENKINS_UPSTEAMPROJECT_ENDUR = "rest.jenkins.job.upstreamProjects.endurl";

	String JENKINS_PROD_RESOURCENAME = "rest.jenkins.production.job.resourcename";

	String JENKINS_VIEW_ENDURL = "rest.jenkins.view.endurl";
	String JENKINS_PIPELINE_RESOURCENAME = "rest.jenkins.pipeline.view.resourcename";
	String JENKINS_VIEW_JOB_ENDURL = "rest.jenkins.view.job.endurl";

	String JENKINS_JOB_PROJECTNAME_ENDURL = "rest.jenkins.job.projectname.endurl";

	// Manifest
	String MANIFEST_ENDPOINTURL = "rest.manifest.job.endpointurl";
	String MANIFEST_JOB_ENDURL = "rest.manifest.endurl";
	String MANIFEST_ENV = "manifest.env";

	// Release Status Widget
	String RELEASE_STATUS_PROPERTIES_FILENAME = "proddeploymentstatus.properties";
	String RELEASE_BUILD_PARAMETER_ROLLBACK = "rest.build.parmameter.rollback";
	String RELEASE_BUILD_PARAMETER_HOT = "rest.build.parmameter.hot";
	String RELEASE_BUILD_PARAMETER_PATCH = "rest.build.parmameter.patch";
	String RELEASE_BUILD_PARAMETER_RELEASE = "rest.build.parmameter.release";
	String RELEASE_STARTDATE_1 = "start.date1.prod";
	String RELEASE_ENDDATE_1 = "end.date1.prod";
	String RELEASE_STARTDATE_2 = "start.date2.prod";
	String RELEASE_ENDDATE_2 = "end.date2.prod";
	String RELEASE_FAILURE_RATE_PROD_ENV = "rest.build.env.prod";
	String PROD_RESOURCENAME = "rest.jenkines.prod.job.resourcename";
	String TEST_RESOURCENAME = "rest.jenkines.test.job.resourcename";
	String INT_RESOURCENAME = "rest.jenkines.int.job.resourcename";
	String DEV_RESOURCENAME = "rest.jenkines.dev.job.resourcename";

	String DELAY_PERIOD_DAYS_JIRA = "delay.period.days.jira";

	String PRODUCTIONS_LOGS_EXCEPTIONS = "production.logs.exceptions";
	String PRODUCTIONS_LOGS_PROPERTIES_FILENAME = "productionLogsAnalysis.properties";
	String PRODUCTIONS_LOGS_RESOURCENAME = "production.logs.resourcename";
	String PRODUCTION_LOGS_PATH = "production.logs.path";
	String PROJECT_NAME = "projectName";
	String JIRA_PROJECT_NAME = "rest.jira.projectname";
	String MINUSDAYS = "testlink.numberOfDays.minus";
	
	String FAILURE_RELEASE_STARTDATE_1 = "failure.start.date1.prod";
	String FAILURE_RELEASE_ENDDATE_1 = "failure.end.date1.prod";
	String FAILURE_RELEASE_STARTDATE_2 = "failure.start.date2.prod";
	String FAILURE_RELEASE_ENDDATE_2 = "failure.end.date2.prod";

}
