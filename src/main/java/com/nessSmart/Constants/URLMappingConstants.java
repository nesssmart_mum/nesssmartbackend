package com.nessSmart.Constants;

public interface URLMappingConstants {
	
	// CONTROLLER URL MAPPINGS
	
	String DEVELOPMENT_WIDGET_MAPPING = "/nessSmart/developmentPipelineTrendWidget";
	String RELEASE_STATUS_WIDGET_MAPPING = "/nessSmart/releasestatusWidget";
	String PRODUCTION_REPORTING_WIDGET_MAPPING = "/nessSmart/productionReportingWidget";
	String QUALITY_WIDGET_MAPPING = "/nessSmart/qualityWidget";
	String ASSESSMENT_MAPPING = "/nessSmart/assessment";
	String DASHBOARD_MAPPING = "/nessSmart/dashboard";
	
	// AssessmentController
	String DELETE_ASSESSMENT_ENDURL = "/deleteAssessment";
	String SAVE_ASSESSMENT_ENDURL = "/saveAssessment";
	String GET_ASSESSMENT_BY_ID_ENDURL = "/getAssessmentById";
	String GET_ASSESSMENT_TITLES = "/getAssessmentTitles";
	String SAVE_QUESTIONBANK_ENDURL = "/saveQuestionBank";
	String GET_QUESTIONBANK_ENDURL = "/getQuestionBank";
	String SAVE_ASSESSMENT_RESPONSE_ENDURL = "/saveAssessmentResponse";
	String GET_ASSESSMENT_RESPONSE_REPORT_BY_ID_ENDURL = "/getAssessmentReportById";
	String GET_ASSESSMENT_RESPONSE_BY_ID_ENDURL = "/getAssessmentResponseById";
	String GET_ASSESSMENT_RESPONSE_MAPPING_ENDURL = "/getAssessmentResponseMapping";
	String DELETE_ASSESSMENT_RESPONSE_ENDURL = "/deleteAssessmentResponse";

	// DashBoardController
	String SAVE_USER_DETAILS = "/saveUserDetails";
	String UPDATE_USER_DETAILS = "/updateUserDetails";
	String SAVE_WIDGETS_MASTER_DETAILS = "/saveWidgetsMasterDetails";
	String GET_DASHBOARD_WIDGETS__DETAILS = "/getAllWidgets";
	String GET_ALL_USER__DETAILS = "/getAllUserDetails";
	String SAVE_DASHBOARD_APIS = "/saveDashboard";
	String DELETE_DASHBOARD_APIS = "/deleteDashboard";
	String GET_DASHBOARD_APIS = "/getDashboard";

	// QualityTrendWidgetController
	String GET_TESTLINK_DATA_ENDURL = "/testlink";
	String JENKINS_SUCCESS_RATIO = "/jenkinsSuccessRatio";
	String JENKINS_SONAR = "/jenkinsSonarData";

	// DevelopmentPipelineTrendWidgetController
	String GET_JENKINS_AVERAGE_LEAD_TIME_FOR_PRODUCTION = "/getAverageLeadTimeforProductionChanges";
	String GET_JIRA_PRODUCTION_RECOVERY_TIME = "/getJiraRecoveryTimeSpent";
	String GET_PRODUCTION_FAILURE_RATE = "/getProductionFailureRate";
	String GET_JENKINS_PRODUCTION_DEPLOYMENT_FREQUENCY = "/getProductionDeploymentFrequency";

	// ReleaseStatusWidgetController
	String GET_JIRA_RELEASE_STATUS = "/getJiraReleaseStatus";
	String GET_FUNTIONAL_TEST_STATUS = "/getFunctionalTestStatus";
	String GET_RELEASE_MANIFEST_MAPPING = "/getReleaseManifestMapping";

	// ProductionReportingWidgetController
	String GET_JIRA_ISSUES = "/getJiraIssues";
	String GET_PRODUCTION_LOGS_ANALYSIS = "/getProductionLogsAnalysis";
	String READ_PRODUCTION_LOGS_ANALYSIS = "/readProductionLogsAnalysis";
	String GET_JENKINS_PRODUCTION_DEPLOYMENT_FREQUENCY_DRILL_DOWN = "/getProductionDeploymentDrillDown";
	String SAVE_TESTLINK_YEAR_DATA = "/saveTestlinkYearData";
	
}
