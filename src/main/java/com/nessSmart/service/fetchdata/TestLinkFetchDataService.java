package com.nessSmart.service.fetchdata;

import java.io.IOException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.NumberUtils;

import com.mongodb.client.model.Projections;
import com.nessSmart.Constants.Constants;
import com.nessSmart.Constants.PropertiesConstants;
import com.nessSmart.dao.MongoStorage;
import com.nessSmart.mapping.common.model.Builds;
import com.nessSmart.mapping.database.model.TestLink;
import com.nessSmart.mapping.database.model.TestLinkBuilds;
import com.nessSmart.mapping.database.model.TestLinkData;
import com.nessSmart.mapping.response.model.DrillDown;
import com.nessSmart.mapping.response.model.DrillDownData;
import com.nessSmart.mapping.response.model.Sections;
import com.nessSmart.mapping.response.model.WidgetDataAndDrillDown;
import com.nessSmart.mapping.response.model.WidgetResponse;
import com.nessSmart.utils.CommonUtility;
import com.nessSmart.utils.LoadProperties;

@Service
public class TestLinkFetchDataService {

	@Autowired
	MongoStorage storage;

	@Autowired
	LoadProperties propertyLoader;

	public WidgetDataAndDrillDown fetchTestLinkData() {
		Map<String, String> filterMap = new HashMap<String, String>();
		List<TestLink> testLinkDataList = storage.findWithFilters(filterMap, TestLink.class);
		WidgetDataAndDrillDown widgetDataAndDrillDown = new WidgetDataAndDrillDown();
		try {
			Properties properties = propertyLoader.loadPropertiesFile(PropertiesConstants.TESTLINK_PROPERTIES_FILENAME);
			filterMap.put(Constants.PROJECTNAME,
					properties.getProperty(PropertiesConstants.REST_TESTLINK_RESOURCENAME));

			List<TestLinkBuilds> testLinkDataLink = testLinkDataList.get(0).getTestLinkBuilds();

			float currentPassCount = 0;
			float currentFailCount = 0;
			float currentManualCount = 0;
			float currentAutomationCount = 0;

			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
			String currentDateStr = simpleDateFormat.format(new Date());
			// Date currentDate = simpleDateFormat.parse(currentDateStr);
			// Calendar currentDateCal = Calendar.getInstance();
			// currentDateCal.setTime(currentDate);
			String currentDate = properties.getProperty(PropertiesConstants.TESTLINK_STARTDATE_1);

			Date startdate1 = CommonUtility.getDate(Constants.DATE_FORMAT_NO_TIME,
					properties.getProperty(PropertiesConstants.TESTLINK_STARTDATE_1));
			Date enddate1 = CommonUtility.getDate(Constants.DATE_FORMAT_NO_TIME,
					properties.getProperty(PropertiesConstants.TESTLINK_ENDDATE_1));
			Date startdate2 = CommonUtility.getDate(Constants.DATE_FORMAT_NO_TIME,
					properties.getProperty(PropertiesConstants.TESTLINK_STARTDATE_2));
			Date enddate2 = CommonUtility.getDate(Constants.DATE_FORMAT_NO_TIME,
					properties.getProperty(PropertiesConstants.TESTLINK_ENDDATE_2));

			/*
			 * String minusDays =
			 * properties.getProperty(PropertiesConstants.MINUSDAYS); int
			 * numberOfminusDays = Integer.parseInt(minusDays); Calendar cal =
			 * Calendar.getInstance(); cal.setTime(currentDate);
			 * cal.add(Calendar.DATE, -numberOfminusDays); Date dateBefore7Days
			 * = cal.getTime();
			 */
			double currentTotalAPMP = 0;
			double currentTotalPPFP = 0;
			for (TestLinkBuilds build : testLinkDataLink) {
				String createDateStr = build.getBuildCreateDate();
				Date createDate = simpleDateFormat.parse(createDateStr);
				if (build.getTestExecutionStatus() != null && build.getTestExecutionStatus() != "") {
					if (build.getTestExecutionStatus().equalsIgnoreCase(Constants.TEST_EXECUTION_PASS_STATUS)
							|| build.getTestExecutionStatus().equalsIgnoreCase(Constants.TEST_EXECUTION_FAIL_STATUS)) {
						currentTotalPPFP = currentTotalPPFP + 1;
					}
				}
				if (build.getTestExecutionType() != null && build.getTestExecutionType() != "") {
					if (build.getTestExecutionType().equalsIgnoreCase(Constants.TEST_EXECUTION_TYPE_AUTOMATION)||build.getTestExecutionType()
							.equalsIgnoreCase(Constants.TEST_EXECUTION_TYPE_MANUAL)) {
						currentTotalAPMP = currentTotalAPMP + 1;
					}
				}
				if (/*
					 * CommonUtility.isDateBetween(createDate, dateBefore7Days,
					 * currentDate)
					 */
				CommonUtility.isDateBetween(createDate, startdate1, enddate1)) {

					if (build.getTestExecutionStatus() != null && build.getTestExecutionStatus() != "") {
						if (build.getTestExecutionStatus().equalsIgnoreCase(Constants.TEST_EXECUTION_PASS_STATUS)) {
							currentPassCount = currentPassCount + 1;
						} else if (build.getTestExecutionStatus()
								.equalsIgnoreCase(Constants.TEST_EXECUTION_FAIL_STATUS)) {
							currentFailCount = currentFailCount + 1;
						}
					}
					if (build.getTestExecutionType() != null && build.getTestExecutionType() != "") {
						if (build.getTestExecutionType().equalsIgnoreCase(Constants.TEST_EXECUTION_TYPE_AUTOMATION)) {
							currentAutomationCount = currentAutomationCount + 1;
						} else if (build.getTestExecutionType()
								.equalsIgnoreCase(Constants.TEST_EXECUTION_TYPE_MANUAL)) {
							currentManualCount = currentManualCount + 1;
						}
					}

				}
			}
			double currentPassPercentage = 0;
			double currentAutomationPercentage = 0;

			if (currentPassCount == 0 && currentFailCount == 0) {
				currentPassPercentage = 0;
			} else {
				currentPassPercentage = (float) (currentPassCount / (currentPassCount + currentFailCount)) * 100;
				currentPassPercentage = Math.round(currentPassPercentage * 100.0) / 100.0;
			}
			if (currentAutomationCount == 0 && currentManualCount == 0) {
				currentAutomationPercentage = 0;
			} else {
				currentAutomationPercentage = (currentAutomationCount / (currentAutomationCount + currentManualCount))
						* 100;
				currentAutomationPercentage = Math.round(currentAutomationPercentage * 100) / 100;
			}

			/*
			 * Calendar calPre = Calendar.getInstance();
			 * calPre.setTime(dateBefore7Days); calPre.add(Calendar.DATE,
			 * -numberOfminusDays); Date calPreWeek = calPre.getTime();
			 */

			float prePassCount = 0;
			float preFailCount = 0;
			float preManualCount = 0;
			float preAutomationCount = 0;
			for (TestLinkBuilds prebuild : testLinkDataLink) {
				String createDateStr = prebuild.getBuildCreateDate();
				Date createDate = simpleDateFormat.parse(createDateStr);
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(createDate);

				if (/*
					 * CommonUtility.isDateBetween(createDate, calPreWeek,
					 * dateBefore7Days)
					 */
				CommonUtility.isDateBetween(createDate, startdate2, enddate2)) {

					if (prebuild.getTestExecutionStatus() != null && prebuild.getTestExecutionStatus() != "") {
						if (prebuild.getTestExecutionStatus().equalsIgnoreCase(Constants.TEST_EXECUTION_PASS_STATUS)) {
							prePassCount = prePassCount + 1;
						} else if (prebuild.getTestExecutionStatus()
								.equalsIgnoreCase(Constants.TEST_EXECUTION_FAIL_STATUS)) {
							preFailCount = preFailCount + 1;
						}
					}
					if (prebuild.getTestExecutionType() != null && prebuild.getTestExecutionType() != "") {
						if (prebuild.getTestExecutionType()
								.equalsIgnoreCase(Constants.TEST_EXECUTION_TYPE_AUTOMATION)) {
							preAutomationCount = preAutomationCount + 1;
						} else if (prebuild.getTestExecutionType()
								.equalsIgnoreCase(Constants.TEST_EXECUTION_TYPE_MANUAL)) {
							preManualCount = preManualCount + 1;
						}
					}

				}

			}

			double prePassPercentage = 0;
			double preAutomationPercentage = 0;
			double preTotalAPMP = 0;
			double preTotalPPFP = 0;
			if (prePassCount == 0 && preFailCount == 0) {
				prePassPercentage = 0;
			} else {

				prePassPercentage = (prePassCount / (prePassCount + preFailCount)) * 100;
				prePassPercentage = Math.round(prePassPercentage * 100.0) / 100.0;
			}
			if (preAutomationCount == 0 && preManualCount == 0) {
				preAutomationPercentage = 0;
			} else {

				preAutomationPercentage = (preAutomationCount / (preAutomationCount + preManualCount)) * 100;
				preAutomationPercentage = Math.round(preAutomationPercentage * 100.0) / 100.0;
			}

			WidgetResponse passTestCases = new WidgetResponse();
			passTestCases.setName(Constants.PASS_PERCENTAGE);
			passTestCases.setValue(Double.toString(currentPassPercentage));

			if (currentPassPercentage > prePassPercentage) {
				passTestCases.setArrowValue(Constants.UP);
			} else {
				passTestCases.setArrowValue(Constants.DOWN);
			}
			passTestCases.setUnitValue(Constants.PERCENTAGE);

			WidgetResponse automationTestCases = new WidgetResponse();
			automationTestCases.setName(Constants.AUTOMATION_PERCENTAGE);
			automationTestCases.setValue(Double.toString(currentAutomationPercentage));
			if (currentAutomationPercentage > preAutomationPercentage) {
				automationTestCases.setArrowValue(Constants.UP);
			} else {
				automationTestCases.setArrowValue(Constants.DOWN);
			}
			automationTestCases.setUnitValue(Constants.PERCENTAGE);

			List<WidgetResponse> qualityWidgetResponseList = new ArrayList<WidgetResponse>();
			qualityWidgetResponseList.add(passTestCases);
			qualityWidgetResponseList.add(automationTestCases);
			DrillDown drillDown = fetchTestLinkDataDrillDown(currentTotalAPMP, currentTotalPPFP);

			widgetDataAndDrillDown.setDrillDown(drillDown);
			widgetDataAndDrillDown.setWidgetData(qualityWidgetResponseList);

		} catch (ParseException | IOException e) {
			e.printStackTrace();
		}
		return widgetDataAndDrillDown;
	}

	public DrillDown fetchTestLinkDataDrillDown(double currentTotalAPMP, double currentTotalPPFP) {
		Properties properties;
		DrillDown drillDown = new DrillDown();
		List<TestLink> testLinkDataList = new ArrayList<>();
		try {
			properties = propertyLoader.loadPropertiesFile(PropertiesConstants.JENKINS_PROPERTIES_FILENAME);

			String projectName = properties.getProperty(PropertiesConstants.PROJECT_NAME);

			Map<String, String> filterMap = new HashMap<String, String>();
			filterMap.put(Constants.PROJECTNAME, projectName);
			testLinkDataList = storage.findWithFilters(filterMap, TestLink.class);
			Map<String, Double> passTestCases = new HashMap<>();
			Map<String, Double> automatedTestCases = new HashMap<>();
			for (TestLink testlink : testLinkDataList) {
				if (testlink.getProjectName().equalsIgnoreCase(projectName)) {
					List<TestLinkBuilds> TestLinkDataList = testlink.getTestLinkBuilds();
					double passTestCaseCount = 0F;
					double automatedTestCaseCount = 0F;
					for (TestLinkBuilds testLinkBuilds : TestLinkDataList) {
						int month = 0;
						String createDateStr = testLinkBuilds.getBuildCreateDate();
						DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
						Date createDate = (Date) formatter.parse(createDateStr);
						Calendar calendar = Calendar.getInstance();
						calendar.setTime(createDate);
						month = calendar.get(Calendar.MONTH);
						String monthFromMap = properties.getProperty(Integer.toString(month));

						if (testLinkBuilds.getTestExecutionStatus() != null && testLinkBuilds.getTestExecutionStatus()
								.equalsIgnoreCase(Constants.TEST_EXECUTION_PASS_STATUS)) {

							if (passTestCases.get(monthFromMap) != null) {
								passTestCaseCount = passTestCases.get(monthFromMap);
								passTestCaseCount = passTestCaseCount + 1;
								passTestCases.put(monthFromMap, passTestCaseCount);
							} else {
								passTestCases.put(monthFromMap, 1D);
							}
						}
						if (testLinkBuilds.getTestExecutionType() != null && testLinkBuilds.getTestExecutionType()
								.equalsIgnoreCase(Constants.TEST_EXECUTION_TYPE_AUTOMATION)) {

							if (automatedTestCases.get(monthFromMap) != null) {
								automatedTestCaseCount = automatedTestCases.get(monthFromMap);
								automatedTestCaseCount = automatedTestCaseCount + 1;
								automatedTestCases.put(monthFromMap, automatedTestCaseCount);
							} else {
								automatedTestCases.put(monthFromMap, 1D);
							}
						}
					}

				}

			}

			String[] monthList = Constants.MONTH_LIST;
			List<DrillDownData> drillDownDataList = new ArrayList<>();
			for (String month : monthList) {
				Double passTestCase = passTestCases.get(month);
				Double automatedTestCase = automatedTestCases.get(month);

				drillDown.setxLabel("Year 2017");
				drillDown.setyLabel("Number");
				List<Sections> sectionList = new ArrayList<>();
				Sections passSections = new Sections();
				passSections.setTitle(Constants.PASS_PERCENTAGE);
				if (passTestCase != null) {
					passTestCase = (passTestCase / currentTotalPPFP) * 100;
					passTestCase = Math.round(passTestCase*100.0)/100.0;
					passSections.setValue(passTestCase);
				} else {
					passSections.setValue(0D);
				}
				sectionList.add(passSections);

				Sections automatedSections = new Sections();
				automatedSections.setTitle(Constants.AUTOMATION_PERCENTAGE);
				if (automatedTestCase != null) {
					automatedTestCase = (automatedTestCase / currentTotalAPMP) * 100;
					automatedTestCase = Math.round(automatedTestCase*100.0)/100.0;
					automatedSections.setValue(automatedTestCase);

				} else {
					automatedSections.setValue(0D);
				}
				sectionList.add(automatedSections);

				DrillDownData drillDownData = new DrillDownData();
				drillDownData.setName(month);
				drillDownData.setSections(sectionList);
				drillDownDataList.add(drillDownData);
				drillDown.setDrillDownData(drillDownDataList);
			}

		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {

			e.printStackTrace();
		}

		return drillDown;
	}

}
