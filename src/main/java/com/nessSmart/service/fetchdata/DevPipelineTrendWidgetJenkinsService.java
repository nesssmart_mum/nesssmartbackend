package com.nessSmart.service.fetchdata;

import java.io.IOException;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.info.ProjectInfoProperties.Build;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.util.SystemPropertyUtils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.nessSmart.Constants.Constants;
import com.nessSmart.Constants.PropertiesConstants;
import com.nessSmart.dao.MongoStorage;
import com.nessSmart.mapping.common.model.Actions;
import com.nessSmart.mapping.common.model.Builds;
import com.nessSmart.mapping.common.model.JenkinsPipelineDetails;
import com.nessSmart.mapping.common.model.JenkinsProjectBuildDetails;
import com.nessSmart.mapping.common.model.JenkinsProjectDetails;
import com.nessSmart.mapping.common.model.Jobs;
import com.nessSmart.mapping.common.model.Modules;
import com.nessSmart.mapping.common.model.Parameters;
import com.nessSmart.mapping.database.model.FunctionalTestStatusReport;
import com.nessSmart.mapping.database.model.FunctionalTestStatusResult;
import com.nessSmart.mapping.json.model.ChildReports;
import com.nessSmart.mapping.json.model.FunctionalTestingStatus;
import com.nessSmart.mapping.response.model.DrillDownData;
import com.nessSmart.mapping.response.model.DrillDownLeadTimeForChanges;
import com.nessSmart.mapping.response.model.Sections;
import com.nessSmart.mapping.response.model.VersionData;
import com.nessSmart.mapping.response.model.DevelopmentPipelineTrendWidget;
import com.nessSmart.mapping.response.model.DrillDown;

import com.nessSmart.utils.CommonUtility;
import com.nessSmart.utils.LoadProperties;
import com.nessSmart.utils.RestTemplateUtil;

@Service
public class DevPipelineTrendWidgetJenkinsService {

	@Autowired
	LoadProperties propertyLoader;

	@Autowired
	RestTemplateUtil restService;

	@Autowired
	MongoStorage storage;

	private Map<String, String> createJenkinsParametersForBuildData() throws IOException {
		Map<String, String> jenkinsParametersMap = new HashMap<String, String>();
		return jenkinsParametersMap;
	}

	public JenkinsPipelineDetails saveJenkinsPipelineDetails() {
		String projectName = "";
		String pipelineName = "";
		String jenkinsPipelineDetails = "";
		try {
			Properties jenkinsProperties = propertyLoader
					.loadPropertiesFile(PropertiesConstants.JENKINS_PROPERTIES_FILENAME);
			projectName = jenkinsProperties.getProperty(PropertiesConstants.PROJECT_NAME);
			pipelineName = jenkinsProperties.getProperty(PropertiesConstants.JENKINS_PIPELINE);

			jenkinsPipelineDetails = getJenkinsPipelineDataFromAPI(pipelineName);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return saveJenkinsPipelineData(jenkinsPipelineDetails, projectName);
	}

	public String getJenkinsPipelineDataFromAPI(String pipelineName) {
		String jsonResponse = null;
		try {
			Properties properties = propertyLoader.loadPropertiesFile(PropertiesConstants.JENKINS_PROPERTIES_FILENAME);
			String endPointUrl = properties.getProperty(PropertiesConstants.JENKINS_ENDPOINTURL);
			String endUrl = properties.getProperty(PropertiesConstants.JENKINS_VIEW_ENDURL);
			String resourceName = properties.getProperty(PropertiesConstants.JENKINS_PIPELINE_RESOURCENAME);
			endUrl = endUrl.replace(Constants.RESOURCENAME, resourceName);
			String userName = properties.getProperty(PropertiesConstants.JENKINS_USERNAME);
			String password = properties.getProperty(PropertiesConstants.JENKINS_PASSWORD);
			jsonResponse = restService.getRestResponseWithCredentials(createJenkinsParametersForBuildData(),
					CommonUtility.getPropertiesMap(endPointUrl, endUrl, userName, password, resourceName),
					HttpMethod.GET);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return jsonResponse;

	}

	/**
	 * 
	 * @param jsonObj
	 * @return
	 */
	private JenkinsPipelineDetails saveJenkinsPipelineData(String jsonObj, String projectName) {
		ObjectMapper mapper = new ObjectMapper();
		JenkinsProjectBuildDetails jenkinsProjectDetails = new JenkinsProjectBuildDetails();
		try {
			JenkinsPipelineDetails pipelineDetails = mapper.readValue(jsonObj, JenkinsPipelineDetails.class);
			List<Jobs> pipelineJobsList = pipelineDetails.getJobs();
			List<Builds> buildsDetailsList = new ArrayList<Builds>();
			for (Jobs pipelineJob : pipelineJobsList) {

				List<Builds> buildsList = getPipelineJobAndEnvironmentBuildDetails(pipelineJob);

				if (jenkinsProjectDetails.getProjectName() == null) {

					jenkinsProjectDetails.setProjectName(getProjectName(pipelineJob.getUrl(), projectName));
				}

				buildsDetailsList.addAll(buildsList);

			}

			jenkinsProjectDetails.setBuilds(buildsDetailsList);
			storage.saveJenkinsProjectDetails(jenkinsProjectDetails);
			return pipelineDetails;
		} catch (IOException e) {
			// TODO Auto generated catch block
			return null;
		}

	}

	private List<Builds> getPipelineJobAndEnvironmentBuildDetails(Jobs pipelineJob) {
		String jsonResponse = null;
		Properties properties;
		ObjectMapper mapper = new ObjectMapper();
		JenkinsProjectDetails jenkinsProjectDetails = null;
		String convertedDate = null;
		try {
			properties = propertyLoader.loadPropertiesFile(PropertiesConstants.JENKINS_PROPERTIES_FILENAME);
			String endPointUrl = pipelineJob.getUrl();
			String endUrl = properties.getProperty(PropertiesConstants.JENKINS_JOB_DEPLOMENT_TYPE_ENDURL);
			String userName = properties.getProperty(PropertiesConstants.JENKINS_USERNAME);
			String password = properties.getProperty(PropertiesConstants.JENKINS_PASSWORD);
			jsonResponse = restService.getRestResponseWithCredentials(createJenkinsParametersForBuildData(),
					CommonUtility.getPropertiesMap(endPointUrl, endUrl, userName, password, ""), HttpMethod.GET);
			jenkinsProjectDetails = mapper.readValue(jsonResponse, JenkinsProjectDetails.class);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		List<Builds> builds = new ArrayList<Builds>();

		for (Builds build : jenkinsProjectDetails.getBuilds()) {
			build.setEnvironment(pipelineJob.getName());
			build.setBuildVersion(build.getDisplayName());
			String buildVersion = build.getDisplayName();
			build.setNumber(build.getId());
			String manifestVersion = buildVersion.substring(0, buildVersion.length() - build.getId().length());
			build.setManifestVersion(manifestVersion);
			convertedDate = CommonUtility.getConvertedDate(build.getTimestamp());
			build.setCreateDate(convertedDate);
			build.setCreatedDate(convertedDate);
			List<Actions> listActions = build.getActions();
			for (Actions action : listActions) {
				List<Parameters> listParameters = action.getParameters();

				if (listParameters != null) {
					for (Parameters parameter : listParameters) {
						build.setDeploymentType(parameter.getValue());

					}
				}

			}
			build.setActions(null);
			builds.add(build);
		}

		return builds;
	}

	private String getProjectName(String endPointUrl, String projectName) {

		String jsonResponse = null;
		Properties properties;
		ObjectMapper mapper = new ObjectMapper();
		JenkinsProjectDetails jenkinsProjectDetails = new JenkinsProjectDetails();
		try {
			properties = propertyLoader.loadPropertiesFile(PropertiesConstants.JENKINS_PROPERTIES_FILENAME);
			String endUrl = properties.getProperty(PropertiesConstants.JENKINS_JOB_PROJECTNAME_ENDURL);
			String userName = properties.getProperty(PropertiesConstants.JENKINS_USERNAME);
			String password = properties.getProperty(PropertiesConstants.JENKINS_PASSWORD);
			jsonResponse = restService.getRestResponseWithCredentials(createJenkinsParametersForBuildData(),
					CommonUtility.getPropertiesMap(endPointUrl, endUrl, userName, password, ""), HttpMethod.GET);
			jenkinsProjectDetails = mapper.readValue(jsonResponse, JenkinsProjectDetails.class);

			List<Modules> molulesList = jenkinsProjectDetails.getModules();
			for (Modules modules : molulesList) {

				if (modules.getDisplayName().equalsIgnoreCase(projectName)) {
					projectName = modules.getDisplayName();

				}
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return projectName;
	}

	public DevelopmentPipelineTrendWidget getAverageLeadTimeforProductionChanges() {

		Map<String, String> filters = new HashMap<String, String>();
		Properties properties;
		DevelopmentPipelineTrendWidget developmentPipelineTrendWidget = new DevelopmentPipelineTrendWidget();

		try {
			properties = propertyLoader.loadPropertiesFile(PropertiesConstants.JENKINS_PROPERTIES_FILENAME);

			String jenkinesProjectName = properties.getProperty(PropertiesConstants.PROJECT_NAME);
			String prodEnvironment = properties.getProperty(PropertiesConstants.JENKINS_PROD_RESOURCENAME);
			String devEnvironment = properties.getProperty(PropertiesConstants.JENKINS_RESOURCENAME);

			filters.put(Constants.PROJECTNAME, jenkinesProjectName);
			// long days = 0;
			List<JenkinsProjectBuildDetails> jenkinsProjectDetails = storage.findWithFilters(filters,
					JenkinsProjectBuildDetails.class);
			List<Builds> totalBuildDetails = jenkinsProjectDetails.get(0).getBuilds();
			// For Latest Production Build
			int laststableBuildNumber = 0;
			String prodBuildVersion = null;
			long prodBuildtimestamp = 0;
			long devBuildtimestamp = 0;
			//long minutes = 0;
			long weeks =0;
			for (Builds build : totalBuildDetails) {

				if (build.getEnvironment().equalsIgnoreCase(prodEnvironment)) {

					laststableBuildNumber = getLastBuildNumber(Integer.parseInt(build.getNumber()),
							laststableBuildNumber);

					if (build.getNumber().equalsIgnoreCase(new Integer(laststableBuildNumber).toString())) {

						prodBuildVersion = build.getBuildVersion();

						prodBuildtimestamp = build.getTimestamp();

					}
				}
			}

			if (prodBuildtimestamp != 0) {

				Calendar currentMonth = Calendar.getInstance();
				currentMonth.setTimeInMillis(prodBuildtimestamp);

				SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
				//String productionDeploymentDateFormat = simpleDateFormat.format(new Date());
				String productionDeploymentDateFormat = properties.getProperty(PropertiesConstants.JENKINS_PROD_DEPLOYMENT_CURRENTMONTH);
				Date prodDeploymentBuildVersion = null;
				try {
					prodDeploymentBuildVersion = simpleDateFormat.parse(productionDeploymentDateFormat);
				} catch (ParseException e) {
					e.printStackTrace();
				}

				Calendar productionDeploymentCal = Calendar.getInstance();
				productionDeploymentCal.setTime(prodDeploymentBuildVersion);

				if (currentMonth.get(Calendar.MONTH) == productionDeploymentCal.get(Calendar.MONTH)) {

					for (Builds build : totalBuildDetails) {
						if (build.getEnvironment().equalsIgnoreCase(devEnvironment)) {

							if (build.getBuildVersion().equalsIgnoreCase(prodBuildVersion)) {

								devBuildtimestamp = build.getTimestamp();
							}
						}

					}
					Calendar prodDeploymentDate = Calendar.getInstance();
					Calendar sitDeploymentDate = Calendar.getInstance();
					prodDeploymentDate.setTimeInMillis(prodBuildtimestamp);
					sitDeploymentDate.setTimeInMillis(devBuildtimestamp);
					long prodDeploymentDateMilsecs = prodDeploymentDate.getTimeInMillis();
					long sitDeploymentDateMilsecs = sitDeploymentDate.getTimeInMillis();
					long MSPERDAY = 60 * 60 * 24 * 1000;
					long days = (prodDeploymentDateMilsecs - sitDeploymentDateMilsecs) / MSPERDAY;
					
					 weeks = days/7;
					// long secs = (prodDeploymentDateMilsecs -
					// sitDeploymentDateMilsecs) /
					// 1000;
					// int seconds = (int) (( (prodDeploymentDateMilsecs -
					// sitDeploymentDateMilsecs) / 1000) % 60);
					//minutes = (prodDeploymentDateMilsecs - sitDeploymentDateMilsecs) / (60 * 1000);

					// Production last (previous) Build Date

					String previousProdBuildVersion = null;
					long previousProdBuildtimestamp = 0;
					long previousDevBuildtimestamp = 0;

					int previousStableBuildNumber = laststableBuildNumber - 1;

					String previousStableBuild = Integer.toString(previousStableBuildNumber);

					for (Builds build : totalBuildDetails) {

						if (build.getEnvironment().equalsIgnoreCase(prodEnvironment)) {

							if (build.getNumber().equalsIgnoreCase(previousStableBuild)) {

								previousProdBuildVersion = build.getBuildVersion();

								previousProdBuildtimestamp = build.getTimestamp();

							}
						}
					}

					for (Builds build : totalBuildDetails) {
						if (build.getEnvironment().equalsIgnoreCase(devEnvironment)) {

							if (build.getBuildVersion().equalsIgnoreCase(previousProdBuildVersion)) {

								previousDevBuildtimestamp = build.getTimestamp();
							}
						}

					}

					Calendar previousProdDeploymentDate = Calendar.getInstance();
					Calendar previousSitDeploymentDate = Calendar.getInstance();
					previousProdDeploymentDate.setTimeInMillis(previousProdBuildtimestamp);
					previousSitDeploymentDate.setTimeInMillis(previousDevBuildtimestamp);
					long previousProdDeploymentDateMilsecs = previousProdDeploymentDate.getTimeInMillis();
					long previousSitDeploymentDateMilsecs = previousSitDeploymentDate.getTimeInMillis();
					long MSPERDAY1 = 60 * 60 * 24 * 1000;
					long days1 = (previousProdDeploymentDateMilsecs - previousSitDeploymentDateMilsecs) / MSPERDAY1;
					
					long weeks1 = days1/7;
					// long secs1 = (previousProdDeploymentDateMilsecs -
					// previousSitDeploymentDateMilsecs) / 1000;
					//long minutes1 = (previousProdDeploymentDateMilsecs - previousSitDeploymentDateMilsecs)/ (60 * 1000);
							

					Long currentProdBuild = new Long(weeks);

					Long previousProdBuild = new Long(weeks1);

					int prodBuildDifference = currentProdBuild.compareTo(previousProdBuild);

					if (prodBuildDifference > 0) {

						if (currentProdBuild > 7) {
							developmentPipelineTrendWidget.setArrowValue(Constants.UP);
						} else {
							developmentPipelineTrendWidget.setArrowValue(Constants.UP);
						}

					}

					if (prodBuildDifference < 0) {

						if (currentProdBuild < 7) {
							developmentPipelineTrendWidget.setArrowValue(Constants.DOWN);
						} else {
							developmentPipelineTrendWidget.setArrowValue(Constants.DOWN);
						}

					}
					if (prodBuildDifference == 0) {

						developmentPipelineTrendWidget.setArrowValue(Constants.NO_CHANGES);
					}
				}
			}
			// developmentPipelineTrendWidget.setValue((int) minutes / 60);
			developmentPipelineTrendWidget.setName(Constants.AVG_LEAD_TIME_FOR_PROD_CHANGES);
			developmentPipelineTrendWidget.setValue((int) weeks);
			developmentPipelineTrendWidget.setUnitValue(Constants.WEEKS);
			DrillDown drillDown = getAverageLeadTimeforProductionChangesDrillDown(totalBuildDetails);
			developmentPipelineTrendWidget.setDrillDown(drillDown);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return developmentPipelineTrendWidget;

	}

	public DrillDown getAverageLeadTimeforProductionChangesDrillDown(List<Builds> totalBuildDetails) {

		Properties properties;

		DrillDown drillDown = new DrillDown();

		try {
			properties = propertyLoader.loadPropertiesFile(PropertiesConstants.JENKINS_PROPERTIES_FILENAME);

			String projectName = properties.getProperty(PropertiesConstants.PROJECT_NAME);
			// String env =
			// properties.getProperty(PropertiesConstants.JENKINS_PROD_RESOURCENAME);
			Map<String, Set<Date>> filters = new HashMap<String, Set<Date>>();
			Set<String> uniqueManifestVersionList = new HashSet<>();
			for (Builds build : totalBuildDetails) {

				String manifestVersion = build.getManifestVersion();
				String dateFormatJenkins = Constants.DATE_FORMAT_JENKINS;
				DateFormat format = new SimpleDateFormat(dateFormatJenkins);
				String createDateStr = build.getCreateDate();
				Date createDate = format.parse(createDateStr);

				if (build.getEnvironment().equalsIgnoreCase("DEV")) {
					uniqueManifestVersionList.add(manifestVersion);
					if (filters.get(manifestVersion + "_" + build.getEnvironment()) != null) {
						Set<Date> devCreateDateList = filters.get(manifestVersion + "_" + build.getEnvironment());
						devCreateDateList.add(createDate);
						filters.put(manifestVersion + "_" + build.getEnvironment(), devCreateDateList);
					} else {
						Set<Date> devCreateDateList = new HashSet<>();
						devCreateDateList.add(createDate);
						filters.put(manifestVersion + "_" + build.getEnvironment(), devCreateDateList);
					}
				}
				
				
				if (build.getEnvironment().equalsIgnoreCase("INT")) {
					uniqueManifestVersionList.add(manifestVersion);
					if (filters.get(manifestVersion + "_" + build.getEnvironment()) != null) {
						Set<Date> intCreateDateList = filters.get(manifestVersion + "_" + build.getEnvironment());
						intCreateDateList.add(createDate);
						filters.put(manifestVersion + "_" + build.getEnvironment(), intCreateDateList);
					} else {
						Set<Date> intCreateDateList = new HashSet<>();
						intCreateDateList.add(createDate);
						filters.put(manifestVersion + "_" + build.getEnvironment(), intCreateDateList);
					}
				}
				if (build.getEnvironment().equalsIgnoreCase("TEST")) {
					uniqueManifestVersionList.add(manifestVersion);
					if (filters.get(manifestVersion + "_" + build.getEnvironment()) != null) {
						Set<Date> testCreateDateSet = filters.get(manifestVersion + "_" + build.getEnvironment());
						testCreateDateSet.add(createDate);
						filters.put(manifestVersion + "_" + build.getEnvironment(), testCreateDateSet);
					} else {
						Set<Date> testCreateDateSet = new HashSet<>();
						testCreateDateSet.add(createDate);
						filters.put(manifestVersion + "_" + build.getEnvironment(), testCreateDateSet);
					}
				}
				if (build.getEnvironment().equalsIgnoreCase("PROD")) {
					uniqueManifestVersionList.add(manifestVersion);
					if (filters.get(manifestVersion + "_" + build.getEnvironment()) != null) {
						Set<Date> prodCreateDateSet = filters.get(manifestVersion + "_" + build.getEnvironment());
						prodCreateDateSet.add(createDate);
						filters.put(manifestVersion + "_" + build.getEnvironment(), prodCreateDateSet);
					} else {
						Set<Date> prodCreateDateSet = new HashSet<>();
						prodCreateDateSet.add(createDate);
						filters.put(manifestVersion + "_" + build.getEnvironment(), prodCreateDateSet);
					}
				}
			}
			Set<String> manifestVersionSet = filters.keySet();
			List<VersionData> versionDataList = new ArrayList<VersionData>();
			List<DrillDownLeadTimeForChanges> drillDownLeadTimeForChangeList = new ArrayList<DrillDownLeadTimeForChanges>();

			Map<String, Integer> tempMap = new HashMap<>();
			for (String manifestVersion : manifestVersionSet) {

				Set<Date> dateList = filters.get(manifestVersion);
				// tempEnvList.add(env);
				// tempMap.put(manifestVersion, tempEnvList);

				SortedSet<Date> dateSet = new TreeSet(dateList);
				Date startDate = dateSet.first();
				Date endDate = dateSet.last();

				long diff = endDate.getTime() - startDate.getTime();

				float dayCount = (float) diff / (24 * 60 * 60 * 1000);

				int week = (int) (dayCount / 7);
				tempMap.put(manifestVersion, week);
				float week1 = (int) (dayCount / 7);
			}
			// }

			Set<String> keySet = tempMap.keySet();
			List<String> keyList = new ArrayList<>(keySet);
			List<String> tempkeyList = new ArrayList<>();

			for (String manifestVern : uniqueManifestVersionList) {
				boolean first = true;
				int count = 0;

				for (String key : keySet) {
					// String key = keyList.get(i);
					// count ++;
					count++;

					String manifestVersion = key.substring(0, key.indexOf("_"));
					String env = key.substring(key.indexOf("_") + 1);
					Integer week = tempMap.get(key);

					if (manifestVern.equalsIgnoreCase(manifestVersion)) {
						VersionData versionData = new VersionData();
						versionData.setEnvironment(env);
						versionData.setWeek(week);

						versionDataList.add(versionData);

					}
					if (keySet.size() == count) {
						DrillDownLeadTimeForChanges drillDownLeadTimeForChanges = new DrillDownLeadTimeForChanges();

						drillDownLeadTimeForChanges.setManifestVersion(manifestVern);
						drillDownLeadTimeForChanges.setVersionData(versionDataList);
						drillDownLeadTimeForChangeList.add(drillDownLeadTimeForChanges);

						versionDataList = new ArrayList<>();

						drillDown.setDrillDownData(drillDownLeadTimeForChangeList);

					}
				}
			}

		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return drillDown;
	}

	public String getlastStableBuildNumberFromJenkins(String resourceName) {
		Properties properties;
		String jsonResponse;
		try {
			properties = propertyLoader.loadPropertiesFile(PropertiesConstants.JENKINS_PROPERTIES_FILENAME);
			String endPointUrl = properties.getProperty(PropertiesConstants.JENKINS_ENDPOINTURL);
			String endUrl = properties.getProperty(PropertiesConstants.JENKINS_JOB_SUCCESSFUL_BUILD_ENDURL);
			endUrl = endUrl.replace(Constants.RESOURCENAME, resourceName);
			String userName = properties.getProperty(PropertiesConstants.JENKINS_USERNAME);
			String password = properties.getProperty(PropertiesConstants.JENKINS_PASSWORD);
			jsonResponse = restService.getRestResponseWithCredentials(createJenkinsParametersForBuildData(),
					CommonUtility.getPropertiesMap(endPointUrl, endUrl, userName, password, resourceName),
					HttpMethod.GET);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		return jsonResponse;

	}

	public String saveFunctionalTestingStatus(String projectName, List<String> envProjectName) {

		FunctionalTestStatusResult testResult = new FunctionalTestStatusResult();
		FunctionalTestStatusReport functionalTestStatusReport;
		List<FunctionalTestStatusReport> functionalTestStatusReportList = new ArrayList<FunctionalTestStatusReport>();

		for (String testProjectName : envProjectName) {

			try {
				Properties properties = propertyLoader
						.loadPropertiesFile(PropertiesConstants.JENKINS_PROPERTIES_FILENAME);
				String endPointUrl = properties.getProperty(PropertiesConstants.JENKINS_ENDPOINTURL);
				String newEndPointUrl = endPointUrl + "/" + "job" + "/" + testProjectName + "/";
				String endUrl = properties.getProperty(PropertiesConstants.JENKINS_JOB_DEPLOMENT_TYPE_ENDURL);
				String userName = properties.getProperty(PropertiesConstants.JENKINS_USERNAME);
				String password = properties.getProperty(PropertiesConstants.JENKINS_PASSWORD);
				String jsonResponse = restService.getRestResponseWithCredentials(createJenkinsParametersForBuildData(),
						CommonUtility.getPropertiesMap(newEndPointUrl, endUrl, userName, password, testProjectName),
						HttpMethod.GET);
				ObjectMapper mapper = new ObjectMapper();
				JenkinsProjectDetails projectDetails = mapper.readValue(jsonResponse, JenkinsProjectDetails.class);
				String url = endPointUrl + "/" + "job" + "/" + testProjectName + "/";

				projectName = getProjectName(url, projectName);

				List<Builds> buildList = projectDetails.getBuilds();
				for (Builds build : buildList) {

					FunctionalTestingStatus functionalTestingStatus = getFunctionalTestingResult(build.getUrl());
					if (functionalTestingStatus.getChildReports() != null) {
					Builds upstreamBuildDetails = getUpstreamBuildNumber(build.getUrl());
					String upstreamProject = null;

					if (upstreamBuildDetails != null) {
						upstreamProject = upstreamBuildDetails.getActions().get(0).getCauses().get(0).getUpstreamProject();
					}
					String upstreamBuild = null;
					if (upstreamBuildDetails != null) {
						upstreamBuild =upstreamBuildDetails.getActions().get(0).getCauses().get(0).getUpstreamBuild();
					}
					if (upstreamProject != null && upstreamBuild != null) {
						String upstreamBuildVersion = getUpstreamBuildVersionNumber(projectName, upstreamProject,
								upstreamBuild);

						functionalTestStatusReport = new FunctionalTestStatusReport();

						functionalTestStatusReport.setBuildVersion(build.getDisplayName());
						functionalTestStatusReport.setUpstreamBuildNumber(
								upstreamBuildDetails.getActions().get(0).getCauses().get(0).getUpstreamBuild());
						functionalTestStatusReport.setUpstreamBuildVersion(upstreamBuildVersion);
						functionalTestStatusReport.setUpstreamProjectName(
								upstreamBuildDetails.getActions().get(0).getCauses().get(0).getUpstreamProject());

						functionalTestStatusReport.setEnvironment(testProjectName);

						ChildReports result = functionalTestingStatus.getChildReports().get(0);

						functionalTestStatusReport.setFailCount(result.getResult().getFailCount());
						functionalTestStatusReport.setPassCount(result.getResult().getPassCount());
						functionalTestStatusReport.setTimestamp(build.getTimestamp());
						if (functionalTestStatusReport.getFailCount() > 0) {
							functionalTestStatusReport.setColor(Constants.RED);
						}

						functionalTestStatusReportList.add(functionalTestStatusReport);

						testResult.setProjectName(projectName);
						testResult.setFunctionalTestStatusReports(functionalTestStatusReportList);

					}
				}
				}
			} catch (IOException e) {
				// TODO Auto generated catch block
				e.printStackTrace();
			}

		}

		storage.saveFunctionalTestingStatus(testResult);
		return projectName;

	}

	public FunctionalTestingStatus getFunctionalTestingResult(String endPointUrl) {
		String jsonResponse = null;
		Properties properties;
		ObjectMapper mapper = new ObjectMapper();
		FunctionalTestingStatus jenkinsFunctionalTestingStatusDetails = new FunctionalTestingStatus();
		try {
			properties = propertyLoader.loadPropertiesFile(PropertiesConstants.JENKINS_PROPERTIES_FILENAME);
			String endUrl = properties.getProperty(PropertiesConstants.JENKINS_JOB_FUNCTIONAL_TEST_PROJECTNAME_ENDUR);
			String userName = properties.getProperty(PropertiesConstants.JENKINS_USERNAME);
			String password = properties.getProperty(PropertiesConstants.JENKINS_PASSWORD);
			jsonResponse = restService.getRestResponseWithCredentials(createJenkinsParametersForBuildData(),
					CommonUtility.getPropertiesMap(endPointUrl, endUrl, userName, password, ""), HttpMethod.GET);
			jenkinsFunctionalTestingStatusDetails = mapper.readValue(jsonResponse, FunctionalTestingStatus.class);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return jenkinsFunctionalTestingStatusDetails;

	}

	public List<FunctionalTestStatusReport> getFunctinalTestStatusDetail() {

		List<String> upStreamEnvList = new ArrayList<String>();
		List<FunctionalTestStatusReport> functionalTestStatusReportList = new ArrayList<FunctionalTestStatusReport>();

		try {
			Properties properties = propertyLoader.loadPropertiesFile(PropertiesConstants.JENKINS_PROPERTIES_FILENAME);
			String prodEnv = properties.getProperty(PropertiesConstants.PROD_RESOURCENAME);
			String testEnv = properties.getProperty(PropertiesConstants.TEST_RESOURCENAME);
			String intEnv = properties.getProperty(PropertiesConstants.INT_RESOURCENAME);
			String devEnv = properties.getProperty(PropertiesConstants.DEV_RESOURCENAME);
			String projectName = properties.getProperty(PropertiesConstants.PROJECT_NAME);
			upStreamEnvList.add(devEnv);
			upStreamEnvList.add(intEnv);
			upStreamEnvList.add(testEnv);
			upStreamEnvList.add(prodEnv);

			for (String upsteamenv : upStreamEnvList) {

				String lastStableBuild = getlastStableBuildNumberFromJenkins(upsteamenv);
				Map<String, String> filters = new HashMap<String, String>();
				filters.put(Constants.PROJECTNAME, projectName);
				List<FunctionalTestStatusResult> testResult = storage.findWithFilters(filters,
						FunctionalTestStatusResult.class);

				FunctionalTestStatusResult automationTestResult = testResult.get(0);
				List<FunctionalTestStatusReport> functionalTestStatusReports = automationTestResult
						.getFunctionalTestStatusReports();

				for (FunctionalTestStatusReport functionalTestStatusReport : functionalTestStatusReports) {

					if (functionalTestStatusReport.getUpstreamProjectName().equalsIgnoreCase(upsteamenv)) {

						if (functionalTestStatusReport.getUpstreamBuildNumber().equalsIgnoreCase(lastStableBuild)) {

							functionalTestStatusReportList.add(functionalTestStatusReport);

						}

					}

				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return functionalTestStatusReportList;
	}

	public Builds getUpstreamBuildNumber(String endPointUrl) {
		Builds builds = null;
		String jsonResponse = null;
		Properties properties;
		ObjectMapper mapper = new ObjectMapper();
		// FunctionalTestingStatus jenkinsFunctionalTestingStatusDetails = new
		// FunctionalTestingStatus();
		try {
			properties = propertyLoader.loadPropertiesFile(PropertiesConstants.JENKINS_PROPERTIES_FILENAME);
			String endUrl = properties.getProperty(PropertiesConstants.JENKINS_UPSTEAMPROJECT_ENDUR);
			String userName = properties.getProperty(PropertiesConstants.JENKINS_USERNAME);
			String password = properties.getProperty(PropertiesConstants.JENKINS_PASSWORD);
			jsonResponse = restService.getRestResponseWithCredentials(createJenkinsParametersForBuildData(),
					CommonUtility.getPropertiesMap(endPointUrl, endUrl, userName, password, ""), HttpMethod.GET);
			builds = mapper.readValue(jsonResponse, Builds.class);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return builds;
	}

	public String getUpstreamBuildVersionNumber(String projectName, String evn, String upsteramBuildVersionNumber) {

		Map<String, String> filters = new HashMap<String, String>();
		filters.put(Constants.PROJECTNAME, "NessSmart");
		List<JenkinsProjectBuildDetails> jenkinsProjectDetails = storage.findWithFilters(filters,
				JenkinsProjectBuildDetails.class);
		List<Builds> totalBuildDetails = jenkinsProjectDetails.get(0).getBuilds();
		String buildVersion = null;

		for (Builds build : totalBuildDetails) {

			if (build.getEnvironment().equalsIgnoreCase(evn)) {

				if (build.getId().equalsIgnoreCase(upsteramBuildVersionNumber)) {

					buildVersion = build.getBuildVersion();

				}
			}
		}
		return buildVersion;
	}

	public DevelopmentPipelineTrendWidget getProductionDeploymentFrequency() throws IOException, ParseException {
		Map<String, String> filters = new HashMap<String, String>();
		Properties properties = propertyLoader.loadPropertiesFile(PropertiesConstants.JENKINS_PROPERTIES_FILENAME);
		String jenkinesProjectName = properties.getProperty(PropertiesConstants.PROJECT_NAME);
		String environment = properties.getProperty(PropertiesConstants.JENKINS_PROD_RESOURCENAME);
		filters.put(Constants.PROJECTNAME, jenkinesProjectName);
		List<Builds> currentMonthProdBuilds = new ArrayList<Builds>();
		List<Builds> lastMonthProdBuilds = new ArrayList<Builds>();
		List<String> currentProdBuildVersionList = new ArrayList<String>();
		List<String> lastMonthProdBuildVersionList = new ArrayList<String>();
		List<JenkinsProjectBuildDetails> jenkinsProjectDetails = storage.findWithFilters(filters,
				JenkinsProjectBuildDetails.class);
		List<Builds> totalBuildDetails = jenkinsProjectDetails.get(0).getBuilds();
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
		for (Builds build : totalBuildDetails) {

			if (build.getEnvironment().equals(environment)) {
				
				String createDateStr = build.getCreateDate();
			//	createDateStr = simpleDateFormat.format(createDateStr);
				Date createDate = simpleDateFormat.parse(createDateStr);
				
				Calendar currentDateCal = Calendar.getInstance();
				currentDateCal.setTime(createDate);
				
			
			
				//String productionDeploymentDateFormat = simpleDateFormat.format(new Date());
				String productionDeploymentDateFormat = properties.getProperty(PropertiesConstants.JENKINS_PROD_DEPLOYMENT_CURRENTMONTH);
				Date prodDeploymentBuildVersion = null;
				try {
					prodDeploymentBuildVersion = simpleDateFormat.parse(productionDeploymentDateFormat);
				} catch (ParseException e) {
					e.printStackTrace();
				}

				Calendar productionDeploymentCal = Calendar.getInstance();
				productionDeploymentCal.setTime(prodDeploymentBuildVersion);
				
				if (currentDateCal.get(Calendar.MONTH) == productionDeploymentCal.get(Calendar.MONTH)) {

					if (!currentProdBuildVersionList.contains(build.getBuildVersion())) {
						currentMonthProdBuilds.add(build);
						currentProdBuildVersionList.add(build.getBuildVersion());
					}

				}

			}
		}

		for (Builds build : totalBuildDetails) {

			if (build.getEnvironment().equals(environment)) {
				
				String createDateStr = build.getCreateDate();
				Date createDate = simpleDateFormat.parse(createDateStr);
				Calendar currentDateCal = Calendar.getInstance();
				currentDateCal.setTime(createDate);

				//String prodDeploymentDateFormatForLastMonth = simpleDateFormat.format(new Date());
				String prodDeploymentDateFormatForLastMonth = properties.getProperty(PropertiesConstants.JENKINS_PROD_DEPLOYMENT_CURRENTMONTH);

				Date lastMonthProdBuildVersion = null;
				try {
					lastMonthProdBuildVersion = simpleDateFormat.parse(prodDeploymentDateFormatForLastMonth);
				} catch (ParseException e) {
					e.printStackTrace();
				}
				Calendar prodDeploymentForLastMonth = Calendar.getInstance();
				prodDeploymentForLastMonth.setTime(lastMonthProdBuildVersion);
				if (currentDateCal.get(Calendar.MONTH) == prodDeploymentForLastMonth.get(Calendar.MONTH) - 1) {

					if (!lastMonthProdBuildVersionList.contains(build.getBuildVersion())) {
						lastMonthProdBuilds.add(build);
						lastMonthProdBuildVersionList.add(build.getBuildVersion());
					}

				}

			}
		}

		DevelopmentPipelineTrendWidget developmentPipelineTrendWidget = new DevelopmentPipelineTrendWidget();

		if (currentMonthProdBuilds.size() > lastMonthProdBuilds.size()) {
			developmentPipelineTrendWidget.setArrowValue(Constants.UP);
		}

		if (currentMonthProdBuilds.size() < lastMonthProdBuilds.size()) {
			developmentPipelineTrendWidget.setArrowValue(Constants.DOWN);
		}

		if (currentMonthProdBuilds.size() == lastMonthProdBuilds.size()) {
			developmentPipelineTrendWidget.setArrowValue(Constants.NO_CHANGES);
		}
		developmentPipelineTrendWidget.setName("Production Deployment Frequency");
		developmentPipelineTrendWidget.setValue(currentMonthProdBuilds.size());
		developmentPipelineTrendWidget.setUnitValue(Constants.MONTHLY);
		DrillDown drillDown = getProductionDeploymentFrequencyDrillDown();
		developmentPipelineTrendWidget.setDrillDown(drillDown);
		return developmentPipelineTrendWidget;
	}

	public DrillDown getProductionDeploymentFrequencyDrillDown() throws IOException {
		Properties properties = propertyLoader.loadPropertiesFile(PropertiesConstants.JENKINS_PROPERTIES_FILENAME);
		String projectName = properties.getProperty(PropertiesConstants.PROJECT_NAME);
		String env = properties.getProperty(PropertiesConstants.JENKINS_PROD_RESOURCENAME);
		Map<String, String> filters = new HashMap<String, String>();
		filters.put(Constants.PROJECTNAME, projectName);
		List<JenkinsProjectBuildDetails> jenkinsProjectDetails = storage.findWithFilters(filters,
				JenkinsProjectBuildDetails.class);
		List<Builds> totalBuildDetails = jenkinsProjectDetails.get(0).getBuilds();
		Map<Integer, List<Integer>> releaseMonthMap = new HashMap<Integer, List<Integer>>();

		for (Builds build : totalBuildDetails) {
			if (build.getEnvironment().equals(env)) {

				DateFormat formatter = new SimpleDateFormat("yyyy-M-dd");
				Date date = null;
				try {
					date = (Date) formatter.parse(build.getCreateDate());
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(date);

				/*
				 * Calendar calendar = Calendar.getInstance();
				 * calendar.setTimeInMillis(build.getTimestamp());
				 */
				int month = calendar.get(Calendar.MONTH);

				if (releaseMonthMap.get(month) != null) {
					List<Integer> buildCount = releaseMonthMap.get(month);
					buildCount.add(buildCount.size() + 1);
					releaseMonthMap.put(month, buildCount);
				} else {
					List<Integer> buildCount = new ArrayList<Integer>();
					buildCount.add(1);
					releaseMonthMap.put(month, buildCount);
				}
			}

		}

		Set<Integer> months = releaseMonthMap.keySet();
		DrillDown developmentPipelineTrendWidgetDrillDown = new DrillDown();
		List<DrillDownData> drillDownDataList = new ArrayList<DrillDownData>();

		for (int month : months) {
			List<Integer> buildCount = releaseMonthMap.get(month);

			developmentPipelineTrendWidgetDrillDown.setxLabel("Year 2017");
			developmentPipelineTrendWidgetDrillDown.setyLabel("No Of Deployments");
			DrillDownData drilDownData = new DrillDownData();
			drilDownData.setName(properties.getProperty(Integer.toString(month)));
			List<Sections> sectionList = new ArrayList<>();
			Sections sections = new Sections();
			sections.setTitle("Deployment");
			sections.setValue((double) buildCount.size());
			sectionList.add(sections);
			drilDownData.setSections(sectionList);
			drillDownDataList.add(drilDownData);

		}
		developmentPipelineTrendWidgetDrillDown.setDrillDownData(drillDownDataList);
		return developmentPipelineTrendWidgetDrillDown;
	}
	
	public int getLastBuildNumber(int currentBuildNo, int lastBuildNo) {
		if (currentBuildNo > lastBuildNo) {
			lastBuildNo = currentBuildNo;
		}
		System.out.println("Smallest Number is : " + lastBuildNo);
		System.out.println("Smallest Number is : " + currentBuildNo);
		return lastBuildNo;
	}
}
