package com.nessSmart.service.fetchdata;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nessSmart.Constants.Constants;
import com.nessSmart.Constants.PropertiesConstants;
import com.nessSmart.dao.MongoStorage;
import com.nessSmart.mapping.common.model.Builds;
import com.nessSmart.mapping.common.model.Issues;
import com.nessSmart.mapping.common.model.JenkinsProjectBuildDetails;
import com.nessSmart.mapping.common.model.Measures;
import com.nessSmart.mapping.database.model.JenkinsQualityWidgetBuildDetails;
import com.nessSmart.mapping.database.model.SonarDataForQualityWidget;
import com.nessSmart.mapping.response.model.DrillDown;
import com.nessSmart.mapping.response.model.DrillDownData;
import com.nessSmart.mapping.response.model.Sections;
import com.nessSmart.mapping.response.model.WidgetDataAndDrillDown;
import com.nessSmart.mapping.response.model.WidgetResponse;
import com.nessSmart.utils.CommonUtility;
import com.nessSmart.utils.LoadProperties;

@Service
public class JenkinsFetchDataService {

	@Autowired
	LoadProperties propertyLoader;

	@Autowired
	MongoStorage storage;

	@Autowired
	SonarDataProcessService sonarDataProcessService;

	public WidgetDataAndDrillDown getJenkinsBuildTestDetailsForQualityWidget() throws IOException {

		Properties properties = propertyLoader.loadPropertiesFile(PropertiesConstants.JENKINS_PROPERTIES_FILENAME);
		String environment = properties.getProperty(PropertiesConstants.JENKINS_RESOURCENAME);
		String sonarProjectName = properties.getProperty(PropertiesConstants.SONARQUBE_RESOURCENAME);

		String startDate1 = properties.getProperty(PropertiesConstants.JENKINS_BUILD_STARTDATE_1);
		String endDate1 = properties.getProperty(PropertiesConstants.JENKINS_BUILD_ENDDATE_1);
		String startDate2 = properties.getProperty(PropertiesConstants.JENKINS_BUILD_STARTDATE_2);
		String endDate2 = properties.getProperty(PropertiesConstants.JENKINS_BUILD_ENDDATE_2);

		List<JenkinsQualityWidgetBuildDetails> jenkinsProjectBuildDetailscurrent = storage.findWithFilters(
				getJenkinsFilterMap(startDate1, endDate1, environment), JenkinsQualityWidgetBuildDetails.class);

		List<JenkinsQualityWidgetBuildDetails> jenkinsProjectBuildDetailsPre= storage.findWithFilters(
				getJenkinsFilterMap(startDate2, endDate2, environment), JenkinsQualityWidgetBuildDetails.class);

		float currentSuccessPercentage = getBuildSuccessPercentage(jenkinsProjectBuildDetailscurrent);
		float preSuccessPercentage = getBuildSuccessPercentage(jenkinsProjectBuildDetailsPre);
		String successPercentageArrowValue = Constants.UP;
		if (preSuccessPercentage > currentSuccessPercentage) {
			successPercentageArrowValue = Constants.DOWN;
		}
		WidgetDataAndDrillDown widgetDataAndDrillDown = new WidgetDataAndDrillDown();

		List<WidgetResponse> qualityWidgetResponseList = new ArrayList<WidgetResponse>();
		WidgetResponse buildData = new WidgetResponse();
		buildData.setName(Constants.BUILD_SUCCESS_RATIO);
		buildData.setValue(String.valueOf(currentSuccessPercentage));
		buildData.setArrowValue(successPercentageArrowValue);
		buildData.setUnitValue(Constants.PERCENTAGE);

		List<SonarDataForQualityWidget> sonarData = storage
				.findWithFilters(getSonarFilterMap(environment, sonarProjectName), SonarDataForQualityWidget.class);

		List<String> measureNameList = new ArrayList<String>();
		measureNameList.add(Constants.NCLOC);
		measureNameList.add(Constants.COVERAGE);

		qualityWidgetResponseList.addAll(sonarDataProcessService.processSonarValues(sonarData.get(0).getMeasureList(),
				measureNameList, CommonUtility.getDate(Constants.DATE_FORMAT_NO_TIME, startDate1),
				CommonUtility.getDate(Constants.DATE_FORMAT_NO_TIME, endDate1),
				CommonUtility.getDate(Constants.DATE_FORMAT_NO_TIME, startDate2),
				CommonUtility.getDate(Constants.DATE_FORMAT_NO_TIME, endDate2)));
		DrillDown drillDown = getSuccessRatioAndCoverageDrillDown(qualityWidgetResponseList, measureNameList,
				sonarData.get(0).getMeasureList());
		widgetDataAndDrillDown.setDrillDown(drillDown);
		qualityWidgetResponseList.add(buildData);
		widgetDataAndDrillDown.setWidgetData(qualityWidgetResponseList);

		return widgetDataAndDrillDown;

	}

	private DrillDown getSuccessRatioAndCoverageDrillDown(List<WidgetResponse> qualityWidgetResponseList,
			List<String> measureNameList, List<Measures> measureList) {
		Map<String, Double> successRatioMap = getBuildSuccessRatioDrillDown();
		Map<String, Map<String, Double>> CoverageNclocMap = sonarDataProcessService
				.complexityCoverageNclocDrillDown(measureList, measureNameList);
		WidgetDataAndDrillDown widgetDataAndDrillDown = new WidgetDataAndDrillDown();
		widgetDataAndDrillDown.setWidgetData(qualityWidgetResponseList);

		Map<String, Double> coverageMap = CoverageNclocMap.get("coverage");
		Map<String, Double> nclocMap = CoverageNclocMap.get("ncloc");

		String[] monthList = Constants.MONTH_LIST;
		DrillDown drillDown = new DrillDown();

		List<DrillDownData> drillDownDataList = new ArrayList<>();
		for (String month : monthList) {
			Double successRatio = successRatioMap.get(month);
			Double coverage = coverageMap.get(month);
			Double ncloc = nclocMap.get(month);

			drillDown.setxLabel("Year 2017");
			drillDown.setyLabel("Number");
			List<Sections> sectionList = new ArrayList<>();
			Sections successSections = new Sections();
			successSections.setTitle(Constants.BUILD_SUCCESS_RATIO);
			if (successRatio != null) {
				successSections.setValue(successRatio);
			} else {
				successSections.setValue(0D);
			}
			sectionList.add(successSections);

			Sections coverageSections = new Sections();
			coverageSections.setTitle(Constants.UNIT_TEST_COVERAGE);
			if (coverage != null) {
				coverageSections.setValue(coverage);
			} else {
				coverageSections.setValue(0D);
			}
			sectionList.add(coverageSections);

			Sections nclocSection = new Sections();
			nclocSection.setTitle(Constants.NUMBER_OF_LINES_COVERED);
			if (ncloc != null) {
				nclocSection.setValue(ncloc);
			} else {
				nclocSection.setValue(0D);
			}
			sectionList.add(nclocSection);
			DrillDownData drillDownData = new DrillDownData();
			drillDownData.setName(month);
			drillDownData.setSections(sectionList);
			drillDownDataList.add(drillDownData);
			drillDown.setDrillDownData(drillDownDataList);
		}
		return drillDown;
	}

	private Map<String, Double> getBuildSuccessRatioDrillDown() {

		Properties properties;
		Map<Integer, Integer> releaseMonthMap = new HashMap<Integer, Integer>();
		int buildCount = 0;
		Map<String, Double> finalReleaseMap = new HashMap<>();

		try {
			properties = propertyLoader.loadPropertiesFile(PropertiesConstants.JENKINS_PROPERTIES_FILENAME);

			String projectName = properties.getProperty(PropertiesConstants.PROJECT_NAME);
			// String env =
			// properties.getProperty(PropertiesConstants.JENKINS_PROD_RESOURCENAME);
			Map<String, String> filters = new HashMap<String, String>();
			filters.put(Constants.PROJECTNAME, projectName);
			List<JenkinsProjectBuildDetails> jenkinsProjectDetails = storage.findWithFilters(filters,
					JenkinsProjectBuildDetails.class);
			List<Builds> totalBuildDetails = jenkinsProjectDetails.get(0).getBuilds();
			for (Builds build : totalBuildDetails) {
				if (build.getEnvironment().equals("DEV")) {
					if (build.getResult().equalsIgnoreCase("SUCCESS")) {
						Calendar calendar = Calendar.getInstance();
						calendar.setTimeInMillis(build.getTimestamp());
						int month = calendar.get(Calendar.MONTH);

						if (releaseMonthMap.get(month) != null) {
							buildCount = releaseMonthMap.get(month);
							buildCount = buildCount + 1;
							releaseMonthMap.put(month, buildCount);
						} else {
							buildCount = buildCount + 1;
							releaseMonthMap.put(month, buildCount);
						}

					}
				}
			}
			Set<Integer> months = releaseMonthMap.keySet();
			for (int month : months) {
				float sucessRatio = (float) releaseMonthMap.get(month);

				finalReleaseMap.put(properties.getProperty(Integer.toString(month)), (double) sucessRatio);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return finalReleaseMap;

	}

	public WidgetDataAndDrillDown getJenkinsSonarDetailsForQualityWidget() throws IOException {

		Properties properties = propertyLoader.loadPropertiesFile(PropertiesConstants.JENKINS_PROPERTIES_FILENAME);

		String environment = properties.getProperty(PropertiesConstants.JENKINS_RESOURCENAME);
		String sonarProjectName = properties.getProperty(PropertiesConstants.SONARQUBE_RESOURCENAME);

		List<SonarDataForQualityWidget> sonarData = storage
				.findWithFilters(getSonarFilterMap(environment, sonarProjectName), SonarDataForQualityWidget.class);

		List<Issues> issueList = sonarData.get(0).getIssueList();
		List<Measures> measureList = sonarData.get(0).getMeasureList();

		Date startdate1 = CommonUtility.getDate(Constants.DATE_FORMAT_NO_TIME,
				properties.getProperty(PropertiesConstants.JENKINS_BUILD_STARTDATE_1));
		Date enddate1 = CommonUtility.getDate(Constants.DATE_FORMAT_NO_TIME,
				properties.getProperty(PropertiesConstants.JENKINS_BUILD_ENDDATE_1));
		Date startdate2 = CommonUtility.getDate(Constants.DATE_FORMAT_NO_TIME,
				properties.getProperty(PropertiesConstants.JENKINS_BUILD_STARTDATE_2));
		Date enddate2 = CommonUtility.getDate(Constants.DATE_FORMAT_NO_TIME,
				properties.getProperty(PropertiesConstants.JENKINS_BUILD_ENDDATE_2));

		List<String> measureNameList = new ArrayList<String>();
		measureNameList.add(Constants.COMPLEXITY);

		List<WidgetResponse> qualityWidgetResponseList = new ArrayList<WidgetResponse>();
		qualityWidgetResponseList.addAll(sonarDataProcessService.processSonarValues(measureList, measureNameList,
				startdate1, enddate1, startdate2, enddate2));
		qualityWidgetResponseList.addAll(sonarDataProcessService.processSonarCriticalAndBlockerIssues(issueList,
				startdate1, enddate1, startdate2, enddate2));

		DrillDown drillDown = sonarDataProcessService.complexityCriticalIssuesBlockerIssuesDrillDown(issueList,
				measureList, measureNameList);

		WidgetDataAndDrillDown widgetDataAndDrillDown = new WidgetDataAndDrillDown();
		widgetDataAndDrillDown.setWidgetData(qualityWidgetResponseList);
		widgetDataAndDrillDown.setDrillDown(drillDown);
		return widgetDataAndDrillDown;

	}

	private float getBuildSuccessPercentage(List<JenkinsQualityWidgetBuildDetails> jenkinsProjectBuildDetails) {
		float successCount = 0;
		float totalCount = jenkinsProjectBuildDetails.size();
		for (JenkinsQualityWidgetBuildDetails build : jenkinsProjectBuildDetails) {
			if (build.getResult().equalsIgnoreCase(Constants.SUCCESS))
				successCount++;
		}
		float successPercentage = 0;
		if (totalCount != 0) {
			successPercentage = (successCount * 100) / totalCount;
		}
		return successPercentage;
	}

	private Map<String, String> getJenkinsFilterMap(String date1, String date2, String resourceName) {
		Map<String, String> filters = new HashMap<String, String>();
		filters.put(Constants.RESOURCENAME, resourceName);
		filters.put(Constants.CREATED_DATE, CommonUtility.getFormattedDateString(date1 + Constants.START_TIME_APPEND));
		filters.put(Constants.CREATE_DATE, CommonUtility.getFormattedDateString(date2 + Constants.END_TIME_APPEND));
		return filters;
	}

	private Map<String, String> getSonarFilterMap(String environment, String sonarProjectName) {
		Map<String, String> filters = new HashMap<String, String>();
		filters.put(Constants.ENVIRONMENT, environment);
		filters.put(Constants.PROJECTNAME, sonarProjectName);
		return filters;
	}

}
