package com.nessSmart.service.fetchdata;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import com.nessSmart.Constants.Constants;
import com.nessSmart.Constants.PropertiesConstants;
import com.nessSmart.dao.MongoStorage;
import com.nessSmart.mapping.common.model.JiraProjectVersionDetails;
import com.nessSmart.mapping.response.model.WidgetResponse;
import com.nessSmart.utils.ComparatorUtils;
import com.nessSmart.utils.LoadProperties;
import com.nessSmart.utils.RestTemplateUtil;

@Service
public class ReleaseStatusWidgetService {

	@Autowired
	LoadProperties propertyLoader;

	@Autowired
	RestTemplateUtil restService;

	@Autowired
	MongoStorage storage;

	public Map<String, List<WidgetResponse>> getManifestdData() {
		Map<String, List<WidgetResponse>> releaseManifestDetails = new HashMap<String, List<WidgetResponse>>();
		Properties jiraProperties;
		try {
			jiraProperties = propertyLoader.loadPropertiesFile(PropertiesConstants.JIRA_PROPERTIES_FILENAME);
			String resourceName = jiraProperties.getProperty(PropertiesConstants.JIRA_RESOURCENAME);
			Map<String, String> manifestPropertiesMap = new HashMap<>();
			manifestPropertiesMap.put(Constants.ENDURL,
					jiraProperties.getProperty(PropertiesConstants.MANIFEST_JOB_ENDURL));

			Map<String, String> manifestDetailsMap = new HashMap<String, String>();

			for (int i = 0; i < 4; i++) {
				manifestPropertiesMap.put(Constants.ENDPOINT_URL,
						jiraProperties.getProperty(PropertiesConstants.MANIFEST_ENDPOINTURL + i));
				String manifestEnvName = jiraProperties.getProperty(PropertiesConstants.MANIFEST_ENV + i);
				String jsonResponse = restService.getRestResponseWithoutCredentials(
						createManifestParametersForBuildData(), manifestPropertiesMap, HttpMethod.GET);
				JSONObject json = new JSONObject(jsonResponse);
				String manifestVersion = (String) json.getJSONObject(Constants.MAIN_ATTRIBUTES)
						.get(Constants.MANIFEST_VERSION);
				manifestDetailsMap.put(manifestEnvName, manifestVersion);
			}

			Map<String, String> filters = new HashMap<String, String>();
			filters.put(Constants.RESOURCENAME, resourceName);

			List<JiraProjectVersionDetails> versionDetailsList = storage.findWithFilters(filters,
					JiraProjectVersionDetails.class);

			Collections.sort(versionDetailsList, ComparatorUtils.getDateComaparatorForJiraProjectVersionDetails());

			if (versionDetailsList.size() > 0) {
				for (Entry<String, String> manifestMap : manifestDetailsMap.entrySet()) {

					List<WidgetResponse> widgetResponseList = new ArrayList<WidgetResponse>();
					WidgetResponse widgetResponse = new WidgetResponse();
					widgetResponse.setName(manifestMap.getKey());
					widgetResponse.setValue(manifestMap.getValue());
					widgetResponse.setArrowValue(manifestMap.getValue().substring(2, 3));
					String indexString = manifestMap.getValue().substring(0, 1);
					int index = Integer.parseInt(indexString);
					widgetResponseList.add(widgetResponse);
					String releaseName = versionDetailsList.get(index - 1).getName();

					if (releaseManifestDetails.containsKey(releaseName)) {
						List<WidgetResponse> tempList = releaseManifestDetails.get(releaseName);
						widgetResponseList.addAll(tempList);
					}

					releaseManifestDetails.put(releaseName, widgetResponseList);
				}
				for (Entry<String, List<WidgetResponse>> releaseManifest : releaseManifestDetails.entrySet()) {
					List<WidgetResponse> widgetResponseList = releaseManifest.getValue();
					Collections.sort(widgetResponseList, ComparatorUtils.getComaparatorForManifestEnv());
					releaseManifest.setValue(widgetResponseList);
				}
			}

		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return releaseManifestDetails;

	}

	private Map<String, String> createManifestParametersForBuildData() throws IOException {
		Map<String, String> jenkinsParametersMap = new HashMap<String, String>();
		return jenkinsParametersMap;
	}
}
