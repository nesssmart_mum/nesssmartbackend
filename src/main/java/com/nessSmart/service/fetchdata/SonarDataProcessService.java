package com.nessSmart.service.fetchdata;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nessSmart.Constants.Constants;
import com.nessSmart.Constants.PropertiesConstants;
import com.nessSmart.dao.MongoStorage;
import com.nessSmart.mapping.common.model.History;
import com.nessSmart.mapping.common.model.Issues;
import com.nessSmart.mapping.common.model.Measures;
import com.nessSmart.mapping.response.model.DrillDown;
import com.nessSmart.mapping.response.model.DrillDownData;
import com.nessSmart.mapping.response.model.Sections;
import com.nessSmart.mapping.response.model.WidgetResponse;
import com.nessSmart.utils.CommonUtility;
import com.nessSmart.utils.ComparatorUtils;
import com.nessSmart.utils.LoadProperties;

@Service
public class SonarDataProcessService {

	@Autowired
	LoadProperties propertyLoader;

	@Autowired
	MongoStorage storage;

	public List<WidgetResponse> processSonarCriticalAndBlockerIssues(List<Issues> issueList, Date startdate1,
			Date enddate1, Date startdate2, Date enddate2) {

		List<WidgetResponse> widgetResponseList = new ArrayList<WidgetResponse>();
		int oldCriticalIssues = 0;
		int oldBlockerIssues = 0;
		int newCriticalIssues = 0;
		int newBlockerIssues = 0;

		for (Issues issue : issueList) {
			String stringDate = issue.getCreationDate().substring(0, 10);
			Date date = CommonUtility.getDate(Constants.DATE_FORMAT_NO_TIME, stringDate);
			if (CommonUtility.isDateBetween(date, startdate1, enddate1)) {
				if (issue.getSeverity().equalsIgnoreCase(Constants.CRITICAL)) {
					newCriticalIssues++;
				}
				if (issue.getSeverity().equalsIgnoreCase(Constants.BLOCKER)) {
					newBlockerIssues++;
				}
			}
			if (CommonUtility.isDateBetween(date, startdate2, enddate2)) {
				if (issue.getSeverity().equalsIgnoreCase(Constants.CRITICAL)) {
					oldCriticalIssues++;
				}
				if (issue.getSeverity().equalsIgnoreCase(Constants.BLOCKER)) {
					oldBlockerIssues++;
				}
			}
		}

		WidgetResponse issuesCritical = new WidgetResponse();
		issuesCritical.setName(Constants.CRITICAL_ISSUES);
		issuesCritical.setValue(Integer.toString(newCriticalIssues));

		if (oldCriticalIssues > newCriticalIssues)
			issuesCritical.setArrowValue(Constants.DOWN);
		else
			issuesCritical.setArrowValue(Constants.UP);

		WidgetResponse issuesBlocker = new WidgetResponse();
		issuesBlocker.setName(Constants.BLOCKER_ISSUES);
		issuesBlocker.setValue(Integer.toString(newBlockerIssues));

		if (oldBlockerIssues > newBlockerIssues)
			issuesBlocker.setArrowValue(Constants.DOWN);
		else
			issuesBlocker.setArrowValue(Constants.UP);

		widgetResponseList.add(issuesCritical);
		widgetResponseList.add(issuesBlocker);
		return widgetResponseList;
	}

	public List<WidgetResponse> processSonarValues(List<Measures> measureList, List<String> measureNameList,
			Date startdate1, Date enddate1, Date startdate2, Date enddate2) {
		List<WidgetResponse> widgetResponseList = new ArrayList<WidgetResponse>();
		for (Measures measures : measureList) {
			if (measureNameList.contains(measures.getMetric())) {
				float currentValue = 0;
				float preValue = 0;
				String arrowValue = Constants.UP;
				List<History> currentHistoryList = new ArrayList<History>();
				List<History> preHistoryList = new ArrayList<History>();
				for (History history : measures.getHistory()) {
					String stringDate = history.getDate().substring(0, 10);
					Date date = CommonUtility.getDate(Constants.DATE_FORMAT_NO_TIME, stringDate);
					history.setDate(stringDate);
					if (CommonUtility.isDateBetween(date, startdate1, enddate1)) {
						currentHistoryList.add(history);
					}
					if (CommonUtility.isDateBetween(date, startdate2, enddate2)) {
						preHistoryList.add(history);
					}
				}
				currentValue = getLatestValueFromHistoryList(currentHistoryList);
				preValue = getLatestValueFromHistoryList(preHistoryList);
				if (currentValue > preValue)
					arrowValue = Constants.UP;
				WidgetResponse widgetResponse = new WidgetResponse();
				widgetResponse.setValue(String.valueOf(currentValue));
				widgetResponse.setArrowValue(arrowValue);
				widgetResponse.setName(getTagName(measures.getMetric()));
				widgetResponse.setUnitValue(getUnitValue(measures.getMetric()));
				widgetResponseList.add(widgetResponse);
			}

		}
		return widgetResponseList;
	}

	private Map<String, Double> getUnitTestCoverageDrillDown(Measures measureListForDrillDown) {

		Properties properties;
		Map<Integer, List<Float>> releaseMonthMap = new HashMap<Integer, List<Float>>();
		float buildCount = 0;
		Map<String, Double> drillDownResponce = new HashMap<String, Double>();
		try {
			properties = propertyLoader.loadPropertiesFile(PropertiesConstants.JENKINS_PROPERTIES_FILENAME);
			for (History history : measureListForDrillDown.getHistory()) {
				DateFormat formatter = new SimpleDateFormat("yyyy-M-dd");
				Date date = (Date) formatter.parse(history.getDate());
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(date);
				int month = calendar.get(Calendar.MONTH);

				if (releaseMonthMap.get(month) != null) {
					List<Float> buildCountList = releaseMonthMap.get(month);
					buildCount = Float.parseFloat(history.getValue());
					buildCountList.add(buildCount);
					releaseMonthMap.put(month, buildCountList);
				} else {

					buildCount = Float.parseFloat(history.getValue());
					List<Float> buildCountList = new ArrayList<Float>();
					buildCountList.add(buildCount);
					releaseMonthMap.put(month, buildCountList);
				}

			}
			
			Set<Integer> months = releaseMonthMap.keySet();
			for (int month : months) {
				float historyValue = 0F;
				List<Float> sucessRatio = releaseMonthMap.get(month);
				for (Float value : sucessRatio) {
					historyValue = value + historyValue;
				}
				drillDownResponce.put(properties.getProperty(Integer.toString(month)),
						(double) (historyValue / sucessRatio.size()));

			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return drillDownResponce;

	}

	private float getLatestValueFromHistoryList(List<History> historyList) {
		float value = 0;
		if (historyList.size() > 0) {
			int size = 0;
			if (historyList.size() > 2) {
				size = historyList.size() - 1;
				Collections.sort(historyList, ComparatorUtils.getDateComaparatorForSonarHistory());
			}
			value = Float.parseFloat(historyList.get(size).getValue());
		}
		return value;
	}

	private String getTagName(String value) {
		switch (value) {
		case Constants.COMPLEXITY:
			return Constants.CODE_COMPLEXITY;
		case Constants.NCLOC:
			return Constants.NUMBER_OF_LINES_COVERED;
		case Constants.COVERAGE:
			return Constants.UNIT_TEST_COVERAGE;
		default:
			return value;
		}
	}

	private String getUnitValue(String metricName) {
		switch (metricName) {
		case Constants.COVERAGE:
			return Constants.PERCENTAGE;
		default:
			return null;
		}
	}

	private Map<String, String> getSonarFilterMap(String environment, String sonarProjectName) {
		Map<String, String> filters = new HashMap<String, String>();
		filters.put(Constants.ENVIRONMENT, environment);
		filters.put(Constants.PROJECTNAME, sonarProjectName);
		return filters;
	}
	
	public DrillDown complexityCriticalIssuesBlockerIssuesDrillDown(List<Issues> issueList, List<Measures> measureList,
			List<String> measureNameList) {

		Map<String, Map<String, Double>> drillDownMap = complexityCoverageNclocDrillDown(measureList, measureNameList);
		Map<String, Map<String, Double>> blockerAndCriticalIssues = getDrillDownForBlockerAndCritical(issueList);
		Map<String, Double> complexityIssuesMap = drillDownMap.get(Constants.COMPLEXITY_ISSUES);
		Map<String, Double> criticalIssuesMap = blockerAndCriticalIssues.get(Constants.CRITICAL);
		Map<String, Double> blockerIssuesMap = blockerAndCriticalIssues.get(Constants.BLOCKER);
		DrillDown drilldown = populateDrilDown(complexityIssuesMap, criticalIssuesMap, blockerIssuesMap);
		return drilldown;
	}

	public Map<String, Map<String, Double>> complexityCoverageNclocDrillDown(List<Measures> measureList,
			List<String> measureNameList) {
		Map<String, Double> sonarDrillDown = null;
		Map<String, Map<String, Double>> drillDownMap = new HashMap<String, Map<String, Double>>();
		for (Measures measures : measureList) {
			if (measureNameList.contains(measures.getMetric())) {
				sonarDrillDown = getUnitTestCoverageDrillDown(measures);
				if (measures.getMetric().equalsIgnoreCase(Constants.COMPLEXITY)) {
					drillDownMap.put(Constants.COMPLEXITY_ISSUES, sonarDrillDown);
				} else {
					drillDownMap.put(measures.getMetric(), sonarDrillDown);
				}
			}
		}
		return drillDownMap;
	}

	private Map<String, Map<String, Double>> getDrillDownForBlockerAndCritical(List<Issues> issuesList) {
		Properties properties = null;
		try {
			properties = propertyLoader.loadPropertiesFile(PropertiesConstants.JENKINS_PROPERTIES_FILENAME);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Map<Integer, Float> criticalIssuesMap = new HashMap<Integer, Float>();
		Map<Integer, Float> blockerIssues = new HashMap<Integer, Float>();
		Map<String, Map<String, Double>> blockerCriticalIssuesMap = new HashMap<String, Map<String, Double>>();

		for (Issues issue : issuesList) {

			if (issue.getSeverity().equalsIgnoreCase(Constants.CRITICAL)) {
				criticalIssuesMap = getCriticalIssuesDrillDown(issue, criticalIssuesMap);
			}

			if (issue.getSeverity().equalsIgnoreCase(Constants.BLOCKER)) {
				blockerIssues = getCriticalIssuesDrillDown(issue, blockerIssues);
			}
		}

		Map<String, Double> criticalIssues = new HashMap<String, Double>();
		Set<Integer> criticalIssuesSet = criticalIssuesMap.keySet();
		for (int critical : criticalIssuesSet) {
			Float value = criticalIssuesMap.get(critical);
			criticalIssues.put(properties.getProperty(Integer.toString(critical)),(double) value);

		}

		Map<String, Double> blockerIssuesMap = new HashMap<String, Double>();

		Set<Integer> blockerIssuesSet = blockerIssues.keySet();
		for (int blocker : blockerIssuesSet) {
			float value = blockerIssues.get(blocker);
			blockerIssuesMap.put(properties.getProperty(Integer.toString(blocker)), (double) value);

		}

		blockerCriticalIssuesMap.put(Constants.CRITICAL, criticalIssues);
		blockerCriticalIssuesMap.put(Constants.BLOCKER, blockerIssuesMap);

		return blockerCriticalIssuesMap;
	}

	private Map<Integer, Float> getCriticalIssuesDrillDown(Issues issue, Map<Integer, Float> releaseMonthMap) {

		float buildCount = 0;
		try {

			DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			Date date = (Date) formatter.parse(issue.getCreationDate());
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(date);
			int month = calendar.get(Calendar.MONTH);

			if (releaseMonthMap.get(month) != null) {
				buildCount = releaseMonthMap.get(month);
				buildCount++;
				releaseMonthMap.put(month, buildCount);
			} else {
				buildCount++;
				releaseMonthMap.put(month, buildCount);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return releaseMonthMap;
	}

	private DrillDown populateDrilDown(Map<String, Double> complexityMap, Map<String, Double> criticalIssuesMap,
			Map<String, Double> blockerIssuesMap) {

		String[] monthList = Constants.MONTH_LIST;
		DrillDown drillDown = new DrillDown();

		List<DrillDownData> drillDownDataList = new ArrayList<>();
		for (String month : monthList) {

			Double complexity = complexityMap.get(month);
			Double criticalIssues = criticalIssuesMap.get(month);
			Double blockerIssues = blockerIssuesMap.get(month);

			drillDown.setxLabel("Year 2017");
			drillDown.setyLabel("Number");
			List<Sections> sectionList = new ArrayList<>();
			Sections complexitySections = new Sections();
			complexitySections.setTitle(Constants.COMPLEXITY_ISSUES);
			if (complexity != null) {
				complexitySections.setValue(complexity);
			} else {
				complexitySections.setValue(0D);
			}
			sectionList.add(complexitySections);

			Sections criticalIssuesSections = new Sections();
			criticalIssuesSections.setTitle(Constants.CRITICAL_ISSUES);
			if (criticalIssues != null) {
				criticalIssuesSections.setValue(criticalIssues);
			} else {
				criticalIssuesSections.setValue(0D);
			}
			sectionList.add(criticalIssuesSections);

			Sections blockerIssuesSection = new Sections();
			blockerIssuesSection.setTitle(Constants.BLOCKER_ISSUES);
			if (blockerIssues != null) {
				blockerIssuesSection.setValue(blockerIssues);
			} else {
				blockerIssuesSection.setValue(0D);
			}
			sectionList.add(blockerIssuesSection);
			DrillDownData drillDownData = new DrillDownData();
			drillDownData.setName(month);
			drillDownData.setSections(sectionList);
			drillDownDataList.add(drillDownData);
			drillDown.setDrillDownData(drillDownDataList);

		}
		return drillDown;

	}
}
