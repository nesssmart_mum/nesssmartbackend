package com.nessSmart.service.fetchdata;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.SortedSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nessSmart.Constants.Constants;
import com.nessSmart.Constants.PropertiesConstants;
import com.nessSmart.dao.MongoStorage;
import com.nessSmart.mapping.common.model.JiraProjectVersionDetails;
import com.nessSmart.mapping.database.model.JiraIssueDetails;
import com.nessSmart.mapping.response.model.DrillDown;
import com.nessSmart.mapping.response.model.DrillDownData;
import com.nessSmart.mapping.response.model.ProductionReportingResponse;
import com.nessSmart.mapping.response.model.Sections;
import com.nessSmart.mapping.response.model.WidgetDataAndDrillDown;
import com.nessSmart.mapping.response.model.WidgetResponse;
import com.nessSmart.utils.CommonUtility;
import com.nessSmart.utils.LoadProperties;

@Service
public class JiraFetchDataService {

	@Autowired
	MongoStorage storage;

	@Autowired
	LoadProperties propertyLoader;

	public WidgetResponse getJiraRecoveryTimeSpent() {
		float totalTimespent = 0;
		float totaltimespentlist1 = 0;
		String arrowDirection = Constants.UP;
		try {
			Properties properties = propertyLoader.loadPropertiesFile(PropertiesConstants.JIRA_PROPERTIES_FILENAME);
			String resourceName = properties.getProperty(PropertiesConstants.JIRA_PROJECT_NAME);
			Map<String, String> filters = getJiraRecoverySpentTimeFiltersMap(
					properties.getProperty(PropertiesConstants.JIRA_RECOVERY_STARTDATE_1),
					properties.getProperty(PropertiesConstants.JIRA_RECOVERY_ENDDATE_1), resourceName);

			List<JiraIssueDetails> jiraIssueDetailsList1 = storage.findWithFilters(filters, JiraIssueDetails.class);

			filters = getJiraRecoverySpentTimeFiltersMap(
					properties.getProperty(PropertiesConstants.JIRA_RECOVERY_STARTDATE_2),
					properties.getProperty(PropertiesConstants.JIRA_RECOVERY_ENDDATE_2), resourceName);

			List<JiraIssueDetails> jiraIssueDetailsList2 = storage.findWithFilters(filters, JiraIssueDetails.class);

			for (JiraIssueDetails jiraIssue : jiraIssueDetailsList1) {
				totaltimespentlist1 = totaltimespentlist1 + Float.parseFloat(jiraIssue.getTimespent());

			}
			totaltimespentlist1 = totaltimespentlist1 / jiraIssueDetailsList1.size();
			for (JiraIssueDetails jiraIssue : jiraIssueDetailsList2) {
				totalTimespent = totalTimespent + Float.parseFloat(jiraIssue.getTimespent());
			}
			totalTimespent = totalTimespent / jiraIssueDetailsList2.size();
			if (totaltimespentlist1 < totalTimespent) {
				arrowDirection = Constants.DOWN;
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		DrillDown drillDown = getJiraRecoveryTimeSpentDrillDown();

		WidgetResponse widgetResponse = new WidgetResponse();
		widgetResponse.setName(Constants.RECOVERY_TIMESPENT);
		widgetResponse.setValue(CommonUtility.getHours(Float.toString(totaltimespentlist1)));
		widgetResponse.setArrowValue(arrowDirection);
		widgetResponse.setUnitValue(Constants.HOURS);
		widgetResponse.setDrillDown(drillDown);
		return widgetResponse;
	}

	public List<WidgetResponse> getVersionStatus() {
		List<WidgetResponse> widgetResponseList = new ArrayList<WidgetResponse>();
		try {
			Properties properties = propertyLoader.loadPropertiesFile(PropertiesConstants.JIRA_PROPERTIES_FILENAME);
			String resourceName = properties.getProperty(PropertiesConstants.JIRA_RESOURCENAME);
			Map<String, String> filters = new HashMap<String, String>();
			filters.put(Constants.RESOURCENAME, resourceName);
			filters.put(Constants.CREATED_DATE, properties.getProperty(PropertiesConstants.JIRA_RELEASE_START_DATE));
			filters.put(Constants.CREATE_DATE, properties.getProperty(PropertiesConstants.JIRA_RELEASE_END_DATE));
			List<JiraProjectVersionDetails> jiraVersionDetailsList = storage.findWithFilters(filters,
					JiraProjectVersionDetails.class);
			for (JiraProjectVersionDetails version : jiraVersionDetailsList) {
				WidgetResponse response = new WidgetResponse();
				response.setName(version.getName());
				String value = "";
				if (version.getOverdue() != null) {
					if (Boolean.parseBoolean(version.getOverdue())) {
						Date dt1 = CommonUtility.getDate(Constants.DATE_FORMAT_NO_TIME, version.getReleaseDate());
						long time1 = new Date().getTime();
						long time2 = dt1.getTime();
						long diff = time1 - time2;
						float diffInDays = (float) ((diff) / (1000 * 60 * 60 * 24));

						int delayPeriodAllowed = Integer
								.parseInt(properties.getProperty(PropertiesConstants.DELAY_PERIOD_DAYS_JIRA));
						if (diffInDays < delayPeriodAllowed)
							value = Constants.AMBER;
						else
							value = Constants.RED;
					} else
						value = Constants.GREEN;
				}
				if (!value.isEmpty()) {
					response.setValue(value);
					widgetResponseList.add(response);
				}

			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return widgetResponseList;
	}

	public WidgetDataAndDrillDown getP1Issues() {
		ProductionReportingResponse response = new ProductionReportingResponse();
		List<ProductionReportingResponse> responseList = new ArrayList<>();
		WidgetDataAndDrillDown widgetDataAndDrillDown = new WidgetDataAndDrillDown();

		try {
			Properties properties = propertyLoader.loadPropertiesFile(PropertiesConstants.JIRA_PROPERTIES_FILENAME);
			String resourceName = properties.getProperty(PropertiesConstants.JIRA_PROJECT_NAME);
			String startDate1 = properties.getProperty(PropertiesConstants.JIRA_STARTDATE_1);
			String endDate1 = properties.getProperty(PropertiesConstants.JIRA_ENDDATE_1);
			String startDate2 = properties.getProperty(PropertiesConstants.JIRA_STARTDATE_2);
			String endDate2 = properties.getProperty(PropertiesConstants.JIRA_ENDDATE_2);

			List<String> labelList = new ArrayList<String>();
			labelList.add(Constants.L1);
			labelList.add(Constants.L2);
			labelList.add(Constants.L3);
			for (String label : labelList) {
				List<WidgetResponse> severityList = new ArrayList<WidgetResponse>();

				severityList.add(getSeverityValues(startDate1, endDate1, startDate2, endDate2, resourceName, label,
						Constants.HIGHEST, Constants.SEVERITY_1));

				severityList.add(getSeverityValues(startDate1, endDate1, startDate2, endDate2, resourceName, label,
						Constants.HIGH, Constants.SEVERITY_2));

				severityList.add(getSeverityValues(startDate1, endDate1, startDate2, endDate2, resourceName, label,
						Constants.MEDIUM, Constants.SEVERITY_3));

				if (label.equalsIgnoreCase(Constants.L1))
					response.setL1(severityList);
				else if (label.equalsIgnoreCase(Constants.L2))
					response.setL2(severityList);
				else
					response.setL3(severityList);
			}
			ProductionReportingResponse tempResponse = new ProductionReportingResponse();
			tempResponse.setL3(response.getL3());
			tempResponse.setL2(response.getL2());
			tempResponse.setL1(response.getL1());

			responseList.add(tempResponse);

			DrillDown severityDrillDown = jiraProductionDefectsDrillDown();

			widgetDataAndDrillDown.setWidgetData(responseList);
			widgetDataAndDrillDown.setDrillDown(severityDrillDown);

		} catch (IOException e) {

			e.printStackTrace();
		}
		return widgetDataAndDrillDown;
	}

	private DrillDown jiraProductionDefectsDrillDown() {
		List<JiraIssueDetails> jiraIssueDetailsList = new ArrayList<>();
		DrillDown drillDown = new DrillDown();

		try {
			Properties properties = propertyLoader.loadPropertiesFile(PropertiesConstants.JIRA_PROPERTIES_FILENAME);
			String resourceName = properties.getProperty(PropertiesConstants.JIRA_PROJECT_NAME);
			Map<String, String> filters = getJiraP1IssuesDrillDownFiltersMap(resourceName);
			jiraIssueDetailsList = storage.findWithFilters(filters, JiraIssueDetails.class);

			Map<String, Double> severity1L1Map = new HashMap<>();
			Map<String, Double> severity1L2Map = new HashMap<>();
			Map<String, Double> severity1L3Map = new HashMap<>();

			Map<String, Double> severity2L1Map = new HashMap<>();
			Map<String, Double> severity2L2Map = new HashMap<>();
			Map<String, Double> severity2L3Map = new HashMap<>();

			Map<String, Double> severity3L1Map = new HashMap<>();
			Map<String, Double> severity3L2Map = new HashMap<>();
			Map<String, Double> severity3L3Map = new HashMap<>();

			Double severity1_L1Count = 0D;
			Double severity1_L2Count = 0D;
			Double severity1_L3Count = 0D;

			Double severity2_L1Count = 0D;
			Double severity2_L2Count = 0D;
			Double severity2_L3Count = 0D;

			Double severity3_L1Count = 0D;
			Double severity3_L2Count = 0D;
			Double severity3_L3Count = 0D;

			for (JiraIssueDetails jiraIssueDetails : jiraIssueDetailsList) {
				String dateFormatJira = Constants.DATE_FORMAT_JIRA;
				DateFormat format = new SimpleDateFormat(dateFormatJira);
				String createDateStr = jiraIssueDetails.getCreateDate();
				Date createDate = format.parse(createDateStr);

				Calendar calendar = Calendar.getInstance();
				calendar.setTime(createDate);
				int month = calendar.get(Calendar.MONTH);
				month = month + 1;

				String monthStr = String.format("%02d", month);
				if (jiraIssueDetails.getPriority() != null
						&& jiraIssueDetails.getPriority().equalsIgnoreCase(Constants.HIGHEST)) {
					if (jiraIssueDetails.getLabels() != null && jiraIssueDetails.getLabels().size() > 0) {
						for (String label : jiraIssueDetails.getLabels()) {
							if (label.equalsIgnoreCase("L1")) {
								if (severity1L1Map != null
										&& severity1L1Map.get(properties.getProperty(monthStr)) != null) {
									severity1_L1Count = severity1L1Map.get(properties.getProperty(monthStr));
									severity1_L1Count = severity1_L1Count + 1F;

								} else {
									severity1_L1Count = 1D;
								}

								severity1L1Map.put(properties.getProperty(monthStr), severity1_L1Count);
							}
							if (label.equalsIgnoreCase("L2")) {
								if (severity1L2Map != null
										&& severity1L2Map.get(properties.getProperty(monthStr)) != null) {
									severity1_L2Count = severity1L2Map.get(properties.getProperty(monthStr));
									severity1_L2Count = severity1_L2Count + 1F;

								} else {
									severity1_L2Count = 1D;
								}

								severity1L2Map.put(properties.getProperty(monthStr), severity1_L2Count);
							}
							if (label.equalsIgnoreCase("L3")) {
								if (severity1L3Map != null
										&& severity1L3Map.get(properties.getProperty(monthStr)) != null) {
									severity1_L3Count = severity1L3Map.get(properties.getProperty(monthStr));
									severity1_L3Count = severity1_L3Count + 1F;

								} else {
									severity1_L3Count = 1D;
								}

								severity1L3Map.put(properties.getProperty(monthStr), severity1_L3Count);
							}
						}
					}
				}
				if (jiraIssueDetails.getPriority() != null
						&& jiraIssueDetails.getPriority().equalsIgnoreCase(Constants.HIGH)) {
					if (jiraIssueDetails.getLabels() != null && jiraIssueDetails.getLabels().size() > 0) {
						for (String label : jiraIssueDetails.getLabels()) {
							if (label.equalsIgnoreCase("L1")) {
								if (severity2L1Map != null
										&& severity2L1Map.get(properties.getProperty(monthStr)) != null) {
									severity2_L1Count = severity2L1Map.get(properties.getProperty(monthStr));
									severity2_L1Count = severity2_L1Count + 1F;

								} else {
									severity2_L1Count = 1D;
								}

								severity2L1Map.put(properties.getProperty(monthStr), severity2_L1Count);
							}
							if (label.equalsIgnoreCase("L2")) {
								if (severity2L2Map != null
										&& severity2L2Map.get(properties.getProperty(monthStr)) != null) {
									severity2_L2Count = severity2L2Map.get(properties.getProperty(monthStr));
									severity2_L2Count = severity2_L2Count + 1F;

								} else {
									severity2_L2Count = 1D;
								}

								severity2L2Map.put(properties.getProperty(monthStr), severity2_L2Count);
							}
							if (label.equalsIgnoreCase("L3")) {
								if (severity2L3Map != null
										&& severity2L3Map.get(properties.getProperty(monthStr)) != null) {
									severity2_L3Count = severity2L3Map.get(properties.getProperty(monthStr));
									severity2_L3Count = severity2_L3Count + 1F;

								} else {
									severity2_L3Count = 1D;
								}

								severity2L3Map.put(properties.getProperty(monthStr), severity2_L3Count);
							}
						}
					}
				}
				if (jiraIssueDetails.getPriority() != null
						&& jiraIssueDetails.getPriority().equalsIgnoreCase(Constants.MEDIUM)) {
					if (jiraIssueDetails.getLabels() != null && jiraIssueDetails.getLabels().size() > 0) {
						for (String label : jiraIssueDetails.getLabels()) {
							if (label.equalsIgnoreCase("L1")) {
								if (severity3L1Map != null
										&& severity3L1Map.get(properties.getProperty(monthStr)) != null) {
									severity3_L1Count = severity3L1Map.get(properties.getProperty(monthStr));
									severity3_L1Count = severity3_L1Count + 1F;

								} else {
									severity3_L1Count = 1D;
								}

								severity3L1Map.put(properties.getProperty(monthStr), severity3_L1Count);
							}
							if (label.equalsIgnoreCase("L2")) {
								if (severity3L2Map != null
										&& severity3L2Map.get(properties.getProperty(monthStr)) != null) {
									severity3_L2Count = severity3L2Map.get(properties.getProperty(monthStr));
									severity3_L2Count = severity3_L2Count + 1F;

								} else {
									severity3_L2Count = 1D;
								}

								severity3L2Map.put(properties.getProperty(monthStr), severity3_L2Count);
							}
							if (label.equalsIgnoreCase("L3")) {
								if (severity3L3Map != null
										&& severity3L3Map.get(properties.getProperty(monthStr)) != null) {
									severity3_L3Count = severity3L3Map.get(properties.getProperty(monthStr));
									severity3_L3Count = severity3_L3Count + 1F;

								} else {
									severity3_L3Count = 1D;
								}

								severity3L3Map.put(properties.getProperty(monthStr), severity3_L3Count);
							}
						}
					}
				}

			}
			drillDown = drillDownDetails(severity1L1Map, severity1L2Map, severity1L3Map, severity2L1Map, severity2L2Map,
					severity2L3Map, severity3L1Map, severity3L2Map, severity3L3Map, drillDown);
		} catch (

		Exception e) {
			e.printStackTrace();
		}
		return drillDown;
	}

	private DrillDown drillDownDetails(Map<String, Double> severity1L1Map, Map<String, Double> severity1L2Map,
			Map<String, Double> severity1L3Map, Map<String, Double> severity2L1Map, Map<String, Double> severity2L2Map,
			Map<String, Double> severity2L3Map, Map<String, Double> severity3L1Map, Map<String, Double> severity3L2Map,
			Map<String, Double> severity3L3Map, DrillDown drillDown) {
		String[] monthList = Constants.MONTH_LIST;
		List<DrillDownData> drillDownDataList = new ArrayList<>();
		for (String month : monthList) {

			List<Sections> sections = new ArrayList<>();
			Double s1L1Count = severity1L1Map.get(month);
			Sections s1L1Sections = new Sections();
			s1L1Sections.setTitle(Constants.SEVERITY1_L1);
			if (s1L1Count != null) {
				s1L1Sections.setValue(s1L1Count);
			} else {
				s1L1Sections.setValue(0D);
			}
			sections.add(s1L1Sections);

			Double s1L2Count = severity1L2Map.get(month);
			Sections s1L2Sections = new Sections();
			s1L2Sections.setTitle(Constants.SEVERITY1_L2);
			if (s1L2Count != null) {
				s1L2Sections.setValue(s1L2Count);
			} else {
				s1L2Sections.setValue(0D);
			}
			sections.add(s1L2Sections);

			Double s1L3Count = severity1L3Map.get(month);
			Sections s1L3Sections = new Sections();
			s1L3Sections.setTitle(Constants.SEVERITY1_L3);
			if (s1L3Count != null) {
				s1L3Sections.setValue(s1L3Count);
			} else {
				s1L3Sections.setValue(0D);
			}
			sections.add(s1L3Sections);

			Double s2L1Count = severity2L1Map.get(month);
			Sections s2L1Sections = new Sections();
			s2L1Sections.setTitle(Constants.SEVERITY2_L1);
			if (s2L1Count != null) {
				s2L1Sections.setValue(s2L1Count);
			} else {
				s2L1Sections.setValue(0D);
			}
			sections.add(s2L1Sections);

			Double s2L2Count = severity2L2Map.get(month);
			Sections s2L2Sections = new Sections();
			s2L2Sections.setTitle(Constants.SEVERITY2_L2);
			if (s2L2Count != null) {
				s2L2Sections.setValue(s2L2Count);
			} else {
				s2L2Sections.setValue(0D);
			}
			sections.add(s2L2Sections);

			Double s2L3Count = severity2L3Map.get(month);
			Sections s2L3Sections = new Sections();
			s2L3Sections.setTitle(Constants.SEVERITY2_L3);
			if (s2L3Count != null) {
				s2L3Sections.setValue(s2L3Count);
			} else {
				s2L3Sections.setValue(0D);
			}
			sections.add(s2L3Sections);

			Double s3L1Count = severity3L1Map.get(month);
			Sections s3L1Sections = new Sections();
			s3L1Sections.setTitle(Constants.SEVERITY3_L1);
			if (s3L1Count != null) {
				s3L1Sections.setValue(s3L1Count);
			} else {
				s3L1Sections.setValue(0D);
			}
			sections.add(s3L1Sections);

			Double s3L2Count = severity3L2Map.get(month);
			Sections s3L2Sections = new Sections();
			s3L2Sections.setTitle(Constants.SEVERITY3_L2);
			if (s3L2Count != null) {
				s3L2Sections.setValue(s3L2Count);
			} else {
				s3L2Sections.setValue(0D);
			}
			sections.add(s3L2Sections);

			Double s3L3Count = severity3L3Map.get(month);
			Sections s3L3Sections = new Sections();
			s3L3Sections.setTitle(Constants.SEVERITY3_L3);
			if (s3L3Count != null) {
				s3L3Sections.setValue(s3L3Count);
			} else {
				s3L3Sections.setValue(0D);
			}
			sections.add(s3L3Sections);

			DrillDownData drillDownData = new DrillDownData();
			drillDownData.setName(month);
			drillDownData.setSections(sections);
			drillDownDataList.add(drillDownData);

			drillDown.setxLabel("Year 2017");
			drillDown.setyLabel("Number");
			drillDown.setDrillDownData(drillDownDataList);

		}
		return drillDown;
	}

	public WidgetResponse getSeverityValues(String startDate1, String endDate1, String startDate2, String endDate2,
			String resourceName, String label, String priority, String name) {
		WidgetResponse widgetResponse = new WidgetResponse();

		List<JiraIssueDetails> jiraIssueDetailsList1 = storage.findWithFilters(
				getJiraP1IssuesFiltersMap(startDate1, endDate1, resourceName, label, priority), JiraIssueDetails.class);

		List<JiraIssueDetails> jiraIssueDetailsList2 = storage.findWithFilters(
				getJiraP1IssuesFiltersMap(startDate2, endDate2, resourceName, label, priority), JiraIssueDetails.class);

		int currentCount = jiraIssueDetailsList1.size();
		int preCount = jiraIssueDetailsList2.size();
		String arrowValue = Constants.UP;
		if (currentCount < preCount) {
			arrowValue = Constants.DOWN;
		}
		widgetResponse.setName(name);
		widgetResponse.setValue(Integer.toString(currentCount));
		widgetResponse.setHoverValue(Integer.toString(preCount));
		widgetResponse.setArrowValue(arrowValue);
		return widgetResponse;
	}

	private Map<String, String> getJiraP1IssuesFiltersMap(String date1, String date2, String resourceName, String label,
			String priority) {
		Map<String, String> filters = new HashMap<String, String>();
		filters.put(Constants.RESOURCENAME, resourceName);
		filters.put(Constants.RESOLUTION, Constants.UNRESOLVED);
		filters.put(Constants.PRIORITY, priority);
		filters.put(Constants.LABELS, label);
		filters.put(Constants.CREATED_DATE, CommonUtility.getFormattedDateString(date1 + Constants.START_TIME_APPEND));
		filters.put(Constants.CREATE_DATE, CommonUtility.getFormattedDateString(date2 + Constants.END_TIME_APPEND));
		return filters;
	}

	private Map<String, String> getJiraRecoveryTimeSpentFiltersMap(String date1, String date2, String resourceName) {
		Map<String, String> filters = new HashMap<String, String>();
		filters.put(Constants.RESOURCENAME, resourceName);
		filters.put(Constants.ISSUE_TYPE_NAME, Constants.RECOVERY);
		filters.put(Constants.SUBTASK, Constants.TRUE);
		filters.put(Constants.CREATED_DATE, CommonUtility.getFormattedDateString(date1 + Constants.START_TIME_APPEND));
		filters.put(Constants.CREATE_DATE, CommonUtility.getFormattedDateString(date2 + Constants.END_TIME_APPEND));
		return filters;
	}

	private Map<String, String> getJiraRecoverySpentTimeFiltersMap(String date1, String date2, String resourceName) {
		Map<String, String> filters = new HashMap<String, String>();
		filters.put(Constants.RESOURCENAME, resourceName);
		filters.put(Constants.ISSUE_TYPE_NAME, Constants.RECOVERY);
		filters.put(Constants.SUBTASK, Constants.FALSE);
		filters.put(Constants.CREATED_DATE, CommonUtility.getdDateFormatString(date1));
		filters.put(Constants.CREATE_DATE, CommonUtility.getdDateFormatString(date2));
		return filters;
	}

	private Map<String, String> getJiraP1IssuesDrillDownFiltersMap(String resourceName) {
		Map<String, String> filters = new HashMap<String, String>();
		filters.put(Constants.RESOURCENAME, resourceName);
		filters.put(Constants.RESOLUTION, Constants.UNRESOLVED);

		return filters;
	}

	private DrillDown getJiraRecoveryTimeSpentDrillDown() {
		DrillDown drilDown = new DrillDown();
		try {
			Properties properties = propertyLoader.loadPropertiesFile(PropertiesConstants.JIRA_PROPERTIES_FILENAME);
			String resourceName = properties.getProperty(PropertiesConstants.JIRA_PROJECT_NAME);
			Map<String, String> filters = getJiraRecoveryTimeSpentFiltersDrillDownMap(resourceName);
			List<JiraIssueDetails> jiraIssueDetailsList1 = storage.findWithFilters(filters, JiraIssueDetails.class);
			Map<String, Integer> jiraIssuesMap = new HashMap<String, Integer>();
			for (JiraIssueDetails jiraIssueDetails : jiraIssueDetailsList1) {

				String jiraCreateDate = jiraIssueDetails.getIssueCreateDate();
				String[] month = jiraCreateDate.split("-");
				int espentTime = (Integer.parseInt(jiraIssueDetails.getTimespent()));

				if (jiraIssuesMap.get(month[1]) != null) {
					int timeSpend = (int) jiraIssuesMap.get(month[1]);
					int espendTime = timeSpend + espentTime;
					jiraIssuesMap.put(month[1], espendTime);
				} else {
					jiraIssuesMap.put(month[1], espentTime);
				}
			}

			Set<String> months = jiraIssuesMap.keySet();
			List<String> monthsList = new ArrayList<>(months);
			Collections.sort(monthsList, new Comparator<String>() {
				public int compare(String a, String b) {
					return Integer.parseInt(a) - Integer.parseInt(b);
				}
			});

			List<DrillDownData> drillDownDataList = new ArrayList<DrillDownData>();

			for (String month : monthsList) {
				Integer timeExpent = jiraIssuesMap.get(month);
				String monthlyExpentTime = CommonUtility.getHours(timeExpent.toString());

				drilDown.setxLabel("Year 2017");
				drilDown.setyLabel("Recover Time");
				// drilDown.setTitle(Constants.AVERAGE_RECOVER_TIME);
				DrillDownData drilDownData = new DrillDownData();
				drilDownData.setName(properties.getProperty(month));
				Sections sections = new Sections();
				sections.setTitle("Average Recover Time");
				sections.setValue(Double.parseDouble(monthlyExpentTime));
				List<Sections> sectionList = new ArrayList<>();
				sectionList.add(sections);
				drilDownData.setSections(sectionList);
				// drilDownData.setValue(Float.parseFloat(monthlyExpentTime));
				drillDownDataList.add(drilDownData);
				drilDown.setDrillDownData(drillDownDataList);

			}
			System.out.println(jiraIssuesMap);
		} catch (IOException e) {
			e.fillInStackTrace();

		}
		return drilDown;

	}

	private Map<String, String> getJiraRecoveryTimeSpentFiltersDrillDownMap(String resourceName) {
		Map<String, String> filters = new HashMap<String, String>();
		filters.put(Constants.RESOURCENAME, resourceName);
		filters.put(Constants.ISSUE_TYPE_NAME, Constants.RECOVERY);
		filters.put(Constants.SUBTASK, Constants.FALSE);
		return filters;
	}

}
