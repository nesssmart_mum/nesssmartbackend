package com.nessSmart.service.fetchdata;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nessSmart.Constants.Constants;
import com.nessSmart.dao.MongoStorage;
import com.nessSmart.mapping.common.model.Dashboard;
import com.nessSmart.mapping.common.model.DashboardConfiguration;
import com.nessSmart.mapping.common.model.DashboardData;
import com.nessSmart.mapping.common.model.User;
import com.nessSmart.mapping.common.model.Widget;

@Service
public class DashboardFetchDataServices {

	@Autowired
	MongoStorage storage;
	
	public DashboardData getDashboardsForUser(String userId) {
		DashboardData dashboardData = null;
		Map<String, String> filterMap = new HashMap<String, String>();
		filterMap.put(Constants.USER_ID , userId);
		List<Dashboard> dashboardList =  storage.findWithFilters(filterMap, Dashboard.class);
		if (!dashboardList.isEmpty()) {
			
			Map<String, String> filterUserMap = new HashMap<String, String>();
			filterUserMap.put(Constants.USER_ID , userId);
			List<User> userlist =  storage.findWithFilters(filterUserMap, User.class);
			User user = userlist.get(0);
			DashboardConfiguration dashboardConfiguration = user.getDashboardConfig();
			dashboardData = new DashboardData();
			dashboardData.setDashboardList(dashboardList);
			dashboardData.setUserID(userId);
			dashboardData.setDashboardConfiguration(dashboardConfiguration);
		}
		return dashboardData;
	}
	
	
	
	public List<Widget> getAllWidgets() {
		List<Widget> widgetsList =  storage.findWithFilters(new HashMap<String, String>(), Widget.class);
		return widgetsList;
		
	}
	
	public List<User> getAllUserDetails() {
		List<User> userList =  storage.findWithFilters(new HashMap<String, String>(), User.class);
		return userList;
		
	}
}
