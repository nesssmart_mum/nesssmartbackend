package com.nessSmart.service.fetchdata;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.nessSmart.Constants.Constants;
import com.nessSmart.dao.MongoStorage;
import com.nessSmart.mapping.database.model.Assessment;
import com.nessSmart.mapping.database.model.AssessmentResponse;
import com.nessSmart.mapping.database.model.AssessmentTitles;
import com.nessSmart.mapping.database.model.Heading;
import com.nessSmart.mapping.database.model.Level;
import com.nessSmart.mapping.database.model.Options;
import com.nessSmart.mapping.database.model.Question;
import com.nessSmart.mapping.database.model.Section;

@Service
public class AssessmentFetchDataService {

	@Autowired
	MongoStorage storage;

	public Assessment getAssessmentById(String assessmentId) {
		return (Assessment) storage.getObjectById(Constants.ASSESSMENT_ID, assessmentId, Assessment.class);
	}

	public List<AssessmentTitles> getAssessmentTitles() {
		List<Assessment> assessmentList = storage.findWithFilters(new HashMap<String, String>(), Assessment.class);
		List<AssessmentTitles> assessmentTitleList = new ArrayList<AssessmentTitles>();
		for (Assessment assessment : assessmentList) {
			AssessmentTitles title = new AssessmentTitles();
			title.setAssessmentId(assessment.getAssessmentId());
			title.setTitle(assessment.getTitle());
			assessmentTitleList.add(title);
		}
		return assessmentTitleList;
	}

	public List<Question> getQuestionBank(String projectType) {
		Map<String, String> questionFilterMap = new HashMap<String, String>();
		if (!StringUtils.isEmpty(projectType)) {
			questionFilterMap.put(Constants.PROJECTTYPE, projectType);
		}
		return storage.findWithFilters(questionFilterMap, Question.class);
	}

	public AssessmentResponse getAssessmentResponseById(String assessmentResponseId) {
		return (AssessmentResponse) storage.getObjectById(Constants.ASSESSMENT_RESPONSE_ID, assessmentResponseId,
				AssessmentResponse.class);
	}

	public AssessmentResponse getAssessmentReportById(String assessmentResponseId) {
		AssessmentResponse assessmentResponse = (AssessmentResponse) storage.getObjectById(Constants.ASSESSMENT_RESPONSE_ID,
				assessmentResponseId, AssessmentResponse.class);
		AssessmentResponse response = getTotalScoreForAssessmentResponse(assessmentResponse);
		return response;
	}

	public JSONArray getAssessmentResponseMapping() {
		JSONArray array = new JSONArray();
		try {
			List<AssessmentResponse> assessmentResponseList = storage.findWithFilters(new HashMap<String, String>(),
					AssessmentResponse.class);
			for (AssessmentResponse response : assessmentResponseList) {
				JSONObject jsonObject = new JSONObject();
				jsonObject.put(Constants.ID, response.getAssessmentResponseId());
				jsonObject.put(Constants.TITLE, response.getTitle());
				array.put(jsonObject);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return array;
	}

	public AssessmentResponse getTotalScoreForAssessmentResponse(AssessmentResponse assessmentResponse) {
		int totalScore = 0;
		int totalScored = 0;
		List<Section> sectionList = assessmentResponse.getSectionList();
		for (Section section : sectionList) {
			List<Level> levelList = section.getLevelList();
			for (Level level : levelList) {
				List<Heading> headingList = level.getHeadingList();
				List<Question> questionList = new ArrayList<Question>();
				for (Heading heading : headingList) {
					questionList.addAll(heading.getQuestionList());
					heading.setQuestionList(Collections.emptyList());
				}
				Map<String, Integer> scoreValueMap = processLevelQuestionsData(questionList);
				int levelScore = scoreValueMap.get(Constants.LEVEL_SCORE);
				int levelMaxScore = scoreValueMap.get(Constants.LEVEL_MAXSCORE);
				totalScored += levelScore;
				totalScore += levelMaxScore;
				level.setCompletion(Integer.toString(scoreValueMap.get(Constants.LEVEL_COMPLETION)));
				level.setRating(Integer.toString(scoreValueMap.get(Constants.LEVEL_RATING)));
			}
		}
		int totalPercentage = (totalScored * 100) / totalScore;
		assessmentResponse.setTotalScore(Integer.toString(totalPercentage));
		return assessmentResponse;
	}

	private Map<String, Integer> processLevelQuestionsData(List<Question> levelQuestionsList) {
		Map<String, Integer> scoreValueMap = new HashMap<String, Integer>();
		int levelScore = 0, levelQuestionResponses = 0, levelQuestions = 0, levelMaxScore = 0;
		levelQuestions = levelQuestions + levelQuestionsList.size();
		for (Question question : levelQuestionsList) {
			if (!question.getQuestionType().equalsIgnoreCase(Constants.QUESTION_TYPE_3)) {
				boolean questionResponse = false;
				int maxScore = 0;
				List<Options> optionList = question.getOptions();
				for (Options option : optionList) {
					int score = Integer.parseInt(option.getScore());
					if (question.getQuestionType().equalsIgnoreCase(Constants.QUESTION_TYPE_2)) {
						maxScore += score;
					} else {
						if (score > maxScore) {
							maxScore = score;
						}
					}

					if (option.getSelected() != null && option.getSelected().equalsIgnoreCase(Constants.TRUE)) {
						levelScore += score;
						questionResponse = true;
					}
				}
				levelMaxScore += maxScore;
				if (questionResponse)
					levelQuestionResponses++;
			}
		}
		int levelCompletion = (levelQuestionResponses * 100) / levelQuestions;
		int levelRating = (levelScore * 100 / levelMaxScore);
		scoreValueMap.put(Constants.LEVEL_COMPLETION, levelCompletion);
		scoreValueMap.put(Constants.LEVEL_RATING, levelRating);
		scoreValueMap.put(Constants.LEVEL_MAXSCORE, levelMaxScore);
		scoreValueMap.put(Constants.LEVEL_SCORE, levelScore);
		return scoreValueMap;
	}

}
