package com.nessSmart.service.fetchdata;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nessSmart.Constants.Constants;
import com.nessSmart.Constants.PropertiesConstants;
import com.nessSmart.dao.MongoStorage;
import com.nessSmart.mapping.database.model.ProductionLogsAnalysisReport;
import com.nessSmart.utils.LoadProperties;

@Service
public class ProductionLogsFetchDataService {
	@Autowired
	LoadProperties propertyLoader;

	@Autowired
	MongoStorage storage;
	
	public ProductionLogsAnalysisReport getProductionLogsAnalysisReport() {
		
		Properties properties = null;
		String projectName= null;
		try {
			properties = propertyLoader.loadPropertiesFile(PropertiesConstants.PRODUCTIONS_LOGS_PROPERTIES_FILENAME);
			projectName = properties.getProperty(PropertiesConstants.PRODUCTIONS_LOGS_RESOURCENAME);
		} catch (IOException io){
			System.out.println(io);
		}
		
		Map<String, String> filterMap = new HashMap<String, String>();
		filterMap.put(Constants.PROJECTNAME , projectName);
		List<ProductionLogsAnalysisReport> productionLogsAnalysisReport =  storage.findWithFilters(filterMap, ProductionLogsAnalysisReport.class);
		for (ProductionLogsAnalysisReport productionLogsAnalysisReport2 :  productionLogsAnalysisReport) {
			return productionLogsAnalysisReport2;
		}
		return new ProductionLogsAnalysisReport();
	}

}
