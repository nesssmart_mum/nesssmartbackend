package com.nessSmart.service.fetchdata;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nessSmart.Constants.Constants;
import com.nessSmart.Constants.PropertiesConstants;
import com.nessSmart.dao.MongoStorage;
import com.nessSmart.mapping.common.model.Builds;
import com.nessSmart.mapping.common.model.JenkinsProjectBuildDetails;
import com.nessSmart.mapping.response.model.DrillDown;
import com.nessSmart.mapping.response.model.DrillDownForFailureRate;
import com.nessSmart.mapping.response.model.WidgetResponse;
import com.nessSmart.utils.CommonUtility;
import com.nessSmart.utils.LoadProperties;

@Service
public class BuildFailureRateProdService {

	@Autowired
	MongoStorage storage;

	@Autowired
	LoadProperties propertyLoader;

	public WidgetResponse getFailureRate() {

		WidgetResponse widgetResponse = new WidgetResponse();

		try {
			Properties properties = propertyLoader
					.loadPropertiesFile(PropertiesConstants.RELEASE_STATUS_PROPERTIES_FILENAME);
			Map<String, String> filters = new HashMap<String, String>();
			filters.put(Constants.PROJECTNAME, properties.getProperty(Constants.PROJECTNAME));
			List<JenkinsProjectBuildDetails> jenkinsProjectDetails = storage.findWithFilters(filters,
					JenkinsProjectBuildDetails.class);
			List<Builds> totalBuildDetails = jenkinsProjectDetails.get(0).getBuilds();

			float rollbackCount = 0;
			float patchCount = 0;
			float totalbuildCount = 0;
			float hotCount = 0;

			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
			/*		
			String currentDateStr = simpleDateFormat.format(new Date());
			Date currentDate = simpleDateFormat.parse(currentDateStr);
			Calendar currentDateCal = Calendar.getInstance();
			currentDateCal.setTime(currentDate);
			String minusDays = properties.getProperty(PropertiesConstants.MIUNSMONTH);
			int numberOfminusDays = Integer.parseInt(minusDays);
			Calendar cal = Calendar.getInstance();
			cal.setTime(currentDate);
			cal.add(Calendar.DATE, -numberOfminusDays);
			Date dateBefore30Days = cal.getTime();
			*/
			
			
			Date startdate1 = CommonUtility.getDate(Constants.DATE_FORMAT_NO_TIME,
					properties.getProperty(PropertiesConstants.FAILURE_RELEASE_STARTDATE_1));
			Date enddate1 = CommonUtility.getDate(Constants.DATE_FORMAT_NO_TIME,
					properties.getProperty(PropertiesConstants.FAILURE_RELEASE_ENDDATE_1));
			Date startdate2 = CommonUtility.getDate(Constants.DATE_FORMAT_NO_TIME,
					properties.getProperty(PropertiesConstants.FAILURE_RELEASE_STARTDATE_2));
			Date enddate2 = CommonUtility.getDate(Constants.DATE_FORMAT_NO_TIME,
					properties.getProperty(PropertiesConstants.FAILURE_RELEASE_ENDDATE_2));
			
			
			
			

			String env = properties.getProperty(PropertiesConstants.RELEASE_FAILURE_RATE_PROD_ENV);
			String rollback = properties.getProperty(PropertiesConstants.RELEASE_BUILD_PARAMETER_ROLLBACK);
			String patch = properties.getProperty(PropertiesConstants.RELEASE_BUILD_PARAMETER_PATCH);
			String hot = properties.getProperty(PropertiesConstants.RELEASE_BUILD_PARAMETER_HOT);

			for (Builds build : totalBuildDetails) {
				String createDateStr = build.getCreateDate();
				Date createDate = simpleDateFormat.parse(createDateStr);

				if (/*CommonUtility.isDateBetween(createDate, dateBefore30Days, currentDate)*/
						CommonUtility.isDateBetween(createDate, startdate1, enddate1)
						&& build.getEnvironment().equalsIgnoreCase(env)) {
					if(build.getDeploymentType() != null){
					if (build.getDeploymentType().equalsIgnoreCase(rollback)) {
						rollbackCount = rollbackCount + 1;
					}
					if (build.getDeploymentType().equalsIgnoreCase(hot)) {
						hotCount = hotCount + 1;
					}
					if (build.getDeploymentType().equalsIgnoreCase(patch)) {
						patchCount = patchCount + 1;
					}
				}
					totalbuildCount = totalbuildCount + 1;
				}
			}
			float productionRatefailureCurrentMonth = (((rollbackCount + patchCount + hotCount) / totalbuildCount))
					* Constants.HUNDRED;

			if (String.valueOf(productionRatefailureCurrentMonth).equalsIgnoreCase("NaN")) {
				productionRatefailureCurrentMonth = 0f;
			}

			/*Calendar calPre = Calendar.getInstance();
			calPre.setTime(dateBefore30Days);
			calPre.add(Calendar.DATE, -numberOfminusDays);
			Date calPreWeek = calPre.getTime();*/

			float preRollbackCount = 0;
			float prePatchCount = 0;
			float preTotalbuildCount = 0;
			float preHotCount = 0;

			for (Builds prebuild : totalBuildDetails) {
				String createDateStr = prebuild.getCreateDate();
				Date createDate = simpleDateFormat.parse(createDateStr);
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(createDate);

				if (/*CommonUtility.isDateBetween(createDate, calPreWeek, dateBefore30Days)*/
						CommonUtility.isDateBetween(createDate, startdate2, enddate2)
						&& prebuild.getEnvironment().equalsIgnoreCase(env)) {
					/*if (CommonUtility.isDateBetween(createDate, dateBefore30Days, currentDate)
							&& prebuild.getEnvironment().equalsIgnoreCase(env)) {*/
						if (prebuild.getDeploymentType().equalsIgnoreCase(rollback)) {
							preRollbackCount = preRollbackCount + 1;
						}
						if (prebuild.getDeploymentType().equalsIgnoreCase(hot)) {
							preHotCount = preHotCount + 1;
						}
						if (prebuild.getDeploymentType().equalsIgnoreCase(patch)) {
							prePatchCount = prePatchCount + 1;
						}

						preTotalbuildCount = preTotalbuildCount + 1;
					/*}*/
				}

			}

			float productionRatefailurePreviousMonth = (((preRollbackCount + prePatchCount + preHotCount)
					/ preTotalbuildCount)) * Constants.HUNDRED;
			if (String.valueOf(productionRatefailurePreviousMonth).equalsIgnoreCase("NaN")) {
				productionRatefailurePreviousMonth = 0f;
			}

			String arrowDirection = Constants.DOWN, percentageValue;
			if (productionRatefailureCurrentMonth > productionRatefailurePreviousMonth) {
				arrowDirection = Constants.UP;
			}

			percentageValue = String.valueOf(productionRatefailureCurrentMonth);
			percentageValue = percentageValue.equalsIgnoreCase("Infinity") || percentageValue.equalsIgnoreCase("NaN")
					? "0" : percentageValue;
			widgetResponse.setName(Constants.CHANGE_FAILURE_RATE_IN_PRODUCTION);
			widgetResponse.setArrowValue(arrowDirection);
			widgetResponse.setValue(percentageValue);

			DrillDown drillDown = getFailureRateDrillDown();
			widgetResponse.setDrillDown(drillDown);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return widgetResponse;
	}

	private DrillDown getFailureRateDrillDown() {
		Properties properties;
		Map<Integer, List<String>> releaseMonthMap = new HashMap<Integer, List<String>>();
		DrillDown failureRateDrillDown = new DrillDown();

		try {
			properties = propertyLoader.loadPropertiesFile(PropertiesConstants.JENKINS_PROPERTIES_FILENAME);
			Properties rsProperties = propertyLoader
					.loadPropertiesFile(PropertiesConstants.RELEASE_STATUS_PROPERTIES_FILENAME);
			String projectName = properties.getProperty(PropertiesConstants.PROJECT_NAME);
			String env = rsProperties.getProperty(PropertiesConstants.RELEASE_FAILURE_RATE_PROD_ENV);
			Map<String, String> filters = new HashMap<String, String>();
			filters.put(Constants.PROJECTNAME, projectName);
			List<JenkinsProjectBuildDetails> jenkinsProjectDetails = storage.findWithFilters(filters,
					JenkinsProjectBuildDetails.class);
			List<Builds> totalBuildDetails = jenkinsProjectDetails.get(0).getBuilds();
			for (Builds build : totalBuildDetails) {
				if (build.getEnvironment().equalsIgnoreCase(env)) {
					if (build.getDeploymentType()!= null &&(build.getDeploymentType().equalsIgnoreCase(
							rsProperties.getProperty(PropertiesConstants.RELEASE_BUILD_PARAMETER_ROLLBACK))
							|| build.getDeploymentType().equalsIgnoreCase(
									rsProperties.getProperty(PropertiesConstants.RELEASE_BUILD_PARAMETER_PATCH))
							|| build.getDeploymentType().equalsIgnoreCase(
									rsProperties.getProperty(PropertiesConstants.RELEASE_BUILD_PARAMETER_HOT)))) {
						Calendar calendar = Calendar.getInstance();
						calendar.setTimeInMillis(build.getTimestamp());
						int month = calendar.get(Calendar.MONTH);

						String buildVersion = build.getBuildVersion();

						if (releaseMonthMap.get(month) != null) {
							List<String> buildVersionList = releaseMonthMap.get(month);
							buildVersionList.add(buildVersion);
							releaseMonthMap.put(month, buildVersionList);
						} else {
							List<String> buildVersionList = new ArrayList<String>();
							buildVersionList.add(buildVersion);
							releaseMonthMap.put(month, buildVersionList);
						}

					}
				}
			}
			Set<Integer> months = releaseMonthMap.keySet();
			List<DrillDownForFailureRate> drillDownForFailureRateList = new ArrayList<DrillDownForFailureRate>();

			for (int month : months) {

				List<String> buildVersions = releaseMonthMap.get(month);
				DrillDownForFailureRate drillDownForFailureRate = new DrillDownForFailureRate();

				drillDownForFailureRate.setMonth(properties.getProperty(Integer.toString(month)));
				drillDownForFailureRate.setBuildVersion(buildVersions);

				drillDownForFailureRateList.add(drillDownForFailureRate);
				// developmentPipelineTrendWidgetDrillDown.setDrillDownForFailureRate(drillDownForFailureRateList);
				// failureRateDrillDown.setTitle("Change Failure Rate");
				failureRateDrillDown.setDrillDownData(drillDownForFailureRateList);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		return failureRateDrillDown;

	}

	private Map<String, String> getFilterByDeploymentParameter(String env, String date1, String date2,
			String deploymentType) {
		Map<String, String> filters = new HashMap<String, String>();
		filters.put(Constants.RELEASE_DEPLOYMENT_TYPE, deploymentType);
		filters.put(Constants.RELEASE_PROD_ENVIRONMENT, env);
		filters.put(Constants.CREATED_DATE, CommonUtility.getFormattedDateString(date1 + Constants.START_TIME_APPEND));
		filters.put(Constants.CREATE_DATE, CommonUtility.getFormattedDateString(date2 + Constants.END_TIME_APPEND));
		return filters;
	}

	private Map<String, String> getTotalBuildInProd(String env, String date1, String date2) {
		Map<String, String> filters = new HashMap<String, String>();
		filters.put(Constants.RELEASE_PROD_ENVIRONMENT, env);
		filters.put(Constants.CREATED_DATE, CommonUtility.getFormattedDateString(date1 + Constants.START_TIME_APPEND));
		filters.put(Constants.CREATE_DATE, CommonUtility.getFormattedDateString(date2 + Constants.END_TIME_APPEND));
		return filters;
	}

}
