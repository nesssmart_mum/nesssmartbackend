package com.nessSmart.service.savedata;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import com.nessSmart.Constants.Constants;
import com.nessSmart.Constants.ParametersConstants;
import com.nessSmart.dao.MongoStorage;
import com.nessSmart.mapping.common.model.JiraProjectVersionDetails;
import com.nessSmart.mapping.database.model.JiraIssueDetails;
import com.nessSmart.mapping.json.model.JiraDetails;
import com.nessSmart.utils.CommonUtility;
import com.nessSmart.utils.RestTemplateUtil;

@Service
public class JiraSaveDataService {

	@Autowired
	RestTemplateUtil restService;

	@Autowired
	MongoStorage storage;

	public void saveJiraData(Map<String, String> jiraPropertiesMap) {
		try {
			int startAt = 0;
			boolean isLastPage = false;
			String resourceName = jiraPropertiesMap.get(Constants.RESOURCENAME);
			Map<String, String> jiraParametersMap = new HashMap<String, String>();
			jiraParametersMap.put(ParametersConstants.JIRA_RESOURCE_KEY, resourceName);
			while (!isLastPage) {
				jiraParametersMap.put(ParametersConstants.START_AT, Integer.toString(startAt));
				String jsonObject = restService.getRestResponseWithCredentials(jiraParametersMap, jiraPropertiesMap,
						HttpMethod.GET);
				JiraDetails details = CommonUtility.processJiraData(jsonObject);
				List<JiraIssueDetails> jiraIssueDetailsList = CommonUtility.populateJiraIssueDetails(details,
						resourceName);
				for (JiraIssueDetails jiraIssueDetails : jiraIssueDetailsList)
					storage.saveObject(jiraIssueDetails);

				startAt += jiraIssueDetailsList.size();
				if (startAt == details.getTotal())
					isLastPage = true;
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void saveJiraProjectVersionDetails(Map<String, String> jiraPropertiesMap) {
		try {
			String resourceName = jiraPropertiesMap.get(Constants.RESOURCENAME);
			Map<String, String> jiraParametersMap = new HashMap<String, String>();
			String jsonArray = restService.getRestResponseWithCredentials(jiraParametersMap, jiraPropertiesMap,
					HttpMethod.GET);
			List<JiraProjectVersionDetails> versionList = CommonUtility.convertJsonToList(jsonArray,
					JiraProjectVersionDetails.class);
			for (JiraProjectVersionDetails jiraVersionDetails : versionList) {
				jiraVersionDetails.setProjectVersionId(
						jiraVersionDetails.getProjectId() + Constants.COLON + jiraVersionDetails.getId());
				jiraVersionDetails.setCreatedDate(jiraVersionDetails.getReleaseDate());
				jiraVersionDetails.setCreateDate(jiraVersionDetails.getReleaseDate());
				jiraVersionDetails.setResourceName(resourceName);
				storage.saveObject(jiraVersionDetails);
			}

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public String getProjectIdFromJira(Map<String, String> jiraPropertiesMap) {
		try {
			String resourceName = jiraPropertiesMap.get(Constants.RESOURCENAME);
			Map<String, String> jiraParametersMap = new HashMap<String, String>();
			String jsonArray = restService.getRestResponseWithCredentials(jiraParametersMap, jiraPropertiesMap,
					HttpMethod.GET);
			JSONArray jArray;
			try {
				jArray = new JSONArray(jsonArray);
				if (jArray != null) {
					for (int i = 0; i < jArray.length(); i++) {
						JSONObject jsonObject = (JSONObject) jArray.get(i);
						String name = (String) jsonObject.get(Constants.NAME);
						if (name.equalsIgnoreCase(resourceName))
							return (String) jsonObject.get(Constants.ID);

					}
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;

	}
}