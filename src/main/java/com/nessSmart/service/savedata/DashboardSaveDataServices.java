package com.nessSmart.service.savedata;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nessSmart.Constants.Constants;
import com.nessSmart.dao.MongoStorage;
import com.nessSmart.mapping.common.model.Dashboard;
import com.nessSmart.mapping.common.model.Widget;

@Service
public class DashboardSaveDataServices {
	@Autowired
	MongoStorage storage;

	public String saveAnyObject(Object object) {
		Dashboard dashboard = (Dashboard) storage.saveObject(object);
		return dashboard.getDashboardId();
	}

	public boolean deleteDashboard(String dashboardId) {
		return storage.deleteObjectById(Constants.DASHBOARD_ID, dashboardId, Dashboard.class);
	}
	// save WidgetMaster Details
	public String saveWidgetsMasterDetails(Object object) {
		Widget widgetsDeatails = (Widget) storage.saveObject(object);
		return widgetsDeatails.getWidgetId();
	}

}
