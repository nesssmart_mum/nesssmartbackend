package com.nessSmart.service.savedata;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import com.nessSmart.dao.MongoStorage;
import com.nessSmart.mapping.common.model.Issues;
import com.nessSmart.mapping.common.model.Measures;
import com.nessSmart.utils.CommonUtility;
import com.nessSmart.utils.LoadProperties;
import com.nessSmart.utils.RestTemplateUtil;

@Service
public class SonarSaveDataService {

	@Autowired
	LoadProperties propertyLoader;

	@Autowired
	RestTemplateUtil restService;

	@Autowired
	MongoStorage storage;

	public List<Measures> getSonarqubeUnitTestCoverage(Map<String, String> propertiesMap) {
		String jsonResponse = null;
		List<Measures> measureList = null;
		try {
			jsonResponse = restService.getRestResponseWithCredentials(createJenkinsParametersForBuildData(),
					propertiesMap, HttpMethod.GET);
			measureList = CommonUtility.populateSonarqubeCoveragehistory(jsonResponse);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return measureList;
	}

	public List<Issues> getSonarqubeIssues(Map<String, String> propertiesMap) {
		String jsonResponse = null;
		List<Issues> issueList = null;
		try {
			jsonResponse = restService.getRestResponseWithCredentials(createJenkinsParametersForBuildData(),
					propertiesMap, HttpMethod.GET);
			issueList = CommonUtility.populateSonarIssues(jsonResponse);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return issueList;
	}

	private Map<String, String> createJenkinsParametersForBuildData() throws IOException {
		Map<String, String> jenkinsParametersMap = new HashMap<String, String>();
		return jenkinsParametersMap;
	}

}
