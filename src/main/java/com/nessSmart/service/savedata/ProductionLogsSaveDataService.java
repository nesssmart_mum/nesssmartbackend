package com.nessSmart.service.savedata;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nessSmart.Constants.PropertiesConstants;
import com.nessSmart.dao.MongoStorage;
import com.nessSmart.mapping.database.model.ProductionLogsAnalysisReport;
import com.nessSmart.mapping.json.model.ProductionLogsAnalysis;
import com.nessSmart.utils.LoadProperties;

@Service
public class ProductionLogsSaveDataService {

	@Autowired
	MongoStorage storage;

	@Autowired
	LoadProperties propertyLoader;

	public boolean readProductionLogsAndSaveInDb() {

		String resourceName = null;
		Map<String, List<String>> exceptionMap = new HashMap<String, List<String>>();
		Properties properties = null;
		List<ProductionLogsAnalysis> productionLogsAnalysislist = new ArrayList<ProductionLogsAnalysis>();
		ProductionLogsAnalysisReport productionLogsAnalysisReport = new ProductionLogsAnalysisReport();
		String prodLogsPath = null;

		try {
			properties = propertyLoader.loadPropertiesFile(PropertiesConstants.PRODUCTIONS_LOGS_PROPERTIES_FILENAME);
			resourceName = properties.getProperty(PropertiesConstants.PRODUCTIONS_LOGS_EXCEPTIONS);
			prodLogsPath = properties.getProperty(PropertiesConstants.PRODUCTION_LOGS_PATH);
			List<String> exceptionList = Arrays.asList(resourceName.split(",[ ]*"));

			FileInputStream fstream = new FileInputStream(prodLogsPath);
			BufferedReader br = new BufferedReader(new InputStreamReader(fstream));

			List<String> list = new ArrayList<String>();
			String strLine;
			/* read log line by line */
			while ((strLine = br.readLine()) != null) {
				for (String exception : exceptionList) {
					if (strLine.contains(exception)) {

						if (exceptionMap.containsKey(exception)) {
							List<String> logsExceptionList = exceptionMap.get(exception);
							logsExceptionList.add(strLine);
							exceptionMap.put(exception, logsExceptionList);

						} else {
							List<String> logsExceptionList = new ArrayList<String>();
							logsExceptionList.add(strLine);
							exceptionMap.put(exception, logsExceptionList);
						}

						System.out.println(list.size());
					}
				}

			}

			Set<String> exceptionSet = exceptionMap.keySet();
			for (String exception : exceptionSet) {
				List<String> logsExceptionList = exceptionMap.get(exception);
				ProductionLogsAnalysis productionLogsAnalysis = new ProductionLogsAnalysis();
				properties.getProperty(exception);
				String exceptionName  = exception.replace(exception, properties.getProperty(exception));
		        productionLogsAnalysis.setExceptionName(exceptionName);
				productionLogsAnalysis.setCount(logsExceptionList.size());
				productionLogsAnalysislist.add(productionLogsAnalysis);

			}

			productionLogsAnalysisReport
					.setProjectName(properties.getProperty(PropertiesConstants.PRODUCTIONS_LOGS_RESOURCENAME));
			productionLogsAnalysisReport.setProductionLogsAnalysisList(productionLogsAnalysislist);
			storage.saveObject(productionLogsAnalysisReport);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (Exception e) {
			System.err.println("Error: " + e.getMessage());
		}
		return true;

	}
}
