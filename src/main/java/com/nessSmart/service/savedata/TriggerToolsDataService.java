package com.nessSmart.service.savedata;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nessSmart.Constants.Constants;
import com.nessSmart.Constants.PropertiesConstants;
import com.nessSmart.service.fetchdata.DevPipelineTrendWidgetJenkinsService;
import com.nessSmart.utils.CommonUtility;
import com.nessSmart.utils.LoadProperties;

@Service
public class TriggerToolsDataService {

	@Autowired
	LoadProperties propertyLoader;

	@Autowired
	JenkinsSaveDataService jenkinsSaveDataService;

	@Autowired
	JiraSaveDataService jiraService;

	@Autowired
	DevPipelineTrendWidgetJenkinsService devPipelineTrendWidgetJenkinsService;

	public void fetchDataFromJenkins() {

		try {
			Properties jenkinsProperties = propertyLoader
					.loadPropertiesFile(PropertiesConstants.JENKINS_PROPERTIES_FILENAME);
			String resourceName = jenkinsProperties.getProperty(PropertiesConstants.JENKINS_RESOURCENAME);
			Properties sonarProperties = propertyLoader
					.loadPropertiesFile(PropertiesConstants.SONARQUBE_PROPERTIES_FILENAME);

			jenkinsSaveDataService
					.getJenkinsBuildDataForQualityWidget(propertiesMapForJenkinsBuildData(jenkinsProperties));

			jenkinsSaveDataService.saveSonarDataForQualityWidget(propertiesMapForSonarqubeIssues(sonarProperties),
					propertiesMapForSonarqubeUnitTestCoverage(sonarProperties), resourceName);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void fetchJiraDetails() {
		try {

			Properties properties = propertyLoader.loadPropertiesFile(PropertiesConstants.JIRA_PROPERTIES_FILENAME);
			String endpointUrl = properties.getProperty(PropertiesConstants.JIRA_ENDPOINTURL);
			String userName = properties.getProperty(PropertiesConstants.JIRA_USERNAME);
			String password = properties.getProperty(PropertiesConstants.JIRA_PASSWORD);
			String resourceName = properties.getProperty(PropertiesConstants.JIRA_PROJECT_NAME);

			String resourceId = jiraService.getProjectIdFromJira(CommonUtility.getPropertiesMap(endpointUrl,
					properties.getProperty(PropertiesConstants.JIRA_GET_PROJECT_DETAILS_ENDURL), userName, password,
					resourceName));

			jiraService.saveJiraData(CommonUtility.getPropertiesMap(endpointUrl,
					properties.getProperty(PropertiesConstants.JIRA_ISSUES_ENDURL), userName, password, resourceName));

			String endUrl = properties.getProperty(PropertiesConstants.JIRA_VERSIONS_ENDURL);
			endUrl = endUrl.replace(Constants.RESOURCEID, resourceId);

			jiraService.saveJiraProjectVersionDetails(
					CommonUtility.getPropertiesMap(endpointUrl, endUrl, userName, password, resourceName));
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private Map<String, String> propertiesMapForJenkinsBuildData(Properties properties) throws IOException {
		String endPointUrl = properties.getProperty(PropertiesConstants.JENKINS_ENDPOINTURL);
		String endUrl = properties.getProperty(PropertiesConstants.JENKINS_JOB_ENDURL);
		String resourceName = properties.getProperty(PropertiesConstants.JENKINS_RESOURCENAME);
		endUrl = endUrl.replace(Constants.RESOURCENAME, resourceName);
		String userName = properties.getProperty(PropertiesConstants.JENKINS_USERNAME);
		String password = properties.getProperty(PropertiesConstants.JENKINS_PASSWORD);
		return CommonUtility.getPropertiesMap(endPointUrl, endUrl, userName, password, resourceName);
	}

	private Map<String, String> propertiesMapForSonarqubeUnitTestCoverage(Properties properties) throws IOException {
		String endPointUrl = properties.getProperty(PropertiesConstants.SONARQUBE_ENDPOINTURL);
		String endUrl = properties.getProperty(PropertiesConstants.SONARQUBE_UNITTEST_COVERAGE_ENDURL);
		String resourceName = properties.getProperty(PropertiesConstants.SONARQUBE_RESOURCENAME);
		endUrl = endUrl.replace(Constants.RESOURCENAME, resourceName);
		String userName = properties.getProperty(PropertiesConstants.SONARQUBE_USERNAME);
		String password = properties.getProperty(PropertiesConstants.SONARQUBE_PASSWORD);
		return CommonUtility.getPropertiesMap(endPointUrl, endUrl, userName, password, resourceName);
	}

	private Map<String, String> propertiesMapForSonarqubeIssues(Properties properties) throws IOException {
		String endPointUrl = properties.getProperty(PropertiesConstants.SONARQUBE_ENDPOINTURL);
		String endUrl = properties.getProperty(PropertiesConstants.SONARQUBE_ISSUES_ENDURL);
		String resourceName = properties.getProperty(PropertiesConstants.SONARQUBE_RESOURCENAME);
		endUrl = endUrl.replace(Constants.RESOURCENAME, resourceName);
		String userName = properties.getProperty(PropertiesConstants.SONARQUBE_USERNAME);
		String password = properties.getProperty(PropertiesConstants.SONARQUBE_PASSWORD);
		return CommonUtility.getPropertiesMap(endPointUrl, endUrl, userName, password, resourceName);
	}

	public Map<String, String> fetchBuildDeploymentTypeDetails(Properties properties) throws IOException {

		String endPointUrl = properties.getProperty(PropertiesConstants.JENKINS_ENDPOINTURL);
		String endUrl = properties.getProperty(PropertiesConstants.JENKINS_JOB_DEPLOMENT_TYPE_ENDURL);
		String resourceName = properties.getProperty(PropertiesConstants.JENKINS_RESOURCENAME);
		endUrl = endUrl.replace(Constants.RESOURCENAME, resourceName);
		String userName = properties.getProperty(PropertiesConstants.JENKINS_USERNAME);
		String password = properties.getProperty(PropertiesConstants.JENKINS_PASSWORD);
		return CommonUtility.getPropertiesMap(endPointUrl, endUrl, userName, password, resourceName);

	}

	public void saveFunctionalTestingStatus() {
		try {
			Properties jenkinsProperties = propertyLoader
					.loadPropertiesFile(PropertiesConstants.JENKINS_PROPERTIES_FILENAME);
			String projectName = jenkinsProperties.getProperty(PropertiesConstants.PROJECT_NAME);

			String devTestingProjectName = jenkinsProperties.getProperty(PropertiesConstants.DEV_RESOURCENAME);
			String intlTestingProjectName = jenkinsProperties.getProperty(PropertiesConstants.INT_RESOURCENAME);
			String sitTestingProjectName = jenkinsProperties.getProperty(PropertiesConstants.TEST_RESOURCENAME);
			String prodTestingProjectName = jenkinsProperties.getProperty(PropertiesConstants.PROD_RESOURCENAME);

			List<String> automationTestEnvList = new ArrayList<String>();
			automationTestEnvList.add(devTestingProjectName);
			automationTestEnvList.add(intlTestingProjectName);
			automationTestEnvList.add(sitTestingProjectName);
			automationTestEnvList.add(prodTestingProjectName);
			devPipelineTrendWidgetJenkinsService.saveFunctionalTestingStatus(projectName, automationTestEnvList);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
