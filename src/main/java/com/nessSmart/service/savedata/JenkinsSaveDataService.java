package com.nessSmart.service.savedata;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.nessSmart.Constants.Constants;
import com.nessSmart.dao.MongoStorage;
import com.nessSmart.mapping.common.model.Builds;
import com.nessSmart.mapping.common.model.JenkinsProjectDetails;
import com.nessSmart.mapping.database.model.JenkinsQualityWidgetBuildDetails;
import com.nessSmart.mapping.database.model.SonarDataForQualityWidget;
import com.nessSmart.utils.CommonUtility;
import com.nessSmart.utils.LoadProperties;
import com.nessSmart.utils.RestTemplateUtil;

@Service
public class JenkinsSaveDataService {

	@Autowired
	LoadProperties propertyLoader;

	@Autowired
	RestTemplateUtil restService;

	@Autowired
	MongoStorage storage;

	@Autowired
	SonarSaveDataService sonarService;

	public void getJenkinsBuildDataForQualityWidget(Map<String, String> propertiesMap) {
		try {
			String resourceName = propertiesMap.get(Constants.RESOURCENAME);
			String jsonResponse = restService.getRestResponseWithCredentials(createJenkinsParametersForBuildData(),
					propertiesMap, HttpMethod.GET);
			ObjectMapper mapper = new ObjectMapper();
			JenkinsProjectDetails projectDetails = mapper.readValue(jsonResponse, JenkinsProjectDetails.class);
			List<Builds> buildsList = projectDetails.getBuilds();
			for (Builds build : buildsList) {
				String timeStamp = CommonUtility.getConvertedDate(build.getTimestamp());
				JenkinsQualityWidgetBuildDetails details = new JenkinsQualityWidgetBuildDetails();
				details.setId(resourceName + build.getNumber());
				details.setNumber(Integer.parseInt(build.getNumber()));
				details.setResult(build.getResult());
				details.setCreateDate(timeStamp);
				details.setCreatedDate(timeStamp);
				details.setResourceName(resourceName);
				storage.saveObject(details);
			}
		} catch (IOException e) {
			
			e.printStackTrace();
		}
	}

	public void saveSonarDataForQualityWidget(Map<String, String> propertiesMapSonarIssues,
			Map<String, String> propertiesMapSoanarCoverage, String resourceName) {
		SonarDataForQualityWidget sonarData = new SonarDataForQualityWidget();
		String projectName = propertiesMapSoanarCoverage.get(Constants.RESOURCENAME);
		sonarData.setIssueList(sonarService.getSonarqubeIssues(propertiesMapSonarIssues));
		sonarData.setMeasureList(sonarService.getSonarqubeUnitTestCoverage(propertiesMapSoanarCoverage));
		sonarData.setResourceName(projectName + resourceName);
		sonarData.setProjectName(projectName);
		sonarData.setEnv(resourceName);
		storage.saveObject(sonarData);
	}

	private Map<String, String> createJenkinsParametersForBuildData() throws IOException {
		Map<String, String> jenkinsParametersMap = new HashMap<String, String>();
		return jenkinsParametersMap;
	}

	public void getfailureRateInProduction(Map<String, String> propertiesMap) {
		try {
			String resourceName = propertiesMap.get(Constants.RESOURCENAME);
			String jsonResponse = restService.getRestResponseWithCredentials(createJenkinsParametersForBuildData(),
					propertiesMap, HttpMethod.GET);
			ObjectMapper mapper = new ObjectMapper();
			JenkinsProjectDetails projectDetails = mapper.readValue(jsonResponse, JenkinsProjectDetails.class);
			List<Builds> buildsList = projectDetails.getBuilds();
			for (Builds build : buildsList) {
				String timeStamp = CommonUtility.getConvertedDate(build.getTimestamp());
				JenkinsQualityWidgetBuildDetails details = new JenkinsQualityWidgetBuildDetails();
				details.setId(resourceName + build.getNumber());
				details.setNumber(Integer.parseInt(build.getNumber()));
				details.setResult(build.getResult());
				details.setCreateDate(timeStamp);
				details.setCreatedDate(timeStamp);
				details.setResourceName(resourceName);

			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
