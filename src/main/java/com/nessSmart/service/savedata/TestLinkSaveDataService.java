package com.nessSmart.service.savedata;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.Properties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nessSmart.Constants.PropertiesConstants;
import com.nessSmart.dao.MongoStorage;
import com.nessSmart.dao.TestLinkDataDao;
import com.nessSmart.mapping.database.model.TestLink;
import com.nessSmart.mapping.database.model.TestLinkBuilds;
import com.nessSmart.mapping.database.model.TestLinkData;
import com.nessSmart.utils.LoadProperties;


@Service
public class TestLinkSaveDataService {
	
	
	@Autowired
	LoadProperties propertyLoader;

	@Autowired
	MongoStorage storage;

	public boolean saveTestLinkDataToDb(String projectId) {
		try {

			TestLinkDataDao tst = new TestLinkDataDao();
			TestLinkData tst1 = tst.getTcAutomationManualCount(projectId);
			TestLinkData tst2 = tst.getExecPassFailCount(projectId);

			TestLinkData data = new TestLinkData();
			data.setId(projectId);
			data.setBuildId(tst2.getBuildId());
			data.setPassCountTC(tst2.getPassCountTC());
			data.setFailCountTC(tst2.getFailCountTC());
			data.setTotalCountAutomation(tst1.getTotalCountAutomation());
			data.setTotalCountManual(tst1.getTotalCountManual());

			data.setTotalCountTC(getPercentage(data.getPassCountTC(), data.getFailCountTC()));

			data.setTotalAutomationTest(getPercentage(data.getTotalCountAutomation(), data.getTotalCountManual()));

			data.setTotalManualTest(getPercentage(data.getTotalCountManual(), data.getTotalAutomationTest()));
			storage.saveTestLinkData(data);

		} catch (

		SQLException e) { // TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}

		return true;
	}

	public static String getPercentage(String pass, String failed) {
		int passCount = Integer.parseInt(pass);
		int totalCount = Integer.parseInt(failed) + passCount;
		int percentage = (passCount * 100) / totalCount;
		return Integer.toString(percentage);
	}
	
	public TestLink getYearDataFromTestLinkData() throws SQLException {
		Properties properties = null;
		try {
			properties = propertyLoader.loadPropertiesFile(PropertiesConstants.JENKINS_PROPERTIES_FILENAME);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		TestLinkDataDao tst = new TestLinkDataDao();
		List<TestLinkBuilds> testLinkBuildsList = tst.getYearDataFromTestLinkData();
		TestLink testLink = new TestLink();
		String projectName = properties.getProperty(PropertiesConstants.PROJECT_NAME);
		testLink.setProjectName(projectName);
		testLink.setTestLinkBuilds(testLinkBuildsList);
		storage.saveObject(testLink);
		return testLink;
	}
}
