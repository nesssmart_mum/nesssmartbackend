package com.nessSmart.service.savedata;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nessSmart.Constants.Constants;
import com.nessSmart.dao.MongoStorage;
import com.nessSmart.mapping.common.model.Dashboard;
import com.nessSmart.mapping.common.model.User;
import com.nessSmart.mapping.common.model.Widget;

@Service
public class UserSaveDataServices {
	@Autowired
	MongoStorage mongoStorage;

	public String createUserObject(User user) {
		user.getDashboardConfig().setColumnCount("2");
		user.getDashboardConfig().setTheme("dark");
		User userDetails = (User) mongoStorage.saveObject(user);
		Dashboard dashboard = new Dashboard();
		dashboard.setTitle("Company Dashboard");
		List<Widget> widgetList = new ArrayList<Widget>();
		dashboard.setWidgetList(widgetList);
		dashboard.setUserId(userDetails.getUserId());
		Dashboard dashboard2 = (Dashboard) mongoStorage.saveObject(dashboard);
		System.out.println("dashboard2 id ----------" + dashboard2);

		return returnJSONOBject(userDetails.getUserId());
	}
	
	public String updateUserObject(User user) {
		
		User userDetails = (User) mongoStorage.saveObject(user);
		return returnJSONOBject(userDetails.getUserId());
		
	}
	
	private String returnJSONOBject(String id) {
		JSONObject object = new JSONObject();
		try {
			object.put(Constants.ID, id);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return object.toString();
	}
}
