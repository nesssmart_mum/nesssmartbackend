package com.nessSmart.service.savedata;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nessSmart.Constants.Constants;
import com.nessSmart.dao.MongoStorage;
import com.nessSmart.mapping.database.model.Assessment;
import com.nessSmart.mapping.database.model.AssessmentResponse;
import com.nessSmart.mapping.database.model.Options;
import com.nessSmart.mapping.database.model.Question;

@Service
public class AssessmentSaveOrDeleteDataService {

	@Autowired
	MongoStorage storage;

	public String saveAssessmentResponse(AssessmentResponse response) {
		AssessmentResponse assementResponseSavedObject = (AssessmentResponse) storage.saveObject(response);
		String id = assementResponseSavedObject.getAssessmentResponseId();
		JSONObject object = new JSONObject();
		try {
			object.put(Constants.ASSESSMENT_RESPONSE_ID, id);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return object.toString();
	}

	public boolean saveAnyObject(Object object) {
		storage.saveObject(object);
		return true;
	}

	public boolean deleteAssessment(String assessmentId) {
		return storage.deleteObjectById(Constants.ASSESSMENT_ID, assessmentId, Assessment.class);
	}

	public boolean deleteAssessmentResponse(String assessmentResponseId) {
		return storage.deleteObjectById(Constants.ASSESSMENT_RESPONSE_ID, assessmentResponseId,
				AssessmentResponse.class);
	}

	public List<Question> saveQuestionBank(List<Question> questionsList) {
		List<String> uniqueIdList = new ArrayList<String>();
		for (Question question : questionsList) {
			List<Options> optionsList = question.getOptions();
			for (Options options : optionsList) {
				boolean isUnique = false;
				while (!isUnique) {
					String uniqueID = UUID.randomUUID().toString();
					if (!uniqueIdList.contains(uniqueID)) {
						uniqueIdList.add(uniqueID);
						options.setOptionsId(uniqueID);
						isUnique = true;
					}
				}
			}
			storage.saveObject(question);
		}
		return questionsList;
	}

	
}
