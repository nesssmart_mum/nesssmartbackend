package com.nessSmart.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.nessSmart.mapping.database.model.TestLink;
import com.nessSmart.mapping.database.model.TestLinkBuilds;
import com.nessSmart.mapping.database.model.TestLinkData;

public class TestLinkDataDao {

	private Connection connection;
	private Statement statement;

	public TestLinkDataDao() {
	}

	public TestLinkData getTcAutomationManualCount(String projectId) throws SQLException {

		String query = "SELECT COUNT(*),execution_type FROM `tcversions` where id in ( select tcversion_id from testplan_tcversions WHERE testplan_id in ( SELECT id FROM `testplans` where testproject_id="
				+ projectId + ") ) group by execution_type";
		ResultSet rs = null;
		TestLinkData testlinkdata = null;
		try {

			connection = ConnectionFactory.getConnection();
			statement = connection.createStatement();
			rs = statement.executeQuery(query);
			if (rs.next()) {
				testlinkdata = new TestLinkData();
				testlinkdata.setTotalCountManual(rs.getString(1));
				rs.next();
				testlinkdata.setTotalCountAutomation(rs.getString(1));
			}
		} finally {
			DbUtil.close(rs);
			DbUtil.close(statement);
			DbUtil.close(connection);
		}
		return testlinkdata;
	}

	public TestLinkData getExecPassFailCount(String projectId) throws SQLException {
		String query = "select count(*),status,build_id from (select max(id),STATUS,build_id from executions where build_id in ( SELECT id FROM `builds` WHERE testplan_id in ( SELECT id FROM `testplans` where testproject_id="
				+ projectId + ")) group by tcversion_id) a group by status ";
		ResultSet rs = null;
		TestLinkData testlinkdata = null;
		try {

			connection = ConnectionFactory.getConnection();
			statement = connection.createStatement();
			rs = statement.executeQuery(query);
			if (rs.next()) {
				testlinkdata = new TestLinkData();
				testlinkdata.setFailCountTC(rs.getString(1));
				testlinkdata.setBuildId(rs.getString(3));
				rs.next();
				testlinkdata.setPassCountTC(rs.getString(1));
			}
		} finally {
			DbUtil.close(rs);
			DbUtil.close(statement);
			DbUtil.close(connection);
		}
		return testlinkdata;
	}
	
	public List<TestLinkBuilds> getYearDataFromTestLinkData() throws SQLException {
		String projectName ="'demo'";
		String queary = "SELECT prj.id project_id,b.id build_id,date(b.creation_ts) build_date,exc.id exc_id,exc.status exc_status,exc.execution_type exc_type FROM `testprojects` prj,`testplans` tp,`builds` b,`executions` exc where prj.id=tp.testproject_id and tp.id=b.testplan_id and b.id=exc.build_id and prj.prefix="+ projectName + " and date(b.creation_ts) between '2017-01-01' and '2017-12-31'";
		ResultSet rs = null;
		TestLinkBuilds testlinkdata = null;
		List<TestLinkBuilds> testLinkDatasList = new ArrayList<TestLinkBuilds>();
		try {

			connection = ConnectionFactory.getConnection();
			statement = connection.createStatement();
			rs = statement.executeQuery(queary);
			while (rs.next()) {
				testlinkdata = new TestLinkBuilds();
				testlinkdata.setBuildId(rs.getString(2));
				testlinkdata.setBuildCreateDate(rs.getString(3));
				testlinkdata.setTestExecutionId(rs.getString(4));
				testlinkdata.setTestExecutionStatus(rs.getString(5));
				testlinkdata.setTestExecutionType(rs.getString(6));
				testLinkDatasList.add(testlinkdata);
			}
		} finally {
			DbUtil.close(rs);
			DbUtil.close(statement);
			DbUtil.close(connection);
		}
		return testLinkDatasList;
	}
}
