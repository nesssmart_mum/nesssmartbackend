package com.nessSmart.dao;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;

import com.nessSmart.Constants.Constants;
import com.nessSmart.mapping.common.model.Builds;
import com.nessSmart.mapping.common.model.JenkinsPipelineDetails;
import com.nessSmart.mapping.common.model.JenkinsProjectBuildDetails;
import com.nessSmart.mapping.database.model.FunctionalTestStatusResult;
import com.nessSmart.mapping.database.model.TestLinkData;

@Component
public class MongoStorage {

	@Autowired
	MongoTemplate mongoTemplate;

	public Object saveObject(Object o) {
		mongoTemplate.save(o);
		return o;
	}

	public <T> List<T> saveObjectList(List<T> objectList) {
		mongoTemplate.insertAll(objectList);
		return objectList;
	}

	public boolean deleteObjectById(String feildName, String assessmentId, Class<?> className) {
		mongoTemplate.remove(getQueryForId(feildName, assessmentId), className);
		return true;
	}

	public Object getObjectById(String fieldName, String id, Class<?> className) {
		return mongoTemplate.findOne(getQueryForId(fieldName, id), className);
	}

	public <T> List<T> findWithFilters(Map<String, String> filters, Class<T> className) {
		return mongoTemplate.find(buildQuery(filters), className);
	}

	public long countWithFilters(Map<String, String> filters, Class<?> className) {
		return mongoTemplate.count(buildQuery(filters), className);
	}

	private Query buildQuery(Map<String, String> filterMap) {
		Query query = new Query();
		for (Entry<String, String> filter : filterMap.entrySet()) {
			if (filter.getKey().equalsIgnoreCase(Constants.CREATED_DATE)) {
				query.addCriteria(Criteria.where(filter.getKey()).gte(filter.getValue()));
				continue;
			}
			if (filter.getKey().equalsIgnoreCase(Constants.CREATE_DATE)) {
				query.addCriteria(Criteria.where(filter.getKey()).lte(filter.getValue()));
				continue;
			}
			query.addCriteria(Criteria.where(filter.getKey()).is(filter.getValue()));
		}

		return query;
	}
	
	private Query getQueryForId(String fieldName, String id) {
		Query query = new Query();
		query.addCriteria(Criteria.where(fieldName).is(id));
		return query;
	}

	public boolean saveJenkinsProjectDetails(JenkinsProjectBuildDetails jenkinsProjectDetails) {
		List<Builds> listBuilds = jenkinsProjectDetails.getBuilds();
		for (Builds buildlist : listBuilds) {
			mongoTemplate.save(buildlist);
		}
		mongoTemplate.save(jenkinsProjectDetails);
		return true;
	}

	public boolean saveJenkinsPipelineDetails(JenkinsPipelineDetails jenkinsPipelineDetails) {
		mongoTemplate.save(jenkinsPipelineDetails);
		return true;
	}

	public boolean saveTestLinkData(TestLinkData data) {
		mongoTemplate.save(data);
		return true;
	}

	public boolean saveFunctionalTestingStatus(FunctionalTestStatusResult functionalTestStatusResult) {
		mongoTemplate.save(functionalTestStatusResult);
		return true;
	}
	
	public boolean dropCollection(Class<?> className) {
		mongoTemplate.dropCollection(className);
		return true;
	}

}
